# Setup

create parameters.yml from parameters.yml.dist and set database password

in console run:
app/Resources/bin/setup.sh

Now you will able to list the resources at http://localhost/app_dev.php

# Checking the Restful API #

Download [Dev HTTP Client extension](https://chrome.google.com/webstore/detail/aejoelaoggembcahagimdiliamlcdmfm)

Create a WSSE token:

https://dl.dropbox.com/u/3972728/github/devhttpclient01.png

Use headers:

    ACCEPT            : application/json
    HTTP_Content-Type : application/x-www-form-urlencoded

And parameters:

    _username : admin
    _password : admin

copy the content of WSSE response and remove escape characters \

ie
UsernameToken Username=\"trainer1\", PasswordDigest=\"Mf4Yhbb8lsT20B+5VfLgjyE\/Eig=\", Nonce=\"NDUyMzQ0ZjBiY2ZiM2M0OQ==\", Created=\"2013-03-15T09:28:20+00:00\"

becomes
UsernameToken Username="trainer1", PasswordDigest="Mf4Yhbb8lsT20B+5VfLgjyE/Eig=", Nonce="NDUyMzQ0ZjBiY2ZiM2M0OQ==", Created="2013-03-15T09:28:20+00:00"

https://dl.dropbox.com/u/3972728/github/devhttpclient02.png

As headers you will need to use for rest calls (update the X-WSSE by the one you got above):

    Authorization    : WSSE profile="UsernameToken"
    X-wsse           : UsernameToken Username="admin", PasswordDigest="uG4/uZRfXD424+Oi9Q67DH/rrzc=", Nonce="M2Y4ZDY1MWNkYWU5ODdmMw==", Created="2012-07-17T12:53:58+02:00"
    ACCEPT           : application/json

# Install wkhtmltopdf
```bash
sudo apt-get install wkhtmltopdf
wget http://wkhtmltopdf.googlecode.com/files/wkhtmltopdf-0.9.9-static-amd64.tar.bz2
tar xvjf wkhtmltopdf-0.9.9-static-amd64.tar.bz2
sudo mv wkhtmltopdf-amd64 /usr/local/bin/wkhtmltopdf
sudo chmod +x /usr/local/bin/wkhtmltopdf
php composer.phar install

# generate assets files
php app/console assets:install
```

# Fix MySQL DB uppercase table name problem
1. drop existing database "fitfix" because it consists uppercase table
   names which will cause problem.
2. create empty database "fitfix".
3. in MySQL console, source DB dump:

    ```sql
    source path/to/dump/sql/file
    ```
4. update schema:

    ```bash
    php app/console doctrine:schema:update --dump-sql
    ```
    or
    ```bash
    php app/console doctrine:schema:update --force
    ```
5. edit MySQL configuration at /etc/mysql/my.conf.
    Add line to `[mysqld]` section:

    `lower_case_table_names=1`
6. restart mysql.

# Unit tests

    app/Resources/bin/validate.sh
    phpunit -c app

