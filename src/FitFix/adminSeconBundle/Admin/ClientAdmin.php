<?php

namespace FitFix\adminSecond\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ClientAdmin extends Admin {

    // setup the defaut sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'created_at'
    );

    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('gender', 'choice', array(
                'choices' => array('male' => 'Male', 'female' => 'Female')
            ))
            ->add('mobile')
            ->add('dob')
            ->add('firstName')
            ->add('lastName')
            ->add('trainer', 'entity', array(
                'class' => 'FitFixCoreBundle:Trainer',
                'multiple' => true
            ))
            ->add('reviews', 'collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'review',
                    'attr'      => array('class' => 'review-fields-list')
                ),
            ))
            ->add('goals', 'collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'goal',
                    'attr'      => array('class' => 'goal-fields-list')
                ),
            ))
            ->add('objectives', 'collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'objective',
                    'attr'      => array('class' => 'objective-fields-list')
                ),
            ))
            ->add('mealplans', 'collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'mealplans',
                    'attr'      => array('class' => 'meal-fields-list')
                ),
            ))
            ->add('workouts', 'collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'workout',
                    'attr'      => array('class' => 'workout-fields-list')
                ),
            ))
            ->add('notes', 'collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'note',
                    'attr'      => array('class' => 'note-fields-list')
                ),
            ))
    }

    //Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) { 
        $datagridMapper
                ->add('name')
                ->add('slug')
                ->add('short_description')
                ->add('description')
                ->add('is_active');
    }

    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) { 
        $listMapper
                ->addIdentifier('name')
                ->add('slug')
                ->add('short_description')
                ->add('description')
                ->add('is_active', null, array('editable' => true))
                ->add('created_at');
    }

}
