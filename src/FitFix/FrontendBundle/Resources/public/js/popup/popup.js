/*****************************
**     Popup message
******************************/

//close pop-up box
function closePopup()
 {
   $('#opaco').toggleClass('hidden').removeAttr('style');
   $('#popup').toggleClass('hidden');
   return false;
 }

//open pop-up
function showPopup(popup_type)
 {
   //when IE - fade immediately
   if($.browser.msie)
   {
     $('#opaco').height($(document).height()).toggleClass('hidden');
   }
   else
   //in all the rest browsers - fade slowly
   {
     $('#opaco').height($(document).height()).toggleClass('hidden').fadeTo('slow', 0.7);
   }

   $('#popup')
     .html($('#popup_' + popup_type).html())
     .alignCenter()
     .toggleClass('hidden');

   return false;
 }



function bg_close(){


   var table = document.getElementById('assessment')
   var elems = table.getElementsByTagName('a')
   for(var i=0; i<elems.length; i++) {
      if(elems[i].id){
          document.getElementById(elems[i].id).style.backgroundColor="#ffffff";
      }
   }


}


function bg_color(element_id){
  bg_close();
  {
    document.getElementById(element_id).style.backgroundColor="#79bd1c";
  } 
}
function showhiderightbox() {
    if(screenSize().w < 1096) {
       document.getElementById("activation").style.visibility = "hidden";
    } else {
       document.getElementById("activation").style.visibility = "visible";
    }
}
function screenSize() {
 var w, h; // ���塞 ��६����, w - �����, h - ����
 w = (window.innerWidth ? window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth));
 h = (window.innerHeight ? window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.offsetHeight));
 return {w:w};
}
