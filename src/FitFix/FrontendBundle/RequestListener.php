<?php

namespace FitFix\FrontendBundle;


use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
  public function onKernelRequest(GetResponseEvent $event)
  {
    $event->getRequest()->setFormat('css', 'text/css');
  }
}
