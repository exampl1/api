<?php

namespace FitFix\FrontendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FitFix\FrontendBundle\Form\TrainerType;
use FitFix\CoreBundle\Entity\Trainer;
use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use FitFix\CoreBundle\Entity\User;
use FOS\UserBundle\Doctrine\UserManager;

/**
 * Registration controller.
 *
 *// @Route("/registration", name="front_trainer_create")
 */
class RegistrationController extends Controller
{
    /**
     * Displays a form to create a new Client entity.
     *
     * @Route("/", name="frontend_new_trainer")
     * @Template()
     */
    public function newAction()
    {
        $trainer = new Trainer();

        $form = $this->createForm(new TrainerType('add'), $trainer, array('action' => $this->generateUrl('frontend_new_trainer'), 'attr' => array('class' => 'main-form')));

        $em = $this->getDoctrine()->getManager();
        
        $request = $this->getRequest();
        
        if($request->isMethod('POST')){
        	
        	$form->bind($request);
        	        	
        	//if($form->isValid()){

        		// Create the user
        		$user = $trainer->getUser();			
        		$user->setRoles(array('ROLE_TRAINER'));
        		$userManager = $this->get('fos_user.user_manager');
        		$user->setTrainer($trainer);
        		
        		$userManager->updateUser($user);

        		$this->__generateFakeClient($userManager, $trainer);

        		$em->persist($trainer);
        		//return $this->redirect('http://site.fitfixpro.com/activation.html');
        		// Send the stupid email
        		$em->flush();
        		
        		$session = $request->getSession();
        		
        		//$url = $this->generateUrl('frontend_subscription', array('tuuid' => $trainer->getUuid()));
        		
			//return $this->redirect('http://site-s.fitfixpro.com/');
        		return $this->redirect('http://site.fitfixpro.com/activation.html');
        		
        	//}
        	
        }

        return array(
            'entity' => $trainer,
            'form'   => $form,
        );
    }
    
    /**
     * Generates a fake client
     * 
     * @return Client
     */
    private function __generateFakeClient(UserManager $userManager, Trainer $trainer){
    	
    	$client = new Client();
    	$client->setTrainer($trainer);
    	$client->setGender('male');
    	$client->setMobile('123456789');
    	$client->setDob(new \DateTime('1980-12-13'));
    	$client->setFirstName('John');
    	$client->setLastName('Smith');
    	$client->setPhoto('http://fitfixappdata.s3.amazonaws.com/images/defaults/user.png');
    	$client->setArchived(false);
    	
    	$clientUser = $userManager->createUser();
    	$clientUser->setUsername('template-client' . uniqid());
    	$clientUser->setEmail(uniqid() . '@test.com');
    	$clientUser->setPlainPassword('111111');
    	$clientUser->setRoles(array('ROLE_CLIENT'));
    	$clientUser->setRegistered(new \DateTime());
    	
    	$client->setUser($clientUser);
    	$clientUser->setClient($client);
    	
    	$trainer->addClient($client);
    	
    	$userManager->updateUser($clientUser);
    	
    	return $client;
    }


    /**
     * Creates a new Trainer entity.
     *
     * @Route("/create", name="front_trainer_create")
     * @Method("POST")
     * @Template("FitFixFrontendBundle:Registration:new1.html1.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Trainer1();
        $user = new User();
        $user->setTrainer($entity);
        $user->setUsername($username);

        $form = $this->createForm(new TrainerType('add'), $entity);
                
        $form->bind($request);
        $user = $entity->getUser();
        $user->addRole('role_trainer');
        $user->setTrainer($entity);

        //if ($form->isValid()) {
        	
        	$client = new Client();
        	$client->setTrainer($entity);
        	$client->setGender('male');
        	$client->setMobile('123456789');
        	$client->setDob(new \DateTime('1980-12-13'));
        	$client->setFirstName('John');
        	$client->setLastName('Smith');
        	$client->setPhoto('https://s3-eu-west-1.amazonaws.com/dumping-ground/stock/FISH_STOCK_PROFILE_MALE_00000.jpg');
        	
        	$userManager = $this->container->get('fos_user.user_manager');
        	$clientUser = $userManager->createUser();
        	$clientUser->setUsername('template-client' . uniqid());
        	$clientUser->setEmail(uniqid() . '@test.com');
        	$clientUser->setPlainPassword('111111');
        	$clientUser->addRole('role_client');
        	$clientUser->setRegistered(new \DateTime());
        	
        	$client->setUser($clientUser);
        	
        	$userManager->updateUser($clientUser);
        	
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->persist($client);
            $em->flush();

            $userManager = $this->container->get('fos_user.user_manager');
            $user = $entity->getUser();
            $token = new UsernamePasswordToken($user, null, "A Strange Key $@@", $user->getRoles());
            $this->get('security.context')->setToken($token);
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.authentication", $event);
            return $this->redirect($this->generateUrl('frontend_subscription')."?tid=".$entity->getId());
        //}
        
        var_dump($form->getErrorsAsString());

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Handle callback after trainer paid.
     *
     * @Route("/paid_callback", name="frontend_paid_callback")
     * @Template()
     */
    public function paidCallbackAction()
    {
        $em = $this->getDoctrine()->getManager();
        return $view->setStatusCode(200)->setData($entities);

        $entity = $em->getRepository('FitFixCoreBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $view = FOSView::create();
        return $view;
    }


}
