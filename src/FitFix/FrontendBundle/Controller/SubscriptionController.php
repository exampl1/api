<?php

namespace FitFix\FrontendBundle\Controller;

use FitFix\CoreBundle\Entity\Trainer;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View AS FOSView;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Subscription controller.
 *
 * @Route("/subscription")
 */
class SubscriptionController extends Controller
{
    /**
     * Lists all subscription.
     *
     * @Route("/package/{tuuid}/", name="frontend_subscription")
     * @Template()
     */
    public function indexAction(Request $request)
    {
    	$trainer = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:Trainer')->findOneByUuid($request->get('tuuid'));
    	
        $trainerId = $trainer->getId();
        if ( !$trainerId ) { $trainerId = 0; }
        return array(
            'trainerId' => $trainerId,
        	'uuid' => $trainer->getUuid()
        );
    }
    
    /**
     * 
     * @Route("/email/", name="frontend_email")
     * @Template("FitFixFrontendBundle:Emails:subscribed.html.twig")
     */
    public function emailAction(){
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	$repo = $em->getRepository('FitFixCoreBundle:Trainer');
    	
    	$trainer = $repo->find(5);
    	
    	
    	return array(
    							'trainer' => $trainer,
    							'subscription' => array(
    									'period' => '1 month',
    									'price' => '19.99'
    							)
    					);
    	
    }
    
    /**
     * The thank you page
     * 
     * @Route("/thankyou/", name="frontend_subscription_thankyou")
     * 
     * @Template()
     */
    public function thankYouAction(){
    	
    }

    /**
     * Handle Subscription payment success.
     *
     * @Route("/callback/{tid}/", name="front_subscription_callback")
     * @Method("POST")
     * @Template()
     */
    public function paypalCallbackAction(Request $request)
    {
        $logger = $this->get('logger');
        
        $tid = $request->get('tid');
        
        $em = $this->getDoctrine()->getManager();
        $trainer = $em->getRepository('FitFixCoreBundle:Trainer')->find($tid);
        /* @var $trainer Trainer */
        
        if(!$trainer){
        	throw new NotFoundHttpException();
        }
                
        $logger->debug(sprintf('Got subscription for trainer with ID: %s', $trainer->getId()));
        
        foreach($request->request->all() as $key => $value){
        	$logger->debug(sprintf('Key: %s - Value: %s', $key, $value));
        }
        
        $type = $request->get('txn_type');
        $item = $request->get('item_number');
        $expire_date = $trainer->getCreatedAt();
        $amount = '0.00';
        
        if($type == 'subscr_signup'){
        	$trainer->getUser()->setEnabled(true);
        	$expire_date->modify('+14 day');
        	
        	$period = '1 month';
        	
        	if($item == 2){
        		$period = '6 months';
        	} else if ($item == 3){
        		$period = '1 year';
        	}

        	$message = \Swift_Message::newInstance()
        	->setSubject('Welcome to FitFix')
        	->setFrom('info@fitfixpro.co.uk')
        	->setTo($trainer->getUser()->getEmail())
        	->setBody(
        			$this->renderView(
        					'FitFixFrontendBundle:Emails:subscribed.html.twig',
        					array(
        							'trainer' => $trainer,
        							'subscription' => array(
        									'period' => $period,
        									'price' => $request->get('mc_amount3', 0.00)
        							)
        					)
        			)
        	)
        	->setContentType("text/html");
        	 
        	 
        	$this->get('mailer')->send($message);
        	
        }
        
        if($type == 'subscr_cancel' || $type == 'subscr_eot' || $type == 'subscr_failed'){
        	$trainer->getUser()->setEnabled(false);
        	$expire_date = new \DateTime();
        }
        
        if($type == 'subscr_payment'){
        	
        	if($item == 1){
        		$expire_date->add(new \DateInterval("P1M"));
        	} else if ($item == 2) {
        		$expire_date->add(new \DateInterval("P6M"));
        	} else if ($item == 3) {
        		$expire_date->add(new \DateInterval("P12M"));
        	}
        	
        	$amount = $request->get('mc_amount3', 0.00);
        	
        }
        
        $trainer->setLastPaidAmount($amount);
        $trainer->setExpireDate($expire_date);
        $trainer->setLastPaidAt(new \DateTime());
        
        $em->persist($trainer);
        $em->flush();

        $view = FOSView::create();

        $view->setStatusCode(200);
        return $view;
    }
}
