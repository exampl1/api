<?php

namespace FitFix\DocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FitFix\CoreBundle\Entity\Invoice;
use Symfony\Bridge\Monolog\Logger;

/**
 * @Route("/invoice")
 * @author gavinwilliams
 *
 */
class InvoiceController extends Controller
{
	const PAYPAL_REDIRECT_URL = 'https://www.paypal.com/webscr&cmd=';
	
	private $config = array(
    			'mode' => 'live',
    			'acct1.UserName' => 'info_api1.fitfix.co.uk',
    			'acct1.Password' => 'UB44DWYUKU956BGJ',
    			'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31ArV2mJoxEYhkwAbOG5FVCCoL-g46',
    			'acct1.AppId' => 'APP-0D458514KH4224516'
    		);
	
	/**
	 * @Route("/ipn/")
	 * @Template()

	 */
	public function ipnAction(){
		
		$message = new \PPIPNMessage(null, $this->config);
		
		if(!$message->validate()){
			$this->get('logger')->warn("Invalid IPN message");
		}
		
		$data = $message->getRawData();
				
		$invoiceId = $data['transaction%5B0%5D.invoiceId'];
		
		if($invoiceId && $data['status'] == 'COMPLETED'){
			$em = $this->getDoctrine()->getManager();
			$invoice = $em->getRepository('FitFixCoreBundle:Invoice')->find($invoiceId);
			/* @var $invoice Invoice */
			
			if(!$invoice){
				$this->get('logger')->warn(sprintf("No invoice found for transaction: %s", $data['transaction%5B0%5D.id']));
				return;
			}
			
			$invoice->setPaid(new \DateTime());
			$invoice->setPaymentMethod('PayPal');
			$invoice->setStatus('paid');
			
			$em->persist($invoice);
			$em->flush();
		}
		
		foreach($message->getRawData() as $key => $value) {
			$this->get('logger')->debug(sprintf("IPN: %s / %s", $key, $value));
		}
		
		if($message->validate()) {
			$this->get('logger')->debug("Success: Got valid IPN data");
		} else {
			$this->get('logger')->debug("Error: Got invalid IPN data");
		}
		
		return $this->render('FitFixDocBundle:Invoice:ipn.html.twig');
		
	}
	
    /**
     * @Route("/{uuid}/view/")
     * @Template()
     * 
     * @ParamConverter("invoice", class="FitFixCoreBundle:Invoice", options={"mapping": {"uuid": "uuid"}})
     */
    public function viewAction(Invoice $invoice)
    {
    	
    	$paypalPaymentURL = $this->_getPaymentURL($invoice);
    	
    	return $this->render('FitFixDocBundle:Invoice:view.html.twig', array('invoice' => $invoice, 'paypalPaymentURL' => $paypalPaymentURL));
    }
    
    private function _getPaymentURL(Invoice $invoice){
    	$paypalPaymentURL = null;
    	 
    	if($invoice->getFrom()->getPayPalAddress()){
    		$receiver = new \Receiver();
    		$receiver->email = $invoice->getFrom()->getPayPalAddress();
    		$receiver->amount = $invoice->getPrice();
    		$receiver->invoiceId = $invoice->getId();
    	
    		$receiverList = new \ReceiverList($receiver);
    	
    		$successURL = $this->generateUrl('fitfix_doc_invoice_success', array('uuid' => $invoice->getUuid()), true);
    		$errorURL = $this->generateUrl('fitfix_doc_invoice_error', array('uuid' => $invoice->getUuid()), true);
    	
    		$currency = explode(':', $invoice->getFrom()->getBillingCurrency());
    	
    		$paymentRequest = new \PayRequest(new \RequestEnvelope('en_US'), 'PAY', $errorURL, $currency[0], $receiverList, $successURL);

    		$paymentRequest->ipnNotificationUrl = $this->generateUrl('fitfix_doc_invoice_ipn', array(), true);
    		
    		
    		$service = new \AdaptivePaymentsService($this->config);
    	
    		try {
    			$response = $service->Pay($paymentRequest);
    		} catch (Exception $e){
    			echo $e->getMessage();
    		}
    	
    		if($response->responseEnvelope->ack != 'Success'){
    			$this->get('logger')->err(sprintf('Unable to create payment URL for invoice with ID "%s" with error "%s"', $invoice->getId(), $response->error->message));
    		} else {
    			$paypalPaymentURL = sprintf('%s_ap-payment&paykey=%s',self::PAYPAL_REDIRECT_URL, $response->payKey);
    		}
    	
    	}
    	
    	return $paypalPaymentURL;
    }

    /**
     * @Route("/{uuid}/success/")
     * @Template()
     * 
	 * @ParamConverter("invoice", class="FitFixCoreBundle:Invoice", options={"mapping": {"uuid": "uuid"}})
     */
    public function successAction(Invoice $invoice){
    	
    	return $this->render('FitFixDocBundle:Invoice:success.html.twig', array('invoice' => $invoice));
    }
    
    /**
     * @Route("/{uuid}/error/")
     * @Template()
     *
     * @ParamConverter("invoice", class="FitFixCoreBundle:Invoice", options={"mapping": {"uuid": "uuid"}})
     */
    public function errorAction(Invoice $invoice){
    	
    	return $this->render('FitFixDocBundle:Invoice:error.html.twig', array('invoice' => $invoice));
    }
    
    /**
     * @Route("/{uuid}/pay/")
     * @Template()
     * 
     * @ParamConverter("invoice", class="FitFixCoreBundle:Invoice", options={"mapping": {"uuid": "uuid"}})
     */
    public function payAction(Invoice $invoice)
    {
    	$paypalPaymentURL = $this->_getPaymentURL($invoice);
    	
    	return $this->redirect($paypalPaymentURL);
    }

}
