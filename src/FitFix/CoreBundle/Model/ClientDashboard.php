<?php

namespace FitFix\CoreBundle\Model;

use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use FitFix\CoreBundle\Entity\WorkoutLog;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use FitFix\CoreBundle\Entity\Objective;

/**
 * @author gavinwilliams
 * @ExclusionPolicy("all")
 */
class ClientDashboard extends BaseDashboard {
	
	/**
	 * The last workout for the user
	 * @var WorkoutLog
	 * 
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 * @SerializedName("lastWorkoutLog")
	 */
	private $lastWorkoutLog;
	
	/**
	 * The number of sessions booked
	 * @var int
	 * 
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 * @SerializedName("sessionsBooked")
	 */
	private $sessionsBooked;
	
	/**
	 * The total sessions complete
	 * @var int
	 * 
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 * @SerializedName("totalSessions")
	 */
	private $totalSessions;
	
	/**
	 * The goals for the client
	 * @var array
	 * 
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 */
	private $goals;
	
	/**
	 * The clients objectives
	 * @var string
	 * 
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 * @SerializedName("specificAim")
	 */
	private $specificAim;
	
	/**
	 * The client's profile
	 * @var unknown
	 *
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 */
	private $client;
	
	/**
	 * The contact information for the client
	 * @var array
	 * 
	 * @Expose()
	 * @Groups({"dashboard-client"})
	 * @SerializedName("contactInformation")
	 */
	private $contactInformation;
	
	
	public function __construct() {
		$this->setGoals(array());
	}
	
	/**
	 *
	 * @return the Workout
	 */
	public function getLastWorkoutLog() {
		return $this->lastWorkoutLog;
	}
	
	/**
	 *
	 * @param Workout $lastWorkout        	
	 */
	public function setLastWorkoutLog(WorkoutLog $lastWorkoutLog) {
		$this->lastWorkoutLog = $lastWorkoutLog;
		return $this;
	}
	
	/**
	 *
	 * @return the int
	 */
	public function getSessionsBooked() {
		return $this->sessionsBooked;
	}
	
	/**
	 *
	 * @param
	 *        	$sessionsBooked
	 */
	public function setSessionsBooked($sessionsBooked) {
		$this->sessionsBooked = (int) $sessionsBooked;
		return $this;
	}
	
	/**
	 *
	 * @return the int
	 */
	public function getTotalSessions() {
		return $this->totalSessions;
	}
	
	/**
	 *
	 * @param
	 *        	$totalSessions
	 */
	public function setTotalSessions($totalSessions) {
		$this->totalSessions = (int) $totalSessions;
		return $this;
	}
	
	/**
	 *
	 * @return the array
	 */
	public function getGoals() {
		return $this->goals;
	}
	
	/**
	 *
	 * @param array $goals        	
	 */
	public function setGoals(array $goals = array()) {
		$this->goals = $goals;
		return $this;
	}
	
	/**
	 *
	 * @return the array
	 */
	public function getSpecificAim() {
		return $this->specificAim;
	}
	
	/**
	 *
	 * @param string $objectives        	
	 */
	public function setSpecificAim($specificAim = null) {
		$this->specificAim = $specificAim;
		return $this;
	}
	
	/**
	 *
	 * @return the array
	 */
	public function getContactInformation() {
		return $this->contactInformation;
	}
	
	/**
	 *
	 * @param array $contactInformation        	
	 */
	public function setContactInformation(array $contactInformation) {
		$this->contactInformation = $contactInformation;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown
	 */
	public function getClient() {
		return $this->client;
	}
	
	/**
	 *
	 * @param
	 *        	$client
	 */
	public function setClient($client) {
		$this->client = $client;
		return $this;
	}
	
	
	
}