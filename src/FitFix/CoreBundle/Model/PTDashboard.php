<?php

namespace FitFix\CoreBundle\Model;

use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;

/**
 * 
 * @author gavinwilliams
 * @ExclusionPolicy("all")
 */

class PTDashboard extends BaseDashboard {
	
	/**
	 * All of the sessions that a PT has had, just an array of dates
	 * @var array
	 * 
	 * @Expose()
	 * @Groups({"dashboard-pt"})
	 */
	private $sessions = array();
	
	/**
	 * The income for the day, month, year
	 * @var array
	 * 
	 * @Expose()
	 * @Groups({"dashboard-pt"})
	 */
	private $income = array(
		"day" => 0,
		"month" => 0,
		"year" => 0
	);
	
	/**
	 * The income growth for the day, month, year
	 * @var array
	 * 
	 * @Expose()
	 * @Groups({"dashboard-pt"})
	 * @SerializedName("incomeGrowth")
	 */
	private $incomeGrowth = array(
		"day" => 0,
		"month" => 0,
		"year" => 0
	);
	
	/**
	 * The trainer's profile
	 * @var unknown
	 * 
	 * @Expose()
	 * @Groups({"dashboard-pt"})
	 */
	private $trainer;

    /**
     * The trial profile
     * @var unknown
     *
     * @Expose()
     * @Groups({"dashboard-pt"})
     */
    private $trial;

	/**
	 *
	 * @return the array
	 */
	public function getSessions() {
		return $this->sessions;
	}
	
	/**
	 *
	 * @param array $sessions        	
	 */
	public function setSessions(array $sessions) {
		$this->sessions = $sessions;
		return $this;
	}
	
	/**
	 *
	 * @return the array
	 */
	public function getIncome() {
		return $this->income;
	}
	
	/**
	 *
	 * @param array $income        	
	 */
	public function setIncome(array $income) {
		$this->income = $income;
		return $this;
	}
	
	/**
	 *
	 * @return the array
	 */
	public function getIncomeGrowth() {
		return $this->incomeGrowth;
	}
	
	/**
	 *
	 * @param array $incomeGrowth        	
	 */
	public function setIncomeGrowth(array $incomeGrowth) {
		$this->incomeGrowth = $incomeGrowth;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown
	 */
	public function getTrainer() {
		return $this->trainer;
	}
	
	/**
	 *
	 * @param
	 *        	$trainer
	 */
	public function setTrainer($trainer) {
		$this->trainer = $trainer;
		return $this;
	}

    /**
     *
     * @return the unknown
     */
    public function getTrial() {
        return $this->trainer;
    }

    /**
     *
     * @param
     *        	$trainer
     */
    public function setTrial($trial) {
        $this->trial = $trial;
        return $this;
    }
	
}