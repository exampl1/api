<?php

namespace FitFix\CoreBundle\Model;

use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use FitFix\CoreBundle\Entity\Event;

/**
 * 
 * @author gavinwilliams
 * @ExclusionPolicy("all")
 */

class BaseDashboard {
	
	/**
	 * @var array
	 * @Expose()
	 * @Groups({"dashboard-client", "dashboard-pt"})
	 */
	private $notifications = array();
	
	/**
	 * @var CalendarItemMappedSuperclass
	 * @Expose()
	 * @Groups({"dashboard-client", "dashboard-pt"})
	 * @SerializedName("nextAppointment")
	 */
	private $nextAppointment;
	
	/**
	 * @return the array
	 */
	public function getNotifications() {
		return $this->notifications;
	}
	
	/**
	 * @param array $notifications        	
	 */
	public function setNotifications(array $notifications) {
		$this->notifications = $notifications;
		return $this;
	}
	
	/**
	 * @return the array
	 */
	public function getNextAppointment() {
		return $this->nextAppointment;
	}
	
	/**
	 * @param array $nextAppointment        	
	 */
	public function setNextAppointment($nextAppointment) {
		$this->nextAppointment = $nextAppointment;
		return $this;
	}
		
	
}