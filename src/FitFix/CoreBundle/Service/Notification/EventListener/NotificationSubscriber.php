<?php

namespace FitFix\CoreBundle\Service\Notification\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use FitFix\CoreBundle\Entity\Notification;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;

/**
 * Listens for changes in an entity and pushes notifications
 * @author gavinwilliams
 */
class NotificationSubscriber implements EventSubscriber {
	
	/**
	 * Serializer
	 * @var Serializer
	 */
	private $_serializer;
	
	const ACTION_PERSIST = "persist";
	const ACTION_UPDATE = "update";
	
	public function __construct(Serializer $serializer){
		$this->_serializer = $serializer;
	}
	
	public function getSubscribedEvents(){
		return array(
			'postPersist',
			'postUpdate'
		);
	}
	
	public function postPersist(LifecycleEventArgs $args){
		$this->dispatchNotification($args, self::ACTION_PERSIST);
	}
	
	public function postUpdate(LifecycleEventArgs $args){
		$this->dispatchNotification($args, self::ACTION_UPDATE);
	}
	
	private function dispatchNotification(LifecycleEventArgs $args, $action){
		
		$entity = $args->getEntity();
		$entityManager = $args->getEntityManager();
		
		$entityNamespaceArray = explode('\\', get_class($entity));
		$entityClass = end($entityNamespaceArray);
		
		$notification = new Notification();
		$notification->setType(strtolower($entityClass));
		$notification->setAction($action);
		
		$class = '\\FitFix\\CoreBundle\\Service\\Notification\\Model\\' . $entityClass . 'NotificationModel';
		
		if(class_exists($class)){

			$serializationContext = SerializationContext::create()->setGroups(array('notification'));
			$class::setNotificationUser($notification, $entity, $args, $action);
			$notificationData = $this->_serializer->serialize($entity, 'json', $serializationContext);
			$notification->setData($notificationData);
			$entityManager->persist($notification);
			$entityManager->flush();
			
		}
		
	}
	
}