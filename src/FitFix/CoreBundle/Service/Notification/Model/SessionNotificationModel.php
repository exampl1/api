<?php

namespace FitFix\CoreBundle\Service\Notification\Model;

use FitFix\CoreBundle\Service\Notification\Model\AbstractNotificationModel;
use FitFix\CoreBundle\Entity\Notification;
use Doctrine\ORM\Event\LifecycleEventArgs;

class SessionNotificationModel extends AbstractNotificationModel {
	
	/* (non-PHPdoc)
	 * @see \FitFix\CoreBundle\Service\Notification\Model\AbstractNotificationModel::setNotificationUser()
	 */
	public static function setNotificationUser(Notification $notification, $entity, LifecycleEventArgs $args, $action) {
		if($action == 'persist'){
			// This is a new booking, set the user to who it's going to
			$notification->setUser($entity->getTo());
		} else if($action == 'update') {
			// This is an amendement possibly an RSVP, set the user to who it's come from
			$notification->setUser($entity->getFrom());
		}
		
	}

}