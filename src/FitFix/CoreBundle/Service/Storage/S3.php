<?php 

namespace FitFix\CoreBundle\Service\Storage;

use Rhumsaa\Uuid\Uuid;

class S3 {
	
	/**
	 * AWS S3 Service
	 * @var \AmazonS3
	 */
	private $s3;
	
	public function __construct(\AmazonS3 $s3){
		$this->s3 = $s3;
	}

	/**
	 * Gets the url of an object
	 * @param string $key
	 * @param string $bucket
	 */
	public function getFileURL($key, $bucket){
		return $this->s3->get_object_url($bucket, $key);
	}
	
	/**
	 * Uploads data to S3
	 * @param string $bucket
	 * @param string $path
	 * @param string $data
	 * @param string $extension
	 * @param string $contentType
	 * @return string|NULL
	 */
	public function uploadUniqueFileWithData($bucket, $path, $data, $extension, $contentType){
		
		$uniquepath = Uuid::uuid4() . '/' . Uuid::uuid4();
		$objectname = $path . '/' . $uniquepath . '.' . $extension;
		
		$parts = explode(',', $data);
		$data = $parts[1];
		$data = base64_decode($data);
		
		$response = $this->s3->create_object($bucket, $objectname, array(
				'body' => $data,
				'contentType' => $contentType,
				'acl' => \AmazonS3::ACL_PUBLIC
		));
		
		
		if($response->isOK()){
			$url = $this->s3->get_object_url($bucket, $objectname);
			
			return $url;
		}
				
		return null;
		
	}
}