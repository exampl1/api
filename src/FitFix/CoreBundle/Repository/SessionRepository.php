<?php

namespace FitFix\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use FitFix\CoreBundle\Entity\User;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;

class SessionRepository extends EntityRepository {
	
	/**
	 * Finds all events by user, if the user type is client,
	 * declined events do not show
	 * 
	 * @param User $user
	 * @return unknown
	 */
	
	public function findAllByUser(User $user){
		
		if($user->hasRole('ROLE_TRAINER')){
			$query = $this->getEntityManager()->createQuery("SELECT e FROM FitFix\CoreBundle\Entity\Session e WHERE e.from = :user OR e.to = :user AND e.rsvp != 'cancelled'");
		} else if ($user->hasRole('ROLE_CLIENT')){
			$query = $this->getEntityManager()->createQuery("SELECT e FROM FitFix\CoreBundle\Entity\Session e WHERE e.from = :user OR e.to = :user AND (e.rsvp != 'declined' AND e.rsvp != 'cancelled')");
		}
		
		$query->setParameter('user', $user);
		
		$events = $query->getResult();
		
		return $events;
		
	}
	
	public function getTotalSessionsByMonthForPT(User $pt){
		
		$rsm = new ResultSetMapping();
		
		$query = $this->getEntityManager()->getConnection()->prepare("SELECT UNIX_TIMESTAMP(DATE_FORMAT(e.start, '%Y-%m-%d')) as 'date', count(e.id) as 'sessionCount' FROM Session AS e WHERE e.from_id = :pt OR e.to_id = :pt AND e.rsvp = 'accepted' GROUP BY MONTH(e.start), YEAR(e.start)", $rsm);
		$query->bindValue("pt", $pt->getId());
		$query->execute();
		
		$events = $query->fetchAll();
				
		return $events;
		
	}

	/**
	 * Gets the total number of sessions for a client before today
	 * @param User $client
	 * @return int
	 */
	public function getTotalSessionsForClient(User $client){
		
		$query = $this->getEntityManager()->createQuery("SELECT COUNT(s) FROM FitFix\CoreBundle\Entity\Session s WHERE (s.to = :client OR s.from = :client) AND s.rsvp = 'accepted' AND s.end < CURRENT_TIMESTAMP()");
		
		$query->setParameter('client', $client);
		
		$sessionCount = $query->getSingleScalarResult();
		
		return $sessionCount;
	}
	
	/**
	 * Gets the total number of booked sessions for a client
	 * @param User $client
	 * @return int
	 */
	public function getTotalBookedSessionsForClient(User $client){
		
		$query = $this->getEntityManager()->createQuery("SELECT COUNT(s) FROM FitFix\CoreBundle\Entity\Session s WHERE (s.to = :client OR s.from = :client) AND s.rsvp = 'accepted'");
		
		$query->setParameter('client', $client);
		
		$sessionCount = $query->getSingleScalarResult();
		
		return $sessionCount;
		
	}
	
	/**
	 * 
	 * @param User $client
	 * @param User $trainer
	 * @return Ambigous <multitype:, \Doctrine\ORM\mixed, \Doctrine\ORM\Internal\Hydration\mixed, \Doctrine\DBAL\Driver\Statement, \Doctrine\Common\Cache\mixed>
	 */
	public function findAllForClientWithTrainer(User $client, User $trainer){
		
		$query = $this->getEntityManager()->createQuery("SELECT e FROM FitFix\CoreBundle\Entity\Session e WHERE (e.from = :trainer OR e.to = :trainer) AND (e.from = :client OR e.to = :client) AND e.rsvp != 'cancelled'");

		$query->setParameters(array(
			'client' => $client,
			'trainer' => $trainer
		));
		
		$events = $query->getResult();
		
		return $events;
		
	}
	
	/**
	 * Finds the next confirmed session for a specific user
	 * @param User $user
	 */
	public function findNextSessionForUser(User $user){
		$query = $this->getEntityManager()->createQuery("SELECT s FROM FitFix\CoreBundle\Entity\Session s WHERE (s.from = :user OR s.to = :user) AND s.start > CURRENT_TIMESTAMP() AND s.rsvp = 'accepted' ORDER BY s.start ASC");
		$query->setMaxResults(1);
		$query->setParameter('user', $user);
		$event = $query->getOneOrNullResult();
		
		return $event;
	}
}
