<?php

namespace FitFix\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use FitFix\CoreBundle\Entity\Client;
use FitFix\CoreBundle\Entity\Trainer;
use Doctrine\ORM\Query;

/**
 * PackagePurchaseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PackagePurchaseRepository extends EntityRepository
{
	/**
	 * Returns package purchases for a client where a package is from a trainer
	 *
	 * @param Client $client
	 * @param Trainer $trainer
	 * @return Ambigous <multitype:, \Doctrine\ORM\mixed, \Doctrine\ORM\Internal\Hydration\mixed, \Doctrine\DBAL\Driver\Statement, \Doctrine\Common\Cache\mixed>
	 */
	public function findPackagePurchasesForClientFromTrainer(Client $client, Trainer $trainer){
	
		$query = $this->getEntityManager()->createQuery("SELECT pp FROM FitFix\CoreBundle\Entity\PackagePurchase pp INNER JOIN pp.package p WHERE p.trainer = :trainer AND pp.client = :client");
	
		$query->setParameters(array(
				'trainer' => $trainer,
				'client' => $client
		));
	
		$packagepurchases = $query->getResult(Query::HYDRATE_OBJECT);
	
		return $packagepurchases;
	
	}
}
