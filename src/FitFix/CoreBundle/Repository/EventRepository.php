<?php

namespace FitFix\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use FitFix\CoreBundle\Entity\User;

class EventRepository extends EntityRepository {
	
	/**
	 * Finds all events by user, if the user type is client,
	 * declined events do not show
	 * 
	 * @param User $user
	 * @return unknown
	 */
	public function findAllByUser(User $user){
		
		if($user->hasRole('ROLE_TRAINER')){
			$query = $this->getEntityManager()->createQuery("SELECT e FROM FitFix\CoreBundle\Entity\Event e WHERE e.from = :user OR e.to = :user");
		} else if ($user->hasRole('ROLE_CLIENT')){
			$query = $this->getEntityManager()->createQuery("SELECT e FROM FitFix\CoreBundle\Entity\Event e WHERE e.from = :user OR e.to = :user AND e.rsvp != 'declined'");
		}
		
		$query->setParameter('user', $user);
		
		$events = $query->getResult();
		
		return $events;
		
	}
	
}