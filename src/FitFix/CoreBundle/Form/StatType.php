<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('height')
            ->add('heightUnit')
            ->add('weight')
            ->add('weightUnit')
            ->add('restingHeartRate')
            ->add('neck')
            ->add('neckUnit')
            ->add('chest')
            ->add('chestUnit')
            ->add('thigh')
            ->add('thighUnit')
            ->add('calf')
            ->add('calfUnit')
            ->add('hip')
            ->add('hipUnit')
            ->add('upperArm')
            ->add('upperArmUnit')
            ->add('bmi')
            ->add('bmr')
            ->add('targetWeight')
            ->add('targetHeartRate')
            ->add('bodyFat')
            ->add('sidePhoto')
            ->add('frontPhoto')
            ->add('backPhoto')
            ->add('intensity')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Stat'
        ));
    }

    public function getName()
    {
        return '';
    }
}
