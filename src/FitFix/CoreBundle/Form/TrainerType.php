<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FitFix\CoreBundle\Form\DataTransformer\StringToDateTimeTransformer;

class TrainerType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('payPalAddress')
            ->add('gender')
            ->add('lastName')
            ->add('bio')
            ->add('sessionEnvironments')
            ->add('photo')
            ->add('logo')
            ->add('phoneNumber')
            ->add('experience')
            ->add('languages')
            ->add('location')
            ->add('specialisms')
            ->add('qualifications')
            ->add('billingCurrency')
            ->add('repsVerified')
            ->add('terms')
            ->add('email', 'text', array('mapped' => false))
            ->add('createdAt', 'text', array('mapped' => false))
            ->add('reviews', 'text', array('mapped' => false))
            ->add($builder->create('birthday', 'text')->addModelTransformer(new StringToDateTimeTransformer()))
            ->add('id', 'text', array('mapped' => false))
            ->add('reviews', 'text', array('mapped' => false))
            ->add('phoneNumber')
            ->add('expiredDate', 'text', array('mapped' => false))
            ->add('email', 'text', array('mapped' => false))
            ->add('numberOfClients', 'text', array('mapped' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Trainer',
        	'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}
