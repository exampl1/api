<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FitFix\CoreBundle\Form\DataTransformer\StringToDateTimeTransformer;

class ClientType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid', 'text', array('mapped' => false))
            ->add('firstName')
            ->add('lastName')
            ->add('gender')
            ->add($builder->create('dob', 'text')->addModelTransformer(new StringToDateTimeTransformer()))
            ->add('landline')
            ->add('mobile')
            ->add('address')
            ->add('archived', 'text', array('mapped' => false))
            ->add('specificAim')
            ->add('notes')
            ->add('photo')
            ->add('termsAccepted')
            ->add('lifestyle', 'text', array('mapped' => false))
            ->add('nutrition', 'text', array('mapped' => false))
            ->add('objectives')
            ->add('PARQ', 'text', array('mapped' => false))
            ->add('trainer', 'text', array('mapped' => false))
            ->add('user', 'text', array('mapped' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Client'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'fitfix_corebundle_client';
    }
}
