<?php

namespace FitFix\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormBuilderInterface;
use FitFix\CoreBundle\Form\DataTransformer\UserToIdTransformer;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserSelectorType extends AbstractType {
	
	/**
	 * The object manager
	 * @var ObjectManager
	 */
	private $om;
	
	public function __construct(ObjectManager $om) {
		$this->om = $om;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options) {
		
		$transformer = new UserToIdTransformer($this->om);
		$builder->addModelTransformer($transformer);
		
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array(
			'invalid_message' => 'The user does not exist'				
		));
	}
	
	public function getParent(){
		return 'text';
	}
	
	public function getName(){
		return 'user_selector';
	}
	
}