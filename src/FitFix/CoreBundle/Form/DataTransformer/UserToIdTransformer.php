<?php

namespace FitFix\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FitFix\CoreBundle\Entity\User;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserToIdTransformer implements DataTransformerInterface {
	
	/**
	 * ObjectManager
	 * @var ObjectManager
	 */
	private $om;
	
	public function __construct(ObjectManager $om) {
		$this->om = $om;
	}
	
	/**
	 * Transforms the user to an ID
	 * (non-PHPdoc)
	 * @see \Symfony\Component\Form\DataTransformerInterface::reverseTransform()
	 */
	public function reverseTransform($value){
		
		if($value === null){
			return "";
		}
		
		($value instanceof User);
		
		return $value->getId();
		
	}
	
	/**
	 * Transforms an ID to a user
	 * (non-PHPdoc)
	 * @see \Symfony\Component\Form\DataTransformerInterface::transform()
	 */
	public function transform($value){
		
		if(!$value){
			return null;
		}
		
		$user = $this->om->getRepository('FitFixCoreBundle:User')
						 ->findOneBy(array('id' => $value));
		
		if($user == null){
			throw new TransformationFailedException(sprintf('An issue occoured whilst trying to transform user id "%s"', $value));
		}
		
		return $user;
		
	}
	
}