<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FitFix\CoreBundle\Form\DataTransformer\StringToDateTimeTransformer;


class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
            		$builder->create('start', 'text')->addModelTransformer(new StringToDateTimeTransformer())
    		)
            ->add(
            		$builder->create('end', 'text')->addModelTransformer(new StringToDateTimeTransformer())
    		)
            ->add('description')
            ->add('rsvp', 'text')
            ->add('from')
            ->add('to', 'entity', array('class' => 'FitFixCoreBundle:User'))
            ->add('address')
            ->add('invoice')
            ->add('packagepurchase', 'text', array("mapped" => false))
            ->add('client', 'text', array("mapped" => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Session'
        ));
    }

    public function getName()
    {
        return '';
    }
}
