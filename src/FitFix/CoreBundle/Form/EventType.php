<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FitFix\CoreBundle\Form\DataTransformer\StringToDateTimeTransformer;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
            		$builder->create('start', 'text')->addModelTransformer(new StringToDateTimeTransformer())
    		)
            ->add(
            		$builder->create('end', 'text')->addModelTransformer(new StringToDateTimeTransformer())
    		)
            ->add('location')
            ->add('description')
            ->add('rsvp', 'text')
            ->add('invitee', 'text')
            ->add('from')
            ->add('to')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Event'
        ));
    }

    public function getName()
    {
        return '';
    }
}
