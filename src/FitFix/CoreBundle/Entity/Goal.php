<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Goals
 *
 * @ORM\Table(name="goal")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\GoalRepository")
 * @ExclusionPolicy("all")
 */
class Goal
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"list", "details", "dashboard-client"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=true, name="description")
     * @Expose
     * @Groups({"list", "details", "dashboard-client"})
     * @Assert\NotBlank(
     *     message="Please enter a description"
     * )
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Client", inversedBy="goals")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set goal
     *
     * @param string $goal
     * @return Goals
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get goal
     *
     * @return string
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Goal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return Goal
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}