<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * @ORM\Entity
 */
class Discount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $code;
    
    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string")
     * @Expose()     
     */

    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="procent", type="string")
     * @Expose()     
     */

    private $procent;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string")
     * @Expose()     
     */

    private $country;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $createdAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set procent
     *
     * @param string $procent
     * @return Message
     */
    public function setProcent($procent)
    {
        $this->procent = $procent;
    
        return $this;
    }

    /**
     * Get procent
     *
     * @return string 
     */
    public function getProcent()
    {
        return $this->procent;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
   

    /**
     * Set code
     *
     * @param string $code
     * @return Letter
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

  


    /**
     * Set currency
     *
     * @param string $currency
     * @return Descont
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Descont
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Discount
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
}