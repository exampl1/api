<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\ReviewRepository")
 *
 * @ExclusionPolicy("all");
 */
class Review
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose()
     * @Groups({"trainer-profile", "review-list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="comment")
     * @Assert\NotBlank(
     *     message="Please enter a comment"
     * )
     *
     * @Expose()
     * @Groups({"trainer-profile", "review-list", "review-details"})
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="rating")
     * @Assert\GreaterThan(
     *     value = "0",
     *     message = "You must enter a number between 0 and 5"
     * )
     * @Assert\LessThan(
     *     value = "6",
     *     message = "You must enter a number between 0 and 5"
     * )
     *
     * @Expose()
     * @Groups({"trainer-profile", "review-list", "review-details"})
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Client", inversedBy="reviews")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     *
     * @Expose()
     * @Groups({"trainer-profile", "review-list", "review-details"})
     */
    private $client;

    /**
     * @var \Trainer
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="reviews")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     *
     * @Expose()
     * @Groups({"trainer-profile", "review-list", "review-details"})
     */
    private $trainer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Review
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Review
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return Review
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

	/**
	 *
	 * @return the integer
	 */
	public function getRating() {
		return $this->rating;
	}

	/**
	 *
	 * @param
	 *        	$rating
	 */
	public function setRating($rating) {
		$this->rating = $rating;
		return $this;
	}

}
