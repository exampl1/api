<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personal
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Personal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bloodType", type="string", length=255)
     */
    private $bloodType;

    /**
     * @var string
     *
     * @ORM\Column(name="docname", type="string", length=255)
     */
    private $docname;

    /**
     * @var string
     *
     * @ORM\Column(name="emergencycontact", type="string", length=255)
     */
    private $emergencycontact;

    /**
     * @var string
     *
     * @ORM\Column(name="emergencynumber", type="string", length=255)
     */
    private $emergencynumber;

    /**
     * @var string
     *
     * @ORM\Column(name="faxnumber", type="string", length=255)
     */
    private $faxnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="homephone", type="string", length=255)
     */
    private $homephone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobilephone", type="string", length=255)
     */
    private $mobilephone;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bloodType
     *
     * @param string $bloodType
     * @return Personal
     */
    public function setBloodType($bloodType)
    {
        $this->bloodType = $bloodType;
    
        return $this;
    }

    /**
     * Get bloodType
     *
     * @return string 
     */
    public function getBloodType()
    {
        return $this->bloodType;
    }

    /**
     * Set docname
     *
     * @param string $docname
     * @return Personal
     */
    public function setDocname($docname)
    {
        $this->docname = $docname;
    
        return $this;
    }

    /**
     * Get docname
     *
     * @return string 
     */
    public function getDocname()
    {
        return $this->docname;
    }

    /**
     * Set emergencycontact
     *
     * @param string $emergencycontact
     * @return Personal
     */
    public function setEmergencycontact($emergencycontact)
    {
        $this->emergencycontact = $emergencycontact;
    
        return $this;
    }

    /**
     * Get emergencycontact
     *
     * @return string 
     */
    public function getEmergencycontact()
    {
        return $this->emergencycontact;
    }

    /**
     * Set emergencynumber
     *
     * @param string $emergencynumber
     * @return Personal
     */
    public function setEmergencynumber($emergencynumber)
    {
        $this->emergencynumber = $emergencynumber;
    
        return $this;
    }

    /**
     * Get emergencynumber
     *
     * @return string 
     */
    public function getEmergencynumber()
    {
        return $this->emergencynumber;
    }

    /**
     * Set faxnumber
     *
     * @param string $faxnumber
     * @return Personal
     */
    public function setFaxnumber($faxnumber)
    {
        $this->faxnumber = $faxnumber;
    
        return $this;
    }

    /**
     * Get faxnumber
     *
     * @return string 
     */
    public function getFaxnumber()
    {
        return $this->faxnumber;
    }

    /**
     * Set homephone
     *
     * @param string $homephone
     * @return Personal
     */
    public function setHomephone($homephone)
    {
        $this->homephone = $homephone;
    
        return $this;
    }

    /**
     * Get homephone
     *
     * @return string 
     */
    public function getHomephone()
    {
        return $this->homephone;
    }

    /**
     * Set mobilephone
     *
     * @param string $mobilephone
     * @return Personal
     */
    public function setMobilephone($mobilephone)
    {
        $this->mobilephone = $mobilephone;
    
        return $this;
    }

    /**
     * Get mobilephone
     *
     * @return string 
     */
    public function getMobilephone()
    {
        return $this->mobilephone;
    }
}