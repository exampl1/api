<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\ExecutionContextInterface;
use JMS\Serializer\Annotation\Groups;
use FitFix\CoreBundle\Entity\MappedSuperclass\CalendarItemMappedSuperclass;

/**
 * @author gavinwilliams
 * An event associated with a calendar
 * 
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\EventRepository")
 */
class Event extends CalendarItemMappedSuperclass {
	

	/**
	 * The location of this event
	 * @var string
	 * @ORM\Column(type="string", name="location", nullable=false)
	 * @Assert\NotBlank()
	 *
	 * @Groups({"event-list", "event-details"})
	 *
	 * @Expose
	 */
	private $location;
	
	/**
	 * The person who has been invited to the event
	 * @var unknown
	 * 
	 * @ORM\Column(type="string", nullable=true)
	 * 
	 * @Groups({"event-list", "event-details"})
	 * @Expose();
	 */
	private $invitee;

	/**
	 * @return the string
	 */
	public function getLocation() {
		return $this->location;
	}
	
	/**
	 * @param string $location
	 */
	public function setLocation($location) {
		$this->location = $location;
		return $this;
	}
	

    /**
     * Set invitee
     *
     * @param string $invitee
     * @return Event
     */
    public function setInvitee($invitee)
    {
        $this->invitee = $invitee;
    
        return $this;
    }

    /**
     * Get invitee
     *
     * @return string 
     */
    public function getInvitee()
    {
        return $this->invitee;
    }
    
    public function __construct(){
    	$this->setRsvp('accepted');
    }
}