<?php

namespace FitFix\CoreBundle\Entity\MappedSuperclass;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\ExecutionContextInterface;
use JMS\Serializer\Annotation\Groups;
use FitFix\CoreBundle\Entity\User;

/**
 * @author gavinwilliams
 * @ORM\MappedSuperclass
 * @ExclusionPolicy("all")
 * 
 * @Assert\Callback(methods={"_isStartEndTimeValid", "_isToFromValid"})
 */
class CalendarItemMappedSuperclass {
	

	/**
	 * The ID of the event
	 * @var integer
	 * @ORM\Id()
	 * @ORM\Column(type="integer", name="id")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 *
	 * @Groups({"event-list", "event-details", "notification", "packagepurchase-details", "packagepurchase-list"})
	 *
	 * @Expose
	 */
	private $id;

	/**
	 * The start time of the event
	 * @var \DateTime
	 * @ORM\Column(type="datetime", name="start", nullable=false)
	 * @Assert\DateTime()
	 * @Assert\NotBlank()
	 *
	 * @Groups({"event-list", "event-details", "notification", "dashboard-pt"})
	 *
	 * @Expose
	 */
	private $start;
	
	/**
	 * The end time of the event
	 * @var \DateTime
	 * @ORM\Column(type="datetime", name="end", nullable=false)
	 * @Assert\DateTime()
	 * @Assert\NotBlank()
	 *
	 * @Groups({"event-list", "event-details", "notification", "dashboard-pt"})
	 *
	 * @Expose
	 */
	private $end;
	
	/**
	 * The user that this event is coming from
	 * @var User
	 * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\User")
	 * @ORM\JoinColumn(nullable=false)
	 *
	 * @Assert\Type(
	 * 	type="FitFix\CoreBundle\Entity\User"
	 * )
	 *
	 * @Assert\NotBlank()
	 *
	 * @Groups({"event-list", "event-details", "notification"})
	 *
	 * @Expose
	 */
	private $from;
	
	/**
	 * The user that this event is going to
	 * @var User
	 * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\User")
	 * @ORM\JoinColumn(nullable=true)
	 *
	 * @Assert\Type(
	 * 	type="FitFix\CoreBundle\Entity\User"
	 * )
	 *
	 * @Groups({"event-list", "event-details", "notification"})
	 *
	 * @Expose
	 */
	private $to;
	
	/**
	 * The event description
	 * @var string
	 * @ORM\Column(type="string", name="description", nullable=true)
	 * @Groups({"event-list", "event-details", "notification"})
	 *
	 * @Expose
	 */
	private $description;
	
	/**
	 * The current status of the event
	 * @var string
	 * @ORM\Column(type="string", name="rsvp", nullable=false)
	 * @Assert\Choice(
	 * 	choices={"accepted", "declined", "pending", "cancelled"}
	 * )
	 * @Assert\NotBlank()
	 *
	 * @Groups({"event-list", "event-details", "notification", "packagepurchase-details", "packagepurchase-list"})
	 *
	 * @Expose
	 */
	private $rsvp = "pending";
	
	/**
	 * @return the integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param  $id
	 */
	public function setId( $id) {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return the DateTime
	 */
	public function getStart() {
		return $this->start;
	}
	
	/**
	 * @param \DateTime $start
	 */
	public function setStart(\DateTime $start) {
		$this->start = $start;
		return $this;
	}
	
	/**
	 * @return the DateTime
	 */
	public function getEnd() {
		return $this->end;
	}
	
	/**
	 * @param \DateTime $end
	 */
	public function setEnd(\DateTime $end) {
		$this->end = $end;
		return $this;
	}
	
	/**
	 * @return User
	 */
	public function getFrom() {
		return $this->from;
	}
	
	/**
	 * @param User $from
	 */
	public function setFrom(User $from) {
		$this->from = $from;
		return $this;
	}
	
	/**
	 * @return User
	 */
	public function getTo() {
		return $this->to;
	}
	
	/**
	 * @param User $to
	 */
	public function setTo(User $to) {
		$this->to = $to;
		return $this;
	}	
	
	/**
	 * @return the string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return the string
	 */
	public function getRsvp() {
		return $this->rsvp;
	}
	
	/**
	 * @param string $rsvp
	 */
	public function setRsvp($rsvp) {
	
		if($rsvp === null){
			$rsvp = "pending";
		}
	
		$this->rsvp = $rsvp;
		return $this;
	}
	
	/**
	 * Checks to see whether the start time is before the end time and visa versa
	 * @param ExecutionContextInterface $context
	 */
	public function _isStartEndTimeValid(ExecutionContextInterface $context){
	
		if(($this->start >= $this->end) || ($this->end <= $this->start)){
			$context->addViolationAt('start', 'The start time must be before the end time and cannot be the same');
			$context->addViolationAt('end', 'The end time must be after the start time and cannot be the same');
		}
	
	}
	
	/**
	 * Checks to see whether a user has invited themselves to an event
	 * @param ExecutionContextInterface $context
	 */
	public function _isToFromValid(ExecutionContextInterface $context){
		
		if(!$this->getTo()){
			return;
		}
		
		if($this->getFrom()->getId() == $this->getTo()->getId()){
			$context->addViolationAt('from', 'You cannot invite yourself to an event');
			$context->addViolationAt('to', 'You cannot invite yourself to an event');
		}
	
	}
	
}