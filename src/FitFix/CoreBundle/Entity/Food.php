<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as gedmo;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Food
 *
 * @ORM\Table(name="food")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\FoodRepository")
 * @ExclusionPolicy("all")
 */
class Food
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="float", name="calories")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $calories;

    /**
     * @var integer
     *
     * @ORM\Column(type="float", name="carbs")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $carbs;

    /**
     * @var integer
     *
     * @ORM\Column(type="float", name="proteins")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $proteins;

    /**
     * @var integer
     *
     * @ORM\Column(type="float", name="fats")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $fats;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="category")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="unit")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $unit;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", name="base")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $base;



    /**
     * Constructor
     */
    public function __construct()
    {
        //
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Food
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set preparation
     *
     * @param string $preparation
     * @return Food
     */
    public function setPreparation($preparation)
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * Get preparation
     *
     * @return string
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set calories
     *
     * @param string $calories
     * @return Food
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;

        return $this;
    }

    /**
     * Get calories
     *
     * @return string
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set carbs
     *
     * @param string $carbs
     * @return Food
     */
    public function setCarbs($carbs)
    {
        $this->carbs = $carbs;

        return $this;
    }

    /**
     * Get carbs
     *
     * @return string
     */
    public function getCarbs()
    {
        return $this->carbs;
    }

    /**
     * Set proteins
     *
     * @param string $proteins
     * @return Food
     */
    public function setProteins($proteins)
    {
        $this->proteins = $proteins;

        return $this;
    }

    /**
     * Get proteins
     *
     * @return string
     */
    public function getProteins()
    {
        return $this->proteins;
    }

    /**
     * Set fats
     *
     * @param string $fats
     * @return Food
     */
    public function setFats($fats)
    {
        $this->fats = $fats;

        return $this;
    }

    /**
     * Get fats
     *
     * @return string
     */
    public function getFats()
    {
        return $this->fats;
    }


    /**
     * Set unit
     *
     * @param string $unit
     * @return Food
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set base
     *
     * @param string $base
     * @return Food
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }   
}