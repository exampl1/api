<?php

namespace FitFix\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CommentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository {

    public function findById($id) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $result = $qb->select('c')
                ->from('FitFixCoreBundle:User', 'u')
                ->where($qb->expr()->eq('u.id', $id))
                ->getQuery()
                ->getResult();
        return $result;
    }

}
