<?php

namespace FitFix\CoreBundle\Entity;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Notifications sent from various actions to a user
 * @author gavinwilliams
 * @ORM\Table()
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class Notification {
	
	/**
	 * The notification id
	 * @var int
	 * @ORM\Column(type="integer", name="id")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * 
	 * @Expose()
	 * @Groups({"dashboard-pt", "dashboard-client"})
	 */
	private $id;
	
	/**
	 * The notification type
	 * @var string
	 * @ORM\Column(type="string", name="type")
	 * 
	 * @Expose()
	 * @Assert\NotBlank()
	 * @Assert\NotNull()
	 * @Groups({"dashboard-pt", "dashboard-client"})
	 */
	private $type;
	
	/**
	 * The action of the notification (persist/update)
	 * @var string
	 * @ORM\Column(type="string", name="action")
	 * 
	 * @Expose()
	 * 
	 * @Groups({"dashboard-pt", "dashboard-client"})
	 */
	private $action;
	
	/**
	 * The date the notification was created
	 * @var \DateTime
	 * 
	 * @ORM\Column(type="datetime", name="createdstamp")
	 * 
	 * @Expose()
	 * @Assert\DateTime()
	 * @Assert\NotBlank()
	 * 
	 * @Groups({"dashboard-pt", "dashboard-client"})
	 */
	private $created;
	
	/**
	 * The date the notification was read
	 * @var \DateTime
	 * 
	 * @ORM\Column(type="datetime", name="readstamp", nullable=true)
	 * 
	 * @Expose()
	 * @Assert\DateTime()
	 * 
	 * @Groups({"dashboard-pt", "dashboard-client"})
	 */
	private $read;
	
	/**
	 * The user for this notification
	 * @var User
	 * 
	 * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\User")
	 */
	private $user;
	
	/**
	 * The data associated with the notification,
	 * should be a json string
	 * @var string
	 * 
	 * @ORM\Column(type="text", name="data")
	 * 
	 * @Expose()
	 * @Assert\NotBlank()
	 * 
	 * @Groups({"dashboard-pt", "dashboard-client"})
	 */
	private $data;

	public function __construct(){
		$this->setCreated(new \DateTime());
	}
	
	/**
	 * @return the int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param  $id
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}


	/**
	 * @return the string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * @param string $type
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}

	public function getAction() {
		return $this->action;
	}
	
	public function setAction($action) {
		$this->action = $action;
		return $this;
	}
			
	/**
	 * @return the DateTime
	 */
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * @param \DateTime $created
	 */
	public function setCreated(\DateTime $created) {
		$this->created = $created;
		return $this;
	}


	/**
	 * @return the DateTime
	 */
	public function getRead() {
		return $this->read;
	}
	
	/**
	 * @param \DateTime $read
	 */
	public function setRead(\DateTime $read) {
		$this->read = $read;
		return $this;
	}
		
	/**
	 * @return the User
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param User $user
	 */
	public function setUser(User $user) {
		$this->user = $user;
		return $this;
	}
	
	/**
	 * @return the string
	 */
	public function getData() {
		return $this->data;
	}
	
	/**
	 * @param string $data
	 */
	public function setData($data) {
		$this->data = $data;
		return $this;
	}
	
	
}