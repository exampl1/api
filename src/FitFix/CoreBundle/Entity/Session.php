<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use FitFix\CoreBundle\Entity\MappedSuperclass\CalendarItemMappedSuperclass;

/**
 * A session event
 * @author gavinwilliams
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\SessionRepository")
 * 
 * @ExclusionPolicy("all")
 */
class Session extends CalendarItemMappedSuperclass {
	
	/**
	 * The address of the session
	 * @var Address
	 * 
	 * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Address")
	 * @Assert\NotBlank()
	 * 
	 * @Groups({"event-details", "notification"})
	 * 
	 * @Expose
	 */
	private $address;
	
	/**
	 * The invoice associated with this session
	 * @var Invoice
	 * 
	 * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\Invoice")
	 * @Assert\Type(
	 * 	type="FitFix\CoreBundle\Entity\Invoice"
	 * )
	 * 
	 * @Groups({"event-list", "event-details", "notification"})
	 * 
	 * @Expose
	 */
	private $invoice;

	/**
	 * @return the Address
	 */
	public function getAddress() {
		return $this->address;
	}
	
	/**
	 * @param Address $location
	 */
	public function setAddress(Address $address) {
		$this->address = $address;
		return $this;
	}
	
	/**
	 * @return the Invoice
	 */
	public function getInvoice() {
		return $this->invoice;
	}
	
	/**
	 * @param Invoice $invoice
	 */
	public function setInvoice(Invoice $invoice) {
		$this->invoice = $invoice;
		return $this;
	}
	
	
	
}