<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

use JMS\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\Criteria;
use JMS\Serializer\Annotation\VirtualProperty;
use Buzz\Exception\RuntimeException;
use Symfony\Component\Validator\Exception\ValidatorException;

use JMS\Serializer\Annotation\SerializedName;

/**
 * PackagePurchase
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\PackagePurchaseRepository")
 * @ExclusionPolicy("all")
 */
class PackagePurchase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Expose()
     * 
     * @Groups({"packagepurchase-list", "packagepurchase-details"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="purchaseDate", type="datetimetz")
     * 
     * @Expose()
     * 
     * @Groups({"packagepurchase-list", "packagepurchase-details"})
     * @SerializedName("purchaseDate")
     */
    private $purchaseDate;
    
    /**
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Session")
     * @JoinTable(
     * 	name="packagepurchase_sessions",
     * 	joinColumns={@JoinColumn(name="packagepurchase_id", referencedColumnName="id")},
     * 	inverseJoinColumns={@JoinColumn(name="session_id", referencedColumnName="id", unique=true)}
     * )
     * 
     * @Expose()
     * 
     * @Groups({"packagepurchase-list", "packagepurchase-details"})
     */
    private $sessions;
    
    /**
     * @var Invoice
     * 
     * @ORM\OneToOne(targetEntity="Invoice", cascade={"persist"})
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * 
     * @Expose()
     * 
     * @Groups({"packagepurchase-details", "packagepurchase-list"})
     */
    private $invoice;
    
    /**
     * @var Client
     * 
     * @ORM\ManyToOne(targetEntity="Client")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * 
     * @Expose()
     * 
     * @Groups({"packagepurchase-details"})
     */
    private $client;
    
    /**
     * @var Package
     * 
     * @ORM\ManyToOne(targetEntity="Package")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * 
     * @Expose
     * 
     * @Groups({"packagepurchase-list", "packagepurchase-details"})
     */
    private $package;

    /**
     * Constructor
     */
    public function __construct(){
    	$this->setPurchaseDate(new \DateTime());
    	$this->sessions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purchaseDate
     *
     * @param \DateTime $purchaseDate
     * @return PackagePurchase
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
    
        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime 
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * Add sessions
     *
     * @param \FitFix\CoreBundle\Entity\Session $sessions
     * @return PackagePurchase
     */
    public function addSession(\FitFix\CoreBundle\Entity\Session $sessions)
    {
    	
    	if($this->getAvailableSessions() <= 0){
    		throw new RuntimeException('No more sessions available for package');
    	}
    	
        $this->sessions[] = $sessions;
    
        return $this;
    }

    /**
     * Remove sessions
     *
     * @param \FitFix\CoreBundle\Entity\Session $sessions
     */
    public function removeSession(\FitFix\CoreBundle\Entity\Session $sessions)
    {
        $this->sessions->removeElement($sessions);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set invoice
     *
     * @param \FitFix\CoreBundle\Entity\Invoice $invoice
     * @return PackagePurchase
     */
    public function setInvoice(\FitFix\CoreBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;
    
        return $this;
    }

    /**
     * Get invoice
     *
     * @return \FitFix\CoreBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return PackagePurchase
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set package
     *
     * @param \FitFix\CoreBundle\Entity\Package $package
     * @return PackagePurchase
     */
    public function setPackage(\FitFix\CoreBundle\Entity\Package $package = null)
    {
        $this->package = $package;
    
        return $this;
    }

    /**
     * Get package
     *
     * @return \FitFix\CoreBundle\Entity\Package 
     */
    public function getPackage()
    {
        return $this->package;
    }
    
    /**
     * Gets a list of available sessions for this package purchase
     * 
     * @return number
     * 
     * @VirtualProperty
     * @SerializedName("availableSessions")
     * 
     * @Groups({"packagepurchase-list", "packagepurchase-details"})
     */
    public function getAvailableSessions(){
    	 
    	$acceptedSessions = $this->getSessions()->filter(function (Session $session){
    		$rsvp = $session->getRsvp();
			return $rsvp == 'accepted' || $rsvp == 'pending';
    	});
    	    	
    	return ($this->getPackage()->getNumberOfSessions() - $acceptedSessions->count());
    }
}