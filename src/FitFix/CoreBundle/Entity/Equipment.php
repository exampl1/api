<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Exercise
 *
 * @ORM\Table(name="equipment")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Equipment
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FitFix\CoreBundle\Entity\Exercise", mappedBy="equipment", cascade={"persist"})
     */
    private $exercises;

    /**
     * @var The URL for the image
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Expose
     * @Groups({"list", "details"})
     */
    private $image;
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Equipment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set exercise
     *
     * @param \FitFix\CoreBundle\Entity\Exercise $exercise
     * @return Equipment
     */
    public function setExercise(\FitFix\CoreBundle\Entity\Exercise $exercise = null)
    {
        $this->exercise = $exercise;

        return $this;
    }

    /**
     * Get exercise
     *
     * @return \FitFix\CoreBundle\Entity\Exercise
     */
    public function getExercise()
    {
        return $this->exercise;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->exercise = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add exercise
     *
     * @param \FitFix\CoreBundle\Entity\Exercise $exercise
     * @return Equipment
     */
    public function addExercise(\FitFix\CoreBundle\Entity\Exercise $exercise)
    {
        $this->exercise[] = $exercise;

        return $this;
    }

    /**
     * Remove exercise
     *
     * @param \FitFix\CoreBundle\Entity\Exercise $exercise
     */
    public function removeExercise(\FitFix\CoreBundle\Entity\Exercise $exercise)
    {
        $this->exercise->removeElement($exercise);
    }

    /**
     * Get exercises
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExercises()
    {
        return $this->exercises;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Equipment
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}