<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use Gedmo\Mapping\Annotation as gedmo;
use Gedmo\Timestampable\Timestampable;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"address-list", "address-details", "sessiontypelocation-details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="name")
     * @Assert\NotBlank(
     *     message="Please enter a name"
     * )
     * @Expose
     * @Groups({"address-list", "address-details", "sessiontypelocation-details"})
     */
    private $name;

    /**
     * @var string $address1
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="address1")
     * @Expose
     * @Groups({"address-details", "address-list"})
     */
    protected $address1;

    /**
     * @var string $address2
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="address2")
     * @Expose
     * @Groups({"address-details", "address-list", "sessiontypelocation-details"})
     */
    protected $address2;

    /**
     * @var string $town
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="town")
     * @Expose
     * @Groups({"address-details", "address-list"})
     */
    protected $town;

    /**
     * @var string $postcode
     *
     * @ORM\Column(type="string", length=16, nullable=false, name="postcode")
     * @Expose
     * @Groups({"address-details", "address-list"})
     */
    protected $postcode;

    /**
     * @var string $country
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="country")
     * @Expose
     * @Groups({"address-details", "address-list"})
     */
    protected $country;

    /**
     * @var string $latitude
     *
     * @ORM\Column(type="float", nullable=true, name="latitude", precision=10, scale=6)
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }} type."
     * )
     * @Expose
     * @Groups({"address-details", "address-list"})
     */
    protected $latitude;

    /**
     * @var string $longitude
     *
     * @ORM\Column(type="float", nullable=true, name="longitude", precision=10, scale=6)
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }} type."
     * )
     * @Expose
     * @Groups({"address-details", "address-list"})
     */
    protected $longitude;
    
    /**
     * @var Trainer
     * 
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="locations")
     * @Expose
     */
    protected $trainer;
    
    /**
     * Whether the address is enabled or disabled
     * @var unknown
     * 
     * @ORM\Column(type="boolean", nullable=false)
     * @Assert\NotBlank()
     * @Expose()
     * @Groups({"address-details", "address-list"})
     */
    protected $enabled;
    
    /**
     * The formated address
     * @var unknown
     * 
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Expose()
     * @Groups({"address-details", "address-list"})
     * @SerializedName("formattedAddress")
     */
    protected $formattedAddress;
    
    public function __construct()
    {
        $this->name = '';
        $this->address1 = '';
        $this->address2 = '';
        $this->town = '';
        $this->county = '';
        $this->postcode = '';
        $this->country = '';
        $this->latitude = 0.0;
        $this->longitude = 0.0;
        $this->enabled = true;
    }

    /**
     * Implodes an array of address properties
     * @return string
     */
    public function __toString(){
    	return implode(' ', array($this->getName(), $this->getAddress1(), $this->getAddress2(), $this->getCountry(), $this->getPostcode()));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Address
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Address
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Address
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set town
     *
     * @param string $town
     * @return Address
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Address
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Address
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Address
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

	public function getTrainer() {
		return $this->trainer;
	}
	
	public function setTrainer(Trainer $trainer) {
		$this->trainer = $trainer;
		return $this;
	}
	public function getEnabled() {
		return $this->enabled;
	}
	public function setEnabled($enabled) {
		$this->enabled = $enabled;
		return $this;
	}
	public function getFormattedAddress() {
		return $this->formattedAddress;
	}
	public function setFormattedAddress($formattedAddress) {
		$this->formattedAddress = $formattedAddress;
		return $this;
	}
	
	
	
}
