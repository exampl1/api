<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\SerializedName;

/**
 * WorkoutLog
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\WorkoutLogRepository")
 * @ExclusionPolicy("all")
 */
class WorkoutLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Expose()
     * @Groups({"workoutLog-list", "workoutLog-details", "dashboard-client"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateLogged", type="datetime")
     * 
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * 
     * @Expose()
     * @Groups({"workoutLog-list", "workoutLog-details", "dashboard-client"})
     * 
     * @SerializedName("dateLogged")
     */
    private $dateLogged;
    
    /**
     * @var Workout
     * 
     * @ORM\ManyToOne(targetEntity="Workout", fetch="EXTRA_LAZY")
     * 
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * 
     * @Expose()
     * @Groups({"workoutLog-list", "workoutLog-details"})
     */
    private $workout;
    
    /**
     * @var Client
     * 
     * @ORM\ManyToOne(targetEntity="Client", fetch="EXTRA_LAZY")
     * 
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * 
     * @Expose()
     * @Groups({"workoutLog-details"})
     */
    private $client;
    
    /**
     * @var Trainer
     * 
     * @ORM\ManyToOne(targetEntity="Trainer", fetch="EXTRA_LAZY")
     * 
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * 
     * @Expose()
     * @Groups({"workoutLog-details"})
     */
    private $trainer;

    public function __construct() {
        $this->setDateLogged(new \DateTime());
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateLogged
     *
     * @param \DateTime $dateLogged
     * @return WorkoutLog
     */
    public function setDateLogged(\DateTime $dateLogged)
    {
        $this->dateLogged = $dateLogged;
    
        return $this;
    }

    /**
     * Get dateLogged
     *
     * @return \DateTime 
     */
    public function getDateLogged()
    {
        return $this->dateLogged;
    }
    
    /**
     *
     * @return the Workout
     */
    public function getWorkout() {
        return $this->workout;
    }
    
    /**
     *
     * @param Workout $workout          
     */
    public function setWorkout(Workout $workout) {
        $this->workout = $workout;
        return $this;
    }
    
    /**
     *
     * @return the Client
     */
    public function getClient() {
        return $this->client;
    }
    
    /**
     *
     * @param Client $client            
     */
    public function setClient(Client $client) {
        $this->client = $client;
        return $this;
    }
    
    /**
     *
     * @return the Trainer
     */
    public function getTrainer() {
        return $this->trainer;
    }
    
    /**
     *
     * @param Trainer $trainer          
     */
    public function setTrainer(Trainer $trainer) {
        $this->trainer = $trainer;
        return $this;
    }
    
}