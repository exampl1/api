<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Meal
 *
 * @ORM\Table(name="meal")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\MealRepository")
 * @ExclusionPolicy("all")
 */
class Meal
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="type")
     * @Expose
     * @Groups({"list", "details"})
     * @Assert\NotBlank(
     *     message="Please enter a type"
     * )
     */
    private $type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Mealplan", cascade={"persist"})
     * @ORM\JoinColumn(name="mealplan_id", referencedColumnName="id")
     * @Groups({"list", "details"})
     * @Groups({"details"})
     */
    private $mealplan;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Food", cascade={"persist"})
     * @ORM\JoinColumn(name="food_id", referencedColumnName="id")
     * @Expose
     * @Groups({"list", "details"})
     */
    private $food;

     /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="quantity")
     * @Expose
     * @Groups({"list", "details"})
     * @Assert\NotBlank(
     *     message="Please enter a quantity"
     * )
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="calories")
     * @Expose
     * @Groups({"list", "details"})
     * @Assert\NotBlank(
     *     message="Please enter a quantity"
     * )
     */
    private $calories;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="position")
     * @Expose
     * @Groups({"list", "details"})
     * @Assert\NotBlank(
     *     message="Please enter a quantity"
     * )
     */
    private $position;


    //private $breakfast;

    //private $lunch;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Meal 
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

     /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Meal 
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set mealplan
     *
     * @param \FitFix\CoreBundle\Entity\Mealplan $mealplan
     * @return Meal
     */
    public function setMealplan($mealplan)
    {
        $this->mealplan = $mealplan;

        return $this;
    }

    /**
     * Set food
     *
     * @param \FitFix\CoreBundle\Entity\Food $food
     * @return Meal
     */
    public function setFood($food)
    {
        $this->food = $food;

        return $this;
    }

    /**
     * Set calories
     *
     * @param string $description
     * @return Meal
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;

        return $this;
    }

    /**
     * Get calories
     *
     * @return string
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Meal
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }
}