<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;
use Rhumsaa\Uuid\Uuid;

/**
 * Workouts
 *
 * @ORM\Table(name="workout")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\WorkoutRepository")
 * @ExclusionPolicy("all")
 */
class Workout
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"workout-list", "workout-details", "workoutLog-list", "workoutLog-details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     * @Expose
     * @Groups({"workout-list", "workout-details", "workoutLog-list", "workoutLog-details"})
     * @Assert\NotBlank(
     *     message="Please enter a name"
     * )
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(type="datetime", length=128, nullable=false, name="created")
     * @Expose
     * @Groups({"workout-list", "workout-details", "workoutLog-details"})
     * @Assert\NotBlank(
     *     message="Please enter a created date"
     * )
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Client", inversedBy="workouts")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * 
     * @Expose
     * @Groups({"workout-details"})
     */
    private $client;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Intensity", cascade={"persist"})
     * @ORM\JoinColumn(name="intensity_id", referencedColumnName="id")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $intensity;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="FitFix\CoreBundle\Entity\Step", 
     *     mappedBy="warmup", 
     *     orphanRemoval=true, 
     *     cascade={"persist","remove"}
     * )
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $warmup;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="FitFix\CoreBundle\Entity\Step", 
     *     mappedBy="workout", 
     *     orphanRemoval=true, 
     *     cascade={"persist","remove"}
     * )
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $workout;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="FitFix\CoreBundle\Entity\Step", 
     *     mappedBy="warmdown", 
     *     orphanRemoval=true, 
     *     cascade={"persist","remove"}
     * )
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $warmdown;
    
    /**
     * @var Trainer
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="workouts")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     */
    private $trainer;
    
    /**
     * @var unknown
     * 
     * @ORM\Column(type="boolean")
     * 
     * @Expose()
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $active;
    
    /**
     * @var UUID
     * 
     * @ORM\Column(type="string", name="uuid")
     * 
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $uuid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
        $this->warmup = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workout = new \Doctrine\Common\Collections\ArrayCollection();
        $this->warmdown = new \Doctrine\Common\Collections\ArrayCollection();
        $this->active = true;
    }

    public function __toString(){
        return $this->getName();
    }
    
    public function __clone(){
        if($this->id){
            $this->id = null;
            $this->client = null;
        }
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Workout
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return Workout
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set intensity
     *
     * @param \FitFix\CoreBundle\Entity\Intensity $intensity
     * @return Workout
     */
    public function setIntensity(\FitFix\CoreBundle\Entity\Intensity $intensity = null)
    {
        $this->intensity = $intensity;

        return $this;
    }

    /**
     * Get intensity
     *
     * @return \FitFix\CoreBundle\Entity\Intensity
     */
    public function getIntensity()
    {
        return $this->intensity;
    }

    /**
     * Add warmup
     *
     * @param \FitFix\CoreBundle\Entity\Step $warmup
     * @return Workout
     */
    public function addWarmup(\FitFix\CoreBundle\Entity\Step $warmup)
    {
        $warmup->setWarmup($this);
        $this->warmup[] = $warmup;

        return $this;
    }

    /**
     * Remove warmup
     *
     * @param \FitFix\CoreBundle\Entity\Step $warmup
     */
    public function removeWarmup(\FitFix\CoreBundle\Entity\Step $warmup)
    {
        $this->warmup->removeElement($warmup);
    }

    /**
     * Get warmup
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWarmup()
    {
        return $this->warmup;
    }

    /**
     * Add workout
     *
     * @param \FitFix\CoreBundle\Entity\Step $workout
     * @return Workout
     */
    public function addWorkout(\FitFix\CoreBundle\Entity\Step $workout)
    {
        $workout->setWorkout($this);
        $this->workout[] = $workout;

        return $this;
    }

    /**
     * Remove workout
     *
     * @param \FitFix\CoreBundle\Entity\Step $workout
     */
    public function removeWorkout(\FitFix\CoreBundle\Entity\Step $workout)
    {
        $this->workout->removeElement($workout);
    }

    /**
     * Get workout
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkout()
    {
        return $this->workout;
    }

    /**
     * Add warmdown
     *
     * @param \FitFix\CoreBundle\Entity\Step $warmdown
     * @return Workout
     */
    public function addWarmdown(\FitFix\CoreBundle\Entity\Step $warmdown)
    {
        $warmdown->setWarmdown($this);
        $this->warmdown[] = $warmdown;

        return $this;
    }

    /**
     * Remove warmdown
     *
     * @param \FitFix\CoreBundle\Entity\Step $warmdown
     */
    public function removeWarmdown(\FitFix\CoreBundle\Entity\Step $warmdown)
    {
        $this->warmdown->removeElement($warmdown);
    }

    /**
     * Get warmdown
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWarmdown()
    {
        return $this->warmdown;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Workout
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Workout
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;
    
        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer 
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Workout
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }
    public function getActive() {
        return $this->active;
    }
    public function setActive($active) {
        $this->active = $active;
        return $this;
    }
    
}