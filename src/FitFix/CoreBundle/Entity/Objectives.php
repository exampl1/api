<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Objectives
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\ObjectivesRepository")
 * @ExclusionPolicy("all")
 */
class Objectives
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
	 */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="actionsAway", type="text")
     * @Expose
	 */
    private $actionsAway;

    /**
     * @var string
     *
     * @ORM\Column(name="actionsTowards", type="text")
     * @Expose
	 */
    private $actionsTowards;

    /**
     * @var string
     *
     * @ORM\Column(name="activitiesListFive", type="text")
     * @Expose
	 */
    private $activitiesListFive;

    /**
     * @var string
     *
     * @ORM\Column(name="activitiesListFour", type="text")
     * @Expose
	 */
    private $activitiesListFour;

    /**
     * @var string
     *
     * @ORM\Column(name="activitiesListOne", type="text")
     * @Expose
 	 */
    private $activitiesListOne;

    /**
     * @var string
     *
     * @ORM\Column(name="activitiesListSix", type="text")
     * @Expose
	 */
    private $activitiesListSix;

    /**
     * @var string
     *
     * @ORM\Column(name="activitiesListThree", type="text")
     * @Expose
	 */
    private $activitiesListThree;

    /**
     * @var string
     *
     * @ORM\Column(name="activitiesListTwo", type="text")
     * @Expose
	 */
    private $activitiesListTwo;

    /**
     * @var string
     *
     * @ORM\Column(name="assessEmotions", type="text")
     * @Expose
	 */
    private $assessEmotions;

    /**
     * @var string
     *
     * @ORM\Column(name="emotionsReach", type="text")
     * @Expose
	 */
    private $emotionsReach;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListEight", type="text")
     * @Expose
	 */
    private $fullfillmentsListEight;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListFive", type="text")
     * @Expose
	 */
    private $fullfillmentsListFive;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListFour", type="text")
     * @Expose
	 */
    private $fullfillmentsListFour;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListNine", type="text")
     * @Expose
	 */
    private $fullfillmentsListNine;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListOne", type="text")
     * @Expose
	 */
    private $fullfillmentsListOne;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListSeven", type="text")
     * @Expose
	 */
    private $fullfillmentsListSeven;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListSix", type="text")
     * @Expose
	 */
    private $fullfillmentsListSix;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListThree", type="text")
     * @Expose
	 */
    private $fullfillmentsListThree;

    /**
     * @var string
     *
     * @ORM\Column(name="fullfillmentsListTwo", type="text")
     * @Expose
	 */
    private $fullfillmentsListTwo;

    /**
     * @var string
     *
     * @ORM\Column(name="primaryObjective", type="text")
     * @Expose
	 */
    private $primaryObjective;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actionsAway
     *
     * @param string $actionsAway
     * @return Objectives
     */
    public function setActionsAway($actionsAway)
    {
        $this->actionsAway = $actionsAway;
    
        return $this;
    }

    /**
     * Get actionsAway
     *
     * @return string 
     */
    public function getActionsAway()
    {
        return $this->actionsAway;
    }

    /**
     * Set actionsTowards
     *
     * @param string $actionsTowards
     * @return Objectives
     */
    public function setActionsTowards($actionsTowards)
    {
        $this->actionsTowards = $actionsTowards;
    
        return $this;
    }

    /**
     * Get actionsTowards
     *
     * @return string 
     */
    public function getActionsTowards()
    {
        return $this->actionsTowards;
    }

    /**
     * Set activitiesListFive
     *
     * @param string $activitiesListFive
     * @return Objectives
     */
    public function setActivitiesListFive($activitiesListFive)
    {
        $this->activitiesListFive = $activitiesListFive;
    
        return $this;
    }

    /**
     * Get activitiesListFive
     *
     * @return string 
     */
    public function getActivitiesListFive()
    {
        return $this->activitiesListFive;
    }

    /**
     * Set activitiesListFour
     *
     * @param string $activitiesListFour
     * @return Objectives
     */
    public function setActivitiesListFour($activitiesListFour)
    {
        $this->activitiesListFour = $activitiesListFour;
    
        return $this;
    }

    /**
     * Get activitiesListFour
     *
     * @return string 
     */
    public function getActivitiesListFour()
    {
        return $this->activitiesListFour;
    }

    /**
     * Set activitiesListOne
     *
     * @param string $activitiesListOne
     * @return Objectives
     */
    public function setActivitiesListOne($activitiesListOne)
    {
        $this->activitiesListOne = $activitiesListOne;
    
        return $this;
    }

    /**
     * Get activitiesListOne
     *
     * @return string 
     */
    public function getActivitiesListOne()
    {
        return $this->activitiesListOne;
    }

    /**
     * Set activitiesListSix
     *
     * @param string $activitiesListSix
     * @return Objectives
     */
    public function setActivitiesListSix($activitiesListSix)
    {
        $this->activitiesListSix = $activitiesListSix;
    
        return $this;
    }

    /**
     * Get activitiesListSix
     *
     * @return string 
     */
    public function getActivitiesListSix()
    {
        return $this->activitiesListSix;
    }

    /**
     * Set activitiesListThree
     *
     * @param string $activitiesListThree
     * @return Objectives
     */
    public function setActivitiesListThree($activitiesListThree)
    {
        $this->activitiesListThree = $activitiesListThree;
    
        return $this;
    }

    /**
     * Get activitiesListThree
     *
     * @return string 
     */
    public function getActivitiesListThree()
    {
        return $this->activitiesListThree;
    }

    /**
     * Set activitiesListTwo
     *
     * @param string $activitiesListTwo
     * @return Objectives
     */
    public function setActivitiesListTwo($activitiesListTwo)
    {
        $this->activitiesListTwo = $activitiesListTwo;
    
        return $this;
    }

    /**
     * Get activitiesListTwo
     *
     * @return string 
     */
    public function getActivitiesListTwo()
    {
        return $this->activitiesListTwo;
    }

    /**
     * Set assessEmotions
     *
     * @param string $assessEmotions
     * @return Objectives
     */
    public function setAssessEmotions($assessEmotions)
    {
        $this->assessEmotions = $assessEmotions;
    
        return $this;
    }

    /**
     * Get assessEmotions
     *
     * @return string 
     */
    public function getAssessEmotions()
    {
        return $this->assessEmotions;
    }

    /**
     * Set emotionsReach
     *
     * @param string $emotionsReach
     * @return Objectives
     */
    public function setEmotionsReach($emotionsReach)
    {
        $this->emotionsReach = $emotionsReach;
    
        return $this;
    }

    /**
     * Get emotionsReach
     *
     * @return string 
     */
    public function getEmotionsReach()
    {
        return $this->emotionsReach;
    }

    /**
     * Set fullfillmentsListEight
     *
     * @param string $fullfillmentsListEight
     * @return Objectives
     */
    public function setFullfillmentsListEight($fullfillmentsListEight)
    {
        $this->fullfillmentsListEight = $fullfillmentsListEight;
    
        return $this;
    }

    /**
     * Get fullfillmentsListEight
     *
     * @return string 
     */
    public function getFullfillmentsListEight()
    {
        return $this->fullfillmentsListEight;
    }

    /**
     * Set fullfillmentsListFive
     *
     * @param string $fullfillmentsListFive
     * @return Objectives
     */
    public function setFullfillmentsListFive($fullfillmentsListFive)
    {
        $this->fullfillmentsListFive = $fullfillmentsListFive;
    
        return $this;
    }

    /**
     * Get fullfillmentsListFive
     *
     * @return string 
     */
    public function getFullfillmentsListFive()
    {
        return $this->fullfillmentsListFive;
    }

    /**
     * Set fullfillmentsListFour
     *
     * @param string $fullfillmentsListFour
     * @return Objectives
     */
    public function setFullfillmentsListFour($fullfillmentsListFour)
    {
        $this->fullfillmentsListFour = $fullfillmentsListFour;
    
        return $this;
    }

    /**
     * Get fullfillmentsListFour
     *
     * @return string 
     */
    public function getFullfillmentsListFour()
    {
        return $this->fullfillmentsListFour;
    }

    /**
     * Set fullfillmentsListNine
     *
     * @param string $fullfillmentsListNine
     * @return Objectives
     */
    public function setFullfillmentsListNine($fullfillmentsListNine)
    {
        $this->fullfillmentsListNine = $fullfillmentsListNine;
    
        return $this;
    }

    /**
     * Get fullfillmentsListNine
     *
     * @return string 
     */
    public function getFullfillmentsListNine()
    {
        return $this->fullfillmentsListNine;
    }

    /**
     * Set fullfillmentsListOne
     *
     * @param string $fullfillmentsListOne
     * @return Objectives
     */
    public function setFullfillmentsListOne($fullfillmentsListOne)
    {
        $this->fullfillmentsListOne = $fullfillmentsListOne;
    
        return $this;
    }

    /**
     * Get fullfillmentsListOne
     *
     * @return string 
     */
    public function getFullfillmentsListOne()
    {
        return $this->fullfillmentsListOne;
    }

    /**
     * Set fullfillmentsListSeven
     *
     * @param string $fullfillmentsListSeven
     * @return Objectives
     */
    public function setFullfillmentsListSeven($fullfillmentsListSeven)
    {
        $this->fullfillmentsListSeven = $fullfillmentsListSeven;
    
        return $this;
    }

    /**
     * Get fullfillmentsListSeven
     *
     * @return string 
     */
    public function getFullfillmentsListSeven()
    {
        return $this->fullfillmentsListSeven;
    }

    /**
     * Set fullfillmentsListSix
     *
     * @param string $fullfillmentsListSix
     * @return Objectives
     */
    public function setFullfillmentsListSix($fullfillmentsListSix)
    {
        $this->fullfillmentsListSix = $fullfillmentsListSix;
    
        return $this;
    }

    /**
     * Get fullfillmentsListSix
     *
     * @return string 
     */
    public function getFullfillmentsListSix()
    {
        return $this->fullfillmentsListSix;
    }

    /**
     * Set fullfillmentsListThree
     *
     * @param string $fullfillmentsListThree
     * @return Objectives
     */
    public function setFullfillmentsListThree($fullfillmentsListThree)
    {
        $this->fullfillmentsListThree = $fullfillmentsListThree;
    
        return $this;
    }

    /**
     * Get fullfillmentsListThree
     *
     * @return string 
     */
    public function getFullfillmentsListThree()
    {
        return $this->fullfillmentsListThree;
    }

    /**
     * Set fullfillmentsListTwo
     *
     * @param string $fullfillmentsListTwo
     * @return Objectives
     */
    public function setFullfillmentsListTwo($fullfillmentsListTwo)
    {
        $this->fullfillmentsListTwo = $fullfillmentsListTwo;
    
        return $this;
    }

    /**
     * Get fullfillmentsListTwo
     *
     * @return string 
     */
    public function getFullfillmentsListTwo()
    {
        return $this->fullfillmentsListTwo;
    }

    /**
     * Set primary objective
     *
     * @param string $primaryObjective
     * @return Objectives
     */
    public function setPrimaryObjective($primaryObjective)
    {
        $this->primaryObjective = $primaryObjective;
    
        return $this;
    }

    /**
     * Get primary objective
     *
     * @return string 
     */
    public function getPrimaryObjective()
    {
        return $this->primary;
    }
}
