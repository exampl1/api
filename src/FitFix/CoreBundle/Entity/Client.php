<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;

use Symfony\Component\Validator\Constraints as Assert;
use Rhumsaa\Uuid\Uuid;
use Doctrine\Common\Collections\Criteria;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\ClientRepository")
 * @ExclusionPolicy("all")
 */
class Client
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"list", "details", "event-details", "invoice-details", "invoice-list", "workout-details", "packagepurchase-details", "review-list", "review-details"})
     */
    private $id;
    
    /**
     * @var UUID
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Expose()
     * @Groups({"list", "details", "event-details", "invoice-details", "invoice-list", "workout-details", "packagepurchase-details", "review-list", "review-details"})
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=false, name="first_name")
     * @Expose
     * @Groups({"list", "details",  "review-list", "event-details", "notification", "invoice-details", "invoice-list", "packagepurchase-details"})
     * @Assert\NotBlank(
     *     message="Please enter your first name",
     *     groups={"Registration"}
     * )
     * 
     * @SerializedName("firstName")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=false, name="last_name")
     * @Expose
     * @Groups({"list", "details",  "review-list", "event-details", "notification", "invoice-details", "invoice-list", "packagepurchase-details"})
     * @Assert\NotBlank(
     *     message="Please enter your last name",
     *     groups={"Registration"}
     * )
     * 
     * @SerializedName("lastName")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=false, name="gender")
     * @Expose
     * @Groups({"details"})
     * @Assert\NotBlank(
     *     message="Please select your gender",
     *     groups={"Registration"}
     * )
     */
    private $gender;


    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false, name="dob")
     * @Expose
     * @Groups({"details"})
     * @Assert\NotBlank(
     *     message="Please enter your date or birth",
     *     groups={"Registration"}
     * )
     */
    private $dob;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="landline")
     * @Expose
     * @Groups({"details"})
     * @Assert\NotBlank(
     * 		message="Landline number is required",
     * 		groups={"Registration"}
     * )
     */
    private $landline;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=true, name="mobile")
     * @Expose
     * @Groups({"details", "event-details"})
     */
    private $mobile;

    /**
     * The clients address
     * @property string
     *
     * @ORM\Column(type="text", nullable=true, name="address")
     * @Expose
     * @Groups({"details", "event-details"})
     */
    private $address;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean", nullable=true)
     * @Expose
     * @Groups({"list", "details", "event-details", "notification", "invoice-details", "invoice-list", "packagepurchase-details"})
     */
    private $archived;

    /**
     * The clients specific aim
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="specificAim")
     * @Expose
     * @Groups({"details", "event-details"})
     * 
     * @SerializedName("specificAim")
     */
   	private $specificAim;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\Column(type="text", nullable=true, name="notes")
     * @Expose
     * @Groups({"details"})
     */
    private $notes;

    /**
     * @var Lifestyle
     *
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\Lifestyle", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $lifestyle;

    /**
     * @var Nutrition
     *
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\Nutrition", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $nutrition;

    /**
     * @var Objectives
     *
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\Objectives", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $objectives;

    /**
     * @var PARQ
     *
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\PARQ", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $PARQ;

    // TODO: Everything below this line is probably irrelevant, should evanuate it soon

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Review", mappedBy="client", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $reviews;


    /**
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="clients")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     * @Assert\NotBlank(
     *     message="Please select a trainer",
     *     groups={"Registration"}
     * )
     *
     * @Expose
     * @Groups({"details"})
     */
    private $trainer;

    /**
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\User", inversedBy="client", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)
     * @Expose
     * @Groups({"details", "invoice-details"})
     */
    private $user;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     * @Expose
     * @Groups({"details"})
     */
    private $goals;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Workout", mappedBy="client", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid
     */
    private $workouts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Mealplan", mappedBy="client", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid
     */
    private $mealplans;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Invoice", mappedBy="to", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $invoices;

    /**
     * The users next session
     * @var Session
     * @Expose
     * @Groups({"event-details"})
     * 
     * @SerializedName("nextSession")
     */
    private $nextSession;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Thread", mappedBy="client", cascade={"persist", "remove"})
     */
    private $threads;

    /**
     * The users photo
     * @var string
     * @ORM\Column(name="photo", type="string")
     * @Expose
     * @Groups({"list", "details", "event-details", "notification"})
     */
    private $photo;
    
    /**
     * Terms accepted
     * @var bool
     * 
     * @ORM\Column(type="boolean", nullable=true)
     * @Expose()
     * @Groups({"list", "details", "event-details", "notification"})
     * @SerializedName("termsAccepted")
     */
    private $termsAccepted;

    /**
     * Set archived
     *
     * @param boolean $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }

	public function getNutrition() {
		return $this->nutrition;
	}

	public function setNutrition(Nutrition $nutrition) {
		$this->nutrition = $nutrition;
		return $this;
	}


	public function getObjectives() {
		return $this->objectives;
	}

	public function setObjectives(Objectives $objectives) {
		$this->objectives = $objectives;
		return $this;
	}


	/**
	 * @return the PARQ
	 */
	public function getPARQ() {
		return $this->PARQ;
	}

	/**
	 * @param PARQ $PARQ
	 */
	public function setPARQ(PARQ $PARQ) {
		$this->PARQ = $PARQ;
		return $this;
	}


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->review = new \Doctrine\Common\Collections\ArrayCollection();
        $this->goals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workouts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->threads = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mealplans = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notes = new \Doctrine\Common\Collections\ArrayCollection();
        
        $this->uuid = Uuid::uuid4() . '-' . Uuid::uuid4();
    }

    public function __toString()
    {
        return sprintf('%s %s (%s)', $this->firstName, $this->lastName, "fake@user.com");
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Client
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Client
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Client
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     * @return Client
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Client
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Client
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Add reviews
     *
     * @param \FitFix\CoreBundle\Entity\Review $reviews
     * @return Client
     */
    public function addReview(\FitFix\CoreBundle\Entity\Review $reviews)
    {
        $reviews->setClient($this);

        $this->reviews[] = $reviews;

        return $this;
    }

    /**
     * Remove reviews
     *
     * @param \FitFix\CoreBundle\Entity\Review $reviews
     */
    public function removeReview(\FitFix\CoreBundle\Entity\Review $reviews)
    {
        $this->reviews->removeElement($reviews);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }


    /**
     * Get goals
     *
     * @return array
     */
    public function getGoals()
    {
        return $this->goals;
    }
    
    /**
     * Sets the goals
     */
    public function setGoals($goals){
    	$this->goals = $goals;
    }


    /**
     * Add workouts
     *
     * @param \FitFix\CoreBundle\Entity\Workout $workouts
     * @return Client
     */
    public function addWorkout(\FitFix\CoreBundle\Entity\Workout $workouts)
    {
        $workouts->setClient($this);

        $this->workouts[] = $workouts;

        return $this;
    }

    /**
     * Remove workouts
     *
     * @param \FitFix\CoreBundle\Entity\Workout $workouts
     */
    public function removeWorkout(\FitFix\CoreBundle\Entity\Workout $workouts)
    {
        $this->workouts->removeElement($workouts);
    }

    /**
     * Get workouts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkouts()
    {
    	$criteria = Criteria::create()->where(Criteria::expr()->eq('active', true));
        return $this->workouts->matching($criteria);
    }

    /**
     * Add mealplan
     *
     * @param \FitFix\CoreBundle\Entity\Mealplan $mealplan
     * @return Client
     */
    public function addMealplan(\FitFix\CoreBundle\Entity\Mealplan $mealplan)
    {
        $mealplan->setClient($this);

        $this->mealplans[] = $mealplan;

        return $this;
    }

    /**
     * Remove mealplans
     *
     * @param \FitFix\CoreBundle\Entity\Mealplan $mealplan
     */
    public function removeMealplan(\FitFix\CoreBundle\Entity\Mealplan $mealplan)
    {
        $this->mealplans->removeElement($mealplan);
    }

    /**
     * Get mealplans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMealplans()
    {
        return $this->mealplans;
    }


    /**
     * Set user
     *
     * @param \FitFix\CoreBundle\Entity\User $user
     * @return Client
     */
    public function setUser(\FitFix\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \FitFix\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Client
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Add invoices
     *
     * @param \FitFix\CoreBundle\Entity\Invoice $invoices
     * @return Client
     */
    public function addInvoice(\FitFix\CoreBundle\Entity\Invoice $invoices)
    {
        $this->invoices[] = $invoices;

        return $this;
    }

    /**
     * Remove invoices
     *
     * @param \FitFix\CoreBundle\Entity\Invoice $invoices
     */
    public function removeInvoice(\FitFix\CoreBundle\Entity\Invoice $invoices)
    {
        $this->invoices->removeElement($invoices);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Add threads
     *
     * @param \FitFix\CoreBundle\Entity\Thread $threads
     * @return Client
     */
    public function addThread(\FitFix\CoreBundle\Entity\Thread $threads)
    {
        $this->threads[] = $threads;
        return $this;
    }

    /**
     * Remove threads
     *
     * @param \FitFix\CoreBundle\Entity\Thread $threads
     */
    public function removeThread(\FitFix\CoreBundle\Entity\Thread $threads)
    {
        $this->threads->removeElement($threads);
    }

    /**
     * Get threads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreads()
    {
        return $this->threads;
    }

	/**
	 * @return string
	 */
	public function getLandline() {
		return $this->landline;
	}

	/**
	 * @param string $landline
	 */
	public function setLandline($landline) {
		$this->landline = $landline;
		return $this;
	}

	/**
	 * @return the string
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress($address) {
		$this->address = $address;
		return $this;
	}

	/**
	 * @return the Collection
	 */
	public function getNotes() {
		return $this->notes;
	}


	/**
	 * @return the Lifestyle
	 */
	public function getLifestyle() {
		return $this->lifestyle;
	}

	/**
	 * @param Lifestyle $lifestyle
	 */
	public function setLifestyle(Lifestyle $lifestyle) {
		$this->lifestyle = $lifestyle;
		return $this;
	}

	/**
	 * @param  $notes
	 */
	public function setNotes( $notes) {
		$this->notes = $notes;
		return $this;
	}

	/**
	 * @return the string
	 */
	public function getSpecificAim() {
		return $this->specificAim;
	}

	/**
	 * @param string $specificAim
	 */
	public function setSpecificAim($specificAim) {
		$this->specificAim = $specificAim;
		return $this;
	}

	/**
	 * @return the Session
	 */
	public function getNextSession() {
		return $this->nextSession;
	}

	/**
	 * @param Session $nextSession
	 */
	public function setNextSession(Session $nextSession) {
		$this->nextSession = $nextSession;
		return $this;
	}

	/**
	 * @return the string
	 */
	public function getPhoto() {
		return $this->photo;
	}

	/**
	 * @param string $photo
	 */
	public function setPhoto($photo) {
		$this->photo = $photo;
		return $this;
	}

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Client
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }
	
	/**
	 *
	 * @return the bool
	 */
	public function getTermsAccepted() {
		return $this->termsAccepted;
	}
	
	/**
	 *
	 * @param
	 *        	$termsAccepted
	 */
	public function setTermsAccepted($termsAccepted) {
		$this->termsAccepted = $termsAccepted;
		return $this;
	}
	
}