<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mealplan
 *
 * @ORM\Table(name="mealplan")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\MealplanRepository")
 * @ExclusionPolicy("all")
 */
class Mealplan
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"list", "details", "notification"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     * @Expose
     * @Groups({"list", "details", "notification"})
     * @Assert\NotBlank(
     *     message="Please enter a name"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="date", length=128, nullable=false, name="created")
     * @Expose
     * @Groups({"list", "details", "notification"})
     * @Assert\NotBlank(
     *     message="Please enter a created date"
     * )
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Client", inversedBy="mealplans")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * 
     * @Assert\NotBlank(
     *     message="Please select a client"
     * )
     */
    private $client;
    
    /**
     * @var Trainer
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="mealplans")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     */
    private $trainer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Meal", mappedBy="mealplan", cascade={"persist","remove"})
     * @Expose  
     * @Groups({"details", "list"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $meals;

     /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="calories")
     * @Expose
     * @Groups({"list", "details", "notification"})
     * @Assert\NotBlank(
     *     message="Please enter a quantity"
     * )
     */
    private $calories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->meals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mealplan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return Mealplan
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Mealplan
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }


    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Mealplan
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;
    
        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer 
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

     /**
     * Set calories
     *
     * @param string $description
     * @return Mealplan
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;

        return $this;
    }

    /**
     * Get calories
     *
     * @return string
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Add meal
     *
     * @param \FitFix\CoreBundle\Entity\Meal $meal
     * @return Mealplan
     */
    public function addMeal(\FitFix\CoreBundle\Entity\Meal $meal)
    {
        $this->meals[] = $meal;

        return $this;
    }

    /**
     * Remove meal
     *
     * @param \FitFix\CoreBundle\Entity\Meal $meal
     */
    public function removeMeal(\FitFix\CoreBundle\Entity\Meal $meal)
    {
        $this->meals->removeElement($meal);
    }

    /**
     * Get meals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMeals()
    {
        return $this->meals;
    }
}