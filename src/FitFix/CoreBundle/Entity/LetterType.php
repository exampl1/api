<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * @ORM\Entity
 */
class LetterType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @var \FitFix\CoreBundle\Entity\Letter
     *
     * @ORM\OneToMany(targetEntity="Letter", mappedBy="letter_type", cascade={"all"})
     */
    private $letters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->letters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * toString
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LetterType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add letters
     *
     * @param \FitFix\CoreBundle\Entity\Letter $letters
     * @return LetterType
     */
    public function addLetter(\FitFix\CoreBundle\Entity\Letter $letters)
    {
        $this->letters[] = $letters;

        return $this;
    }

    /**
     * Remove letters
     *
     * param \FitFix\CoreBundle\Entity\Letter $letters
     */
    public function removeLetter(\FitFix\CoreBundle\Entity\Letter $letters)
    {
        $this->letters->removeElement($letters);
    }


    /**
     * Get letters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLetters()
    {
        return $this->letters;
    }
}