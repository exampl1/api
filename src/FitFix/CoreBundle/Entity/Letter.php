<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Entity\Repository\LetterRepository")
 *
 */
class Letter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Expose
     * 
     */
    private $title;
    
    /**
     * @var boolean $is_default
     * @ORM\Column(name="is_default", type="boolean")
     */
    private $is_default = false;

    /**
     * @var string
     *
     * @ORM\Column(name="messageBody", type="text")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $messageBody;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\LetterType", inversedBy="letters")
     * @ORM\JoinColumn(name="letters_type_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $letter_type;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set is_default
     *
     * @param boolean $is_default
     * @return Letter
     */
    public function setIsDefault($isDefault) {
        $this->is_default= $isDefault;

        return $this;
    }

    /**
     * Get is_default
     *
     * @return boolean
     */
    public function getIsDefault() {
        return $this->is_default;
    }

    /**
     * Set messageBody
     *
     * @param string $messageBody
     * @return Message
     */
    public function setMessageBody($messageBody)
    {
        $this->messageBody = $messageBody;
    
        return $this;
    }

    /**
     * Get messageBody
     *
     * @return string 
     */
    public function getMessageBody()
    {
        return $this->messageBody;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
   

    /**
     * Set title
     *
     * @param string $title
     * @return Letter
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set letter_type
     *
     * @param \FitFix\CoreBundle\Entity\LetterType $letterType
     * @return Letter
     */
    public function setLetterType(\FitFix\CoreBundle\Entity\LetterType $letterType = null)
    {
        $this->letter_type = $letterType;

        return $this;
    }


    /**
     * Get letter_type
     *
     * @return \FitFix\CoreBundle\Entity\LetterType 
     */
    public function getLetterType()
    {
        return $this->letter_type;
    }
}