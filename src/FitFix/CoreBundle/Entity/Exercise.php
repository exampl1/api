<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as gedmo;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Exercise
 *
 * @ORM\Table(name="exercise")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\ExerciseRepository")
 * @ExclusionPolicy("all")
 */
class Exercise
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @Groups({"workout-list", "workout-details", "list", "details", "workoutLog-details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     * @Expose
     * @Groups({"workout-list", "workout-details", "list", "details", "workoutLog-details"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="slug")
     * @gedmo\Slug(fields={"name"})
     * @Expose
     * @Groups({"workout-list", "workout-details", "list", "details", "workoutLog-details"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="preparation")
     * @Expose
     * @Groups({"workout-details", "details", "workoutLog-details"})
     */
    private $preparation;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="execution")
     * @Expose
     * @Groups({"workout-details", "details", "workoutLog-details"})
     */
    private $execution;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, name="recovery")
     * @Expose
     * @Groups({"workout-details", "details", "workoutLog-details"})
     */
    private $recovery;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="webm_video_path")
     * @Expose
     * @Groups({"workout-details", "details", "workoutLog-details"})
     */
    private $webmVideoPath;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="mp4_video_path")
     * @Expose
     * @Groups({"workout-details", "details", "workoutLog-details"})
     */
    private $mp4VideoPath;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="posterframe_path")
     * @Expose
     * @Groups({"workout-details", "list", "details", "workoutLog-details"})
     */
    private $posterframePath;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="picture_path")
     * @Expose
     * @Groups({"workout-list", "workout-details", "list", "details", "workoutLog-details"})
     */
    private $picturePath;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="cdn_host")
     * @Expose
     * @Groups({"workout-list", "workout-details", "list", "details", "workoutLog-details"})
     */
    private $cdnHost;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FitFix\CoreBundle\Entity\Muscle", inversedBy="exercises", cascade={"persist"})
     * @ORM\JoinTable(
     *     name="exercises_muscles", 
     *     joinColumns={@ORM\JoinColumn(name="exercise_id", referencedColumnName="id")}, 
     *     inverseJoinColumns={@ORM\JoinColumn(name="muscle_id", referencedColumnName="id")}
     * )
     * @Expose
     * @Groups({"workout-details", "list", "details"})
     */
    private $muscles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FitFix\CoreBundle\Entity\Equipment", inversedBy="exercises", cascade={"persist"})
     * @ORM\JoinTable(
     *     name="exercises_equipment", 
     *     joinColumns={@ORM\JoinColumn(name="exercise_id", referencedColumnName="id")}, 
     *     inverseJoinColumns={@ORM\JoinColumn(name="equipment_id", referencedColumnName="id")}
     * )
     * @Expose
     * @Groups({"workout-details", "list", "details"})
     */
    private $equipment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->equipment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Exercise
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set preparation
     *
     * @param string $preparation
     * @return Exercise
     */
    public function setPreparation($preparation)
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * Get preparation
     *
     * @return string
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set execution
     *
     * @param string $execution
     * @return Exercise
     */
    public function setExecution($execution)
    {
        $this->execution = $execution;

        return $this;
    }

    /**
     * Get execution
     *
     * @return string
     */
    public function getExecution()
    {
        return $this->execution;
    }

    /**
     * Set recovery
     *
     * @param string $recovery
     * @return Exercise
     */
    public function setRecovery($recovery)
    {
        $this->recovery = $recovery;

        return $this;
    }

    /**
     * Get recovery
     *
     * @return string
     */
    public function getRecovery()
    {
        return $this->recovery;
    }

    /**
     * Set webmVideoPath
     *
     * @param string $webmVideoPath
     * @return Exercise
     */
    public function setWebmVideoPath($webmVideoPath)
    {
        $this->webmVideoPath = $webmVideoPath;

        return $this;
    }

    /**
     * Get webmVideoPath
     *
     * @return string
     */
    public function getWebmVideoPath()
    {
        return $this->webmVideoPath;
    }

    /**
     * Set mp4VideoPath
     *
     * @param string $mp4VideoPath
     * @return Exercise
     */
    public function setMp4VideoPath($mp4VideoPath)
    {
        $this->mp4VideoPath = $mp4VideoPath;

        return $this;
    }

    /**
     * Get mp4VideoPath
     *
     * @return string
     */
    public function getMp4VideoPath()
    {
        return $this->mp4VideoPath;
    }

    /**
     * Set posterframePath
     *
     * @param string $posterframePath
     * @return Exercise
     */
    public function setPosterframePath($posterframePath)
    {
        $this->posterframePath = $posterframePath;

        return $this;
    }

    /**
     * Get posterframePath
     *
     * @return string
     */
    public function getPosterframePath()
    {
        return $this->posterframePath;
    }

    /**
     * Set picturePath
     *
     * @param string $picturePath
     * @return Exercise
     */
    public function setPicturePath($picturePath)
    {
        $this->picturePath = $picturePath;

        return $this;
    }

    /**
     * Get picturePath
     *
     * @return string
     */
    public function getPicturePath()
    {
        return $this->picturePath;
    }

    /**
     * Set cdnHost
     *
     * @param string $cdnHost
     * @return Exercise
     */
    public function setCdnHost($cdnHost)
    {
        $this->cdnHost = $cdnHost;

        return $this;
    }

    /**
     * Get cdnHost
     *
     * @return string
     */
    public function getCdnHost()
    {
        return $this->cdnHost;
    }

    /**
     * Add equipment
     *
     * @param \FitFix\CoreBundle\Entity\Equipment $equipment
     * @return Exercise
     */
    public function addEquipment(\FitFix\CoreBundle\Entity\Equipment $equipment)
    {
        $equipment->addExercise($this);

        $this->equipment[] = $equipment;

        return $this;
    }

    /**
     * Remove equipment
     *
     * @param \FitFix\CoreBundle\Entity\Equipment $equipment
     */
    public function removeEquipment(\FitFix\CoreBundle\Entity\Equipment $equipment)
    {
        $this->equipment->removeElement($equipment);
    }

    /**
     * Get equipment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Exercise
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add muscles
     *
     * @param \FitFix\CoreBundle\Entity\Muscle $muscles
     * @return Exercise
     */
    public function addMuscle(\FitFix\CoreBundle\Entity\Muscle $muscles)
    {
        $this->muscles[] = $muscles;

        return $this;
    }

    /**
     * Remove muscles
     *
     * @param \FitFix\CoreBundle\Entity\Muscle $muscles
     */
    public function removeMuscle(\FitFix\CoreBundle\Entity\Muscle $muscles)
    {
        $this->muscles->removeElement($muscles);
    }

    /**
     * Get muscles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMuscles()
    {
        return $this->muscles;
    }
}