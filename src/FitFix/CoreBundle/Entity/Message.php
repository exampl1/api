<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Message
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\MessageRepository")
 * 
 * @ExclusionPolicy("all")
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Expose
     * 
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="messageBody", type="text")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $messageBody;

    /**
     * @var string
     *
     * @ORM\Column(name="messageRole", type="text")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $messageRole;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Expose()
     * @Groups({"list", "details"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Thread", inversedBy="messages")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     */
    private $thread;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageBody
     *
     * @param string $messageBody
     * @return Message
     */
    public function setMessageBody($messageBody)
    {
        $this->messageBody = $messageBody;
    
        return $this;
    }

    /**
     * Get messageBody
     *
     * @return string 
     */
    public function getMessageBody()
    {
        return $this->messageBody;
    }

    /**
     * Set messageBody
     *
     * @param string $messageBody
     * @return Message
     */
    public function setMessageRole($messageRole)
    {
        $this->messageRole = $messageRole;
    
        return $this;
    }

    /**
     * Get messageBody
     *
     * @return string 
     */
    public function getMessageRole()
    {
        return $this->messageRole;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set thread
     *
     * @param \FitFix\CoreBundle\Entity\Thread $thread
     * @return Message
     */
    public function setThread(Thread $thread = null)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \FitFix\CoreBundle\Entity\Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    
}