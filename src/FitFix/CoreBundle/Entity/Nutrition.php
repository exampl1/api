<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Nutrition
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\NutritionRepository")
 * @ExclusionPolicy("all")
 */
class Nutrition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alcohol", type="text")
     * @Expose
     */
    private $alcohol;

    /**
     * @var string
     *
     * @ORM\Column(name="bloating", type="text")
     * @Expose
     */
    private $bloating;

    /**
     * @var string
     *
     * @ORM\Column(name="bread", type="text")
     * @Expose
     */
    private $bread;

    /**
     * @var string
     *
     * @ORM\Column(name="breakfast", type="text")
     */
    private $breakfast;

    /**
     * @var string
     *
     * @ORM\Column(name="caffeine", type="text")
     * @Expose
     */
    private $caffeine;

    /**
     * @var string
     *
     * @ORM\Column(name="carbs", type="text")
     * @Expose
     */
    private $carbs;

    /**
     * @var string
     *
     * @ORM\Column(name="dessert", type="text")
     * @Expose
     */
    private $dessert;

    /**
     * @var string
     *
     * @ORM\Column(name="fastfood", type="text")
     * @Expose
     */
    private $fastfood;

    /**
     * @var string
     *
     * @ORM\Column(name="meals", type="text")
     * @Expose
     */
    private $meals;

    /**
     * @var string
     *
     * @ORM\Column(name="milk", type="text")
     * @Expose
     */
    private $milk;

    /**
     * @var string
     *
     * @ORM\Column(name="oils", type="text")
     * @Expose
     */
    private $oils;

    /**
     * @var string
     *
     * @ORM\Column(name="packaged", type="text")
     * @Expose
     */
    private $packaged;

    /**
     * @var string
     *
     * @ORM\Column(name="preparation", type="text")
     * @Expose
     */
    private $preparation;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="text")
     * @Expose
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="shopfood", type="text")
     * @Expose
     */
    private $shopfood;

    /**
     * @var string
     *
     * @ORM\Column(name="snack", type="text")
     * @Expose
     */
    private $snack;

    /**
     * @var string
     *
     * @ORM\Column(name="sugar", type="text")
     * @Expose
     */
    private $sugar;

    /**
     * @var string
     *
     * @ORM\Column(name="sweeteners", type="text")
     * @Expose
     */
    private $sweeteners;

    /**
     * @var string
     *
     * @ORM\Column(name="times", type="text")
     * @Expose
     */
    private $times;

    /**
     * @var string
     *
     * @ORM\Column(name="water", type="text")
     * @Expose
     */
    private $water;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alcohol
     *
     * @param string $alcohol
     * @return Nutdrition
     */
    public function setAlcohol($alcohol)
    {
        $this->alcohol = $alcohol;
    
        return $this;
    }

    /**
     * Get alcohol
     *
     * @return string 
     */
    public function getAlcohol()
    {
        return $this->alcohol;
    }

    /**
     * Set bloating
     *
     * @param string $bloating
     * @return Nutdrition
     */
    public function setBloating($bloating)
    {
        $this->bloating = $bloating;
    
        return $this;
    }

    /**
     * Get bloating
     *
     * @return string 
     */
    public function getBloating()
    {
        return $this->bloating;
    }

    /**
     * Set bread
     *
     * @param string $bread
     * @return Nutdrition
     */
    public function setBread($bread)
    {
        $this->bread = $bread;
    
        return $this;
    }

    /**
     * Get bread
     *
     * @return string 
     */
    public function getBread()
    {
        return $this->bread;
    }

    /**
     * Set breakfast
     *
     * @param string $breakfast
     * @return Nutdrition
     */
    public function setBreakfast($breakfast)
    {
        $this->breakfast = $breakfast;
    
        return $this;
    }

    /**
     * Get breakfast
     *
     * @return string 
     */
    public function getBreakfast()
    {
        return $this->breakfast;
    }

    /**
     * Set caffeine
     *
     * @param string $caffeine
     * @return Nutdrition
     */
    public function setCaffeine($caffeine)
    {
        $this->caffeine = $caffeine;
    
        return $this;
    }

    /**
     * Get caffeine
     *
     * @return string 
     */
    public function getCaffeine()
    {
        return $this->caffeine;
    }

    /**
     * Set carbs
     *
     * @param string $carbs
     * @return Nutdrition
     */
    public function setCarbs($carbs)
    {
        $this->carbs = $carbs;
    
        return $this;
    }

    /**
     * Get carbs
     *
     * @return string 
     */
    public function getCarbs()
    {
        return $this->carbs;
    }

    /**
     * Set dessert
     *
     * @param string $dessert
     * @return Nutdrition
     */
    public function setDessert($dessert)
    {
        $this->dessert = $dessert;
    
        return $this;
    }

    /**
     * Get dessert
     *
     * @return string 
     */
    public function getDessert()
    {
        return $this->dessert;
    }

    /**
     * Set fastfood
     *
     * @param string $fastfood
     * @return Nutdrition
     */
    public function setFastfood($fastfood)
    {
        $this->fastfood = $fastfood;
    
        return $this;
    }

    /**
     * Get fastfood
     *
     * @return string 
     */
    public function getFastfood()
    {
        return $this->fastfood;
    }

    /**
     * Set meals
     *
     * @param string $meals
     * @return Nutdrition
     */
    public function setMeals($meals)
    {
        $this->meals = $meals;
    
        return $this;
    }

    /**
     * Get meals
     *
     * @return string 
     */
    public function getMeals()
    {
        return $this->meals;
    }

    /**
     * Set milk
     *
     * @param string $milk
     * @return Nutdrition
     */
    public function setMilk($milk)
    {
        $this->milk = $milk;
    
        return $this;
    }

    /**
     * Get milk
     *
     * @return string 
     */
    public function getMilk()
    {
        return $this->milk;
    }

    /**
     * Set oils
     *
     * @param string $oils
     * @return Nutdrition
     */
    public function setOils($oils)
    {
        $this->oils = $oils;
    
        return $this;
    }

    /**
     * Get oils
     *
     * @return string 
     */
    public function getOils()
    {
        return $this->oils;
    }

    /**
     * Set packaged
     *
     * @param string $packaged
     * @return Nutdrition
     */
    public function setPackaged($packaged)
    {
        $this->packaged = $packaged;
    
        return $this;
    }

    /**
     * Get packaged
     *
     * @return string 
     */
    public function getPackaged()
    {
        return $this->packaged;
    }

    /**
     * Set preparation
     *
     * @param string $preparation
     * @return Nutdrition
     */
    public function setPreparation($preparation)
    {
        $this->preparation = $preparation;
    
        return $this;
    }

    /**
     * Get preparation
     *
     * @return string 
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Nutdrition
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set shopfood
     *
     * @param string $shopfood
     * @return Nutdrition
     */
    public function setShopfood($shopfood)
    {
        $this->shopfood = $shopfood;
    
        return $this;
    }

    /**
     * Get shopfood
     *
     * @return string 
     */
    public function getShopfood()
    {
        return $this->shopfood;
    }

    /**
     * Set snack
     *
     * @param string $snack
     * @return Nutdrition
     */
    public function setSnack($snack)
    {
        $this->snack = $snack;
    
        return $this;
    }

    /**
     * Get snack
     *
     * @return string 
     */
    public function getSnack()
    {
        return $this->snack;
    }

    /**
     * Set sugar
     *
     * @param string $sugar
     * @return Nutdrition
     */
    public function setSugar($sugar)
    {
        $this->sugar = $sugar;
    
        return $this;
    }

    /**
     * Get sugar
     *
     * @return string 
     */
    public function getSugar()
    {
        return $this->sugar;
    }

    /**
     * Set sweeteners
     *
     * @param string $sweeteners
     * @return Nutdrition
     */
    public function setSweeteners($sweeteners)
    {
        $this->sweeteners = $sweeteners;
    
        return $this;
    }

    /**
     * Get sweeteners
     *
     * @return string 
     */
    public function getSweeteners()
    {
        return $this->sweeteners;
    }

    /**
     * Set times
     *
     * @param string $times
     * @return Nutdrition
     */
    public function setTimes($times)
    {
        $this->times = $times;
    
        return $this;
    }

    /**
     * Get times
     *
     * @return string 
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * Set water
     *
     * @param string $water
     * @return Nutdrition
     */
    public function setWater($water)
    {
        $this->water = $water;
    
        return $this;
    }

    /**
     * Get water
     *
     * @return string 
     */
    public function getWater()
    {
        return $this->water;
    }
}
