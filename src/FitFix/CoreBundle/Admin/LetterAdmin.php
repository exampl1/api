<?php
// src/FitFix/CoreBundle/Admin/MessageAdmin.php

namespace FitFix\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LetterAdmin extends Admin
{

   
    protected $baseRouteName = 'fitfix_admin_letter';

    protected $baseRoutePattern = 'letter';
    
    // setup the defaut sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('title', 'text', array('label' => 'title'))
                ->add('messageBody', 'textarea')
                ->add('createdAt', 'date')
                ->add('letter_type', 'entity', array('label' => 'Type Letter', 'class' => 'FitFix\CoreBundle\Entity\LetterType'))
                ->add('is_default', 'checkbox', array('label' => 'Default', 'required' => false))
                ->setHelps(array(
                    'title' => 'title'
        ));
    }
    
    protected function configureShowFields(ShowMapper $showMapper) {
                
        $showMapper
                ->add('id')
                ->add('title')
                ->add('messageBody')
                ->add('letter_type')
                ->add('is_default');
    }

    //Fields to be shown on filter formss e vtyt d 
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title')
                ->add('messageBody')
                ->add('letter_type')
                ->add('is_default');
    }

    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('route' => array('name' => 'show')))
                ->add('title')
                ->add('messageBody')
                ->add('letter_type')
                ->add('is_default')
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array(),
                            'delete' => array(),
                        )
        ));
    }

    public function postPersist($object) {
        $this->manageDefaultLetterAdmins($object);
    }

    public function preUpdate($object) {
        $this->manageDefaultLetterAdmins($object);
    }
    
    private function manageDefaultLetterAdmins($object) {
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $letter = $em->getRepository('FitFixCoreBundle:Letter')->changeLetterByLetterType($object->getLetterType()->getId());
        if($object->getIsDefault()) {
            $object->setIsDefault(1);
        }
    } 
}