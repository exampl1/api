<?php
// src/FitFix/CoreBundle/Admin/MessageAdmin.php

namespace FitFix\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MesaageAdmin extends Admin
{

   
    protected $baseRouteName = 'fitfix_admin_message';

    protected $baseRoutePattern = 'message';
    
    // setup the defaut sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('title', 'text', array('label' => 'title'))
                ->add('country', 'entity', array('label' => 'country', 'class' => 'Lgn\CoreBundle\Entity\Country'))
                ->add('fips', 'text', array('label' => 'fips', 'required'=> false))
                ->setHelps(array(
                    'title' => 'title'
        ));
    }
    
    protected function configureShowFields(ShowMapper $showMapper) {
                
        $showMapper
                ->add('id')
                ->add('title')
                //->add('country')
                //->add('fips')
                ;
    }

    //Fields to be shown on filter formss e vtyt d 
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title')
                //->add('country')
                ;
    }

    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('route' => array('name' => 'show')))
                ->add('title')
                ->add('country')
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array(),
                            'delete' => array(),
                        )
        ));
    }
}