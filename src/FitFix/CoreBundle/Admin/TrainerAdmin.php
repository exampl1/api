<?php
// src/FitFix/CoreBundle/Admin/TrainerAdmin.php

namespace FitFix\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TrainerAdmin extends Admin
{
    // function __construct(){
    //     die('fffff');
    // }
   
    protected $baseRouteName = 'fitfix_admin_trainer';

    protected $baseRoutePattern = 'trainer';
    
    // setup the defaut sort column and order
    // protected $datagridValues = array(
    //     '_sort_order' => 'DESC',
    //     '_sort_by' => 'id'
    // );

    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper        
                ->add('firstName', 'text')                    
                ->add('lastname', 'text')
                ->add('gender', 'choice', array('choices' => array('Male' => 'Male', 'Female' => 'Female')))
                ->add('birthday', 'date')
                ->add('user.registered', 'date')
                ->add('trial_date', 'date')
                ->add('trial', 'checkbox', array('label' => 'Trial', 'required' => false))
                ->add('emailTrainer', 'text')
                ->add('phoneNumber', 'text')
                ->add('addressNumber', 'text')
                ->add('addressStreet', 'text')
                ->add('addressTown', 'text')
                ->add('addressCountry', 'text')
                ->add('addressPostCode', 'text')
                // ->add('addressPostCode', 'text') descont               
                ->setHelps(array(
                    'title' => 'title'
        ));
    }
    
    protected function configureShowFields(ShowMapper $showMapper) {
                
        $showMapper
                ->add('id')
                ->add('firstName')
                ->add('lastname', 'text')
                //->add('country')
                //->add('fips')
                ;
    }

    //Fields to be shown on filter formss e vtyt d 
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('firstName')
                //->add('country')
                ;
    }

    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('route' => array('name' => 'show')))
                ->add('firstName')
                ->add('lastname')                
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array(),
                            'delete' => array(),
                        )
        ));
    }
}