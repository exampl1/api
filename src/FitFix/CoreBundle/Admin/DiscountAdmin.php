<?php
// src/FitFix/CoreBundle/Admin/DescontAdmin.php

namespace FitFix\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DiscountAdmin extends Admin
{

   
    protected $baseRouteName = 'fitfix_admin_discount';

    protected $baseRoutePattern = 'discount';
    
    // setup the defaut sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('code', 'text', array('label' => 'code'))
                ->add('procent', 'text')
                ->add('amount', 'text')
                ->add('createdAt', 'date')
                ->add('country')
                ->add('currency', 'choice', array('choices' => array('usa' => '$', 'euro' => '€')))
                ->setHelps(array(
                    'code' => 'code'
        ));
    }
    
    protected function configureShowFields(ShowMapper $showMapper) {
                
        $showMapper
                ->add('id')
                ->add('code')
                ->add('procent')
                //->add('fips')
                ;
    }

    //Fields to be shown on filter formss e vtyt d 
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('code')
                ->add('procent')
                ;
    }

    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('route' => array('name' => 'show')))
                ->add('code')
                ->add('procent')
                ->add('amount')
                ->add('currency')
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array(),
                            'delete' => array(),
                        )
        ));
    }
}