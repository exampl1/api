<?php

namespace FitFix\CoreBundle\Generator;

class Password {
	
	private static $words = array(
		// Animals
		'goat',
		'chimp',
		'monkey',
		'panda',
		'turtle',
		'ant',
		'cat',
		'dog',
		'rat',
		'snake',
		'camel',
		'donkey',
		// Kitchen things
		'blender',
		'fork',
		'spoon',
		'knife',
		'chopsticks',
		'oven',
		// Fruits
		'orange',
		'lemon',
		'grapefruit',
		'melon'
	);
	
	public static function entropicPassword($entropy = null){
		if($entropy == null){
			// Generate a random entropy level within a range of 3 - 4
			$entropy = rand(3, 4);
		}
		
		$password = '';
		
		$keys = array_rand(self::$words, $entropy);
		
		foreach($keys as $value){
			$password .= self::$words[$value];
		}
		
		return $password;
		
	}
	
}