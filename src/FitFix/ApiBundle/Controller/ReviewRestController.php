<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Review;
use FitFix\ApiBundle\Form\ReviewType;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use JMS\Serializer\SerializationContext;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Handles reviews for PT's from clients
 *
 * @NamePrefix("fitfix_api_reviewrest_")
 * @author gavinwilliams
 *
 */
class ReviewRestController extends FOSRestController {

    /**
     * Returns all reviews for trainer in security context.
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER, ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getReviewsAction()
    {
		
    	$em = $this->getDoctrine()->getManager();
    	
    	$trainer;
    	
    	if($this->getUser()->getTrainer()){
    		$trainer = $this->getUser()->getTrainer();
    	} else {
    		$trainer = $this->getUser()->getClient()->getTrainer();
    	}
    	
    	$reviews = $em->getRepository('FitFixCoreBundle:Review')->findByTrainer($trainer);
    	
    	$view = $this->view($reviews);
    	
    	$serializationContext = SerializationContext::create()->setGroups(array("review-list"));
    	
    	$view->setSerializationContext($serializationContext);

        return $view;
    }

    /**
     * Returns all reviews for trainer in security context.
     *
     * @param string $id ID
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getReviewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('FitFixCoreBundle:Review')->findOneByUserAndId($authenticatedUser, $id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Review entity.');
        }

        $view = FOSView::create();

        if ($entity) {
            $serializationContext = SerializationContext::create()->setGroups(array('review-details'));
            $view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entity);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * @param string $id ID
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
    */
    public function postReviewAction(){
        $view = $this->processForm(new Review());
        return $view;
    }

    /**
     * Returns review by id for trainer in security context.
     *
     * @param string $trainerId ID
     * @param string $reviewId ID
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER, ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getTrainerReviewAction($trainerId, $reviewId)
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('FitFixCoreBundle:Review')->findOneByUserAndTrainerAndId($authenticatedUser, $trainerId, $reviewId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Review entity.');
        }

        $view = FOSView::create();

        if ($entity) {
            $serializationContext = SerializationContext::create()->setGroups(array('review-details'));
            $view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entity);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Processes an invoice object using a form
     * @param Review $review
     */
    private function processForm(Review $review){


        $statusCode = $review->getId() ? 204 : 201;

        $view = $this->view();
        $data;

        $requestBodyType = $this->getRequest()->getContentType();

        $currentuser = $this->getUser();

        $form = $this->createForm(new ReviewType(), $review);

        if($requestBodyType !== null){
            if($requestBodyType == 'json'){
                $data = json_decode($this->getRequest()->getContent(), true);
                if($data === null){

                    switch (json_last_error()) {
                        default:
                            return;
                        case JSON_ERROR_DEPTH:
                            $error = 'Maximum stack depth exceeded';
                            break;
                        case JSON_ERROR_STATE_MISMATCH:
                            $error = 'Underflow or the modes mismatch';
                            break;
                        case JSON_ERROR_CTRL_CHAR:
                            $error = 'Unexpected control character found';
                            break;
                        case JSON_ERROR_SYNTAX:
                            $error = 'Syntax error, malformed JSON';
                            break;
                        case JSON_ERROR_UTF8:
                            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                            break;
                    }

                    $view->setData(array('error' => $error));
                    $view->setStatusCode(400);
                    return $view;

                }
            }
        } else {
            $data = $this->getRequest();
        }

        // Remove the ID, it'll break EVERYTHING!
        unset($data['id']);

        $data['client'] = $currentuser->getClient()->getId();
        $data['trainer'] = $currentuser->getClient()->getTrainer()->getId();

        $form->submit($data);

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        if($form->isValid()){

            $em = $this->getDoctrine()->getManager();

            $em->persist($review);
            $em->flush();
            $view->setStatusCode($statusCode);

            if (201 == $statusCode) {
                $view->setHeader('location', $this->generateUrl('fitfix_api_reviewrest_get_trainer_review', array(
                        'trainerId' => $review->getTrainer()->getId(),
                        'reviewId' => $review->getId(),
                        '_format' => $this->getRequest()->get('_format', 'json')
                ), true));
            }

            $serializationContext = SerializationContext::create()->setGroups(array('review-details'));
            $view->setSerializationContext($serializationContext);
            $view->setData($review);

            return $view;

        }

        return $view->setData($form, 400);

    }

}