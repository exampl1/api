<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Trainer;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use FOS\RestBundle\Controller\FOSRestController;
use FitFix\CoreBundle\Entity\User;
use FitFix\CoreBundle\Form\TrainerType;

/**
 * Controller that provides Restful services over the resource Trainer.
 *
 * @NamePrefix("fitfix_api_trainerrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class TrainerRestController extends FOSRestController
{
    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function optionsTrainersAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
        return $view;
    }

    /**
     * Returns all trainers.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getTrainersAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FitFixCoreBundle:Trainer')->findAll();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Trainer entities.');
        }

        $view = FOSView::create();

        if ($entities) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }
    
    /**
     * Updates the currently logged in trainers details
     * 
     * @return FOSView
     * 
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function putTrainerAction(){
    	
    	$trainer = $this->getUser()->getTrainer();
    	/* @var $trainer Trainer */
    	
    	$view = $this->processForm($trainer);
    	
    	return $view;
    	
    }

    /**
     * Processes a package object using a form
     * @param Package $package
     * @return FOS\RestBundle\View
     */
    private function processForm(Trainer $trainer){
    
    	$statusCode = $trainer->getId() ? 204 : 201;
    	$view = $this->view();
    	$data;
    	$requestBodyType = $this->getRequest()->getContentType();
    
    	$form = $this->createForm(new TrainerType(), $trainer);
    
    	if($requestBodyType !== null){
    		if($requestBodyType == 'json'){
    			$data = json_decode($this->getRequest()->getContent(), true);
    			if($data === null){
    
    				switch (json_last_error()) {
    					default:
    						return;
    					case JSON_ERROR_DEPTH:
    						$error = 'Maximum stack depth exceeded';
    						break;
    					case JSON_ERROR_STATE_MISMATCH:
    						$error = 'Underflow or the modes mismatch';
    						break;
    					case JSON_ERROR_CTRL_CHAR:
    						$error = 'Unexpected control character found';
    						break;
    					case JSON_ERROR_SYNTAX:
    						$error = 'Syntax error, malformed JSON';
    						break;
    					case JSON_ERROR_UTF8:
    						$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
    						break;
    				}
    
    				$view->setData(array('error' => $error));
    				$view->setStatusCode(400);
    				return $view;
    
    			}
    		}
    	} else {
    		$data = $this->getRequest();
    	}
    
    	// Remove the ID, it'll break EVERYTHING!
    	unset($data['id']);

    	$form->submit($data, false);
    
    	if($form->isValid()){
    
    		$em = $this->getDoctrine()->getManager();
    		
    		$storage = $this->get('fitfix_core.storage.s3');
    			
    		($storage instanceof \FitFix\CoreBundle\Service\Storage);

    		if($trainer->getLogo() && strstr($trainer->getLogo(), 'data:image')){
    		
	    		$logo = $storage->uploadUniqueFileWithData('fitfixuserdata', 'logos', $trainer->getLogo(), 'png', 'image/png');
	    		$trainer->setLogo($logo);

    		}
    		
    		if($trainer->getPhoto() && strstr($trainer->getPhoto(), 'data:image')){
	    		$photo = $storage->uploadUniqueFileWithData('fitfixuserdata', 'profilepictures', $trainer->getPhoto(), 'jpg', 'image/jpg');
	    		$trainer->setPhoto($photo);
    		}
    		
    		$em->persist($trainer);
    		$em->flush();
    		$view->setStatusCode($statusCode);
    		if (201 == $statusCode) {
    			/*
    			 $view->setHeader('Location', $this->generateUrl('fitfix_api_invoicerest_get_invoice', array(
    			 		'invoice' => $invoice->getId(),
    			 		'_format' => $this->getRequest()->get('_format', 'json')
    			 ), true));
    			*/
    		}
    
    		$view->setData($trainer);
    
    		return $view;
    
    	}
    
    	return $view->setData($form, 400);
    
    }
    
    /**
     * Returns a trainer by id.
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER, ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getTrainerAction()
    {
    	
    	$user = $this->getUser();
    	/* @var $user User */
    	
    	$trainer = null;
    	
    	if($user->getTrainer()){
    		
    		$trainer = $user->getTrainer();
    		
    	} else {
    		
    		$trainer = $user->getClient()->getTrainer();
    		
    	}
    	
    	$serializationContext = SerializationContext::create()->setGroups(array("trainer-profile"));
    	
    	$view = $this->view($trainer);
    	$view->setSerializationContext($serializationContext);
    	
    	return $view;
    	
    }

    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return FOSView
     */
    private function get_errors_view($errors)
    {
        $msgs = array();
        $it = $errors->getIterator();
        //$val = new \Symfony\Component\Validator\ConstraintViolation();
        foreach ($it as $val) {
            $msg = $val->getMessage();
            $params = $val->getMessageParameters();
            //using FOSUserBundle translator domain 'validators'
            $msgs[$val->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = FOSView::create($msgs);
        $view->setStatusCode(400);
        return $view;
    }

}
