<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Technique;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Technique.
 *
 * @NamePrefix("fitfix_api_techniquerest_")
 * @author Gavin Williams <gavin.williams@fishrod.co.uk>
 */
class TechniqueRestController extends Controller
{

    /**
     * Returns an technique by id.
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getTechniqueAction($id)
    {
    
    	$em = $this->getDoctrine()->getManager();
    	
    	$view = FOSView::create();
    
    	$entity = $em->getRepository('FitFixCoreBundle:Technique')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Goal entity.');
    	}
    
    	if ($entity) {
    		$view->setSerializationContext(SerializationContext::create()->setGroups(array("details", "list")));
    		$view->setStatusCode(200)->setData($entity);
    	} else {
    		$view->setStatusCode(404);
    	}
    
    	return $view;
    }

    /**
     * Returns all techniques.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getTechniquesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $view = FOSView::create();

        $entities = $em->getRepository('FitFixCoreBundle:Technique')->findAll();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find technique entities.');
        }

        if ($entities) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }
}