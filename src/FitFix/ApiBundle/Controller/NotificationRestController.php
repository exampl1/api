<?php

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializationContext;
use FitFix\CoreBundle\Entity\Notification;


/**
 * Provider of notifications
 * @author gavinwilliams
 * @NamePrefix("fitfix_api_notificationrest_")
 */

class NotificationRestController extends FOSRestController {
	
	/**
	 * @ApiDoc(
	 * 	section="Notifications",
	 * 	resource=true
	 * )
	 */
	public function getNotificationsAction(){
		
		$user = $this->getUser();
		
		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository('FitFixCoreBundle:Notification');
		
		$notifications = $repo->findByUser($user);
		
		return $this->view($notifications);
		
	}
	
}