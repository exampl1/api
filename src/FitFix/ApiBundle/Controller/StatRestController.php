<?php

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FitFix\CoreBundle\Entity\Stat;
use FitFix\CoreBundle\Form\StatType;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FitFix\CoreBundle\Entity\User;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * 
 * @author gavinwilliams
 *
 * @NamePrefix("fitfix_api_statrest_")
 */
class StatRestController extends FOSRestController {
	
	const OPERATION_READ = "read";
	const OPERATION_DELETE = "delete";
	const OPERATION_CREATE = "create";
	
	/**
	 * Gets stats for a specific user
	 *
	 * @ApiDoc(
	 * 	section="Stats",
	 * 	resource=true
	 * )
	 * 
	 * @param User $user
	 * 
	 * @ParamConverter("user", class="FitFixCoreBundle:User")
	 */
	public function getUserStatsAction(User $user){
		
		// Check to see whether the current user is authorised to post a new stat
		if(!$this->isAuthorised($user, self::OPERATION_READ)){
			return $this->view(null, 403);
		}
		
		$searchCriteria = array('user' => $user);
		
		// Check to see whether there's a date in the query params
		$timestamp = $this->getRequest()->get('date');
		if($timestamp){
			try {
				$date = new \DateTime("@" . $timestamp);
				$date->setTime(0, 0, 0);
				$searchCriteria['date'] = $date;
			} catch (\Exception $e){
				throw new HttpException(400, "Date query parameter must be a unix timestamp");
			}
		}
			
		
		$view = $this->view();
		
		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository('FitFixCoreBundle:Stat');
		$result = $repo->findBy($searchCriteria);
		
		$context = SerializationContext::create()->setGroups(array('stat-list'));
		
		$view->setSerializationContext($context);
		
		return $view->setData($result);
	}
	
	/**
	 * Gets the users specific stat
	 * 
	 * @ApiDoc(
	 * 	section="Stats",
	 * 	resource=true
	 * )
	 * 
	 * @param User $user
	 * @param Stat $stat
	 * 
	 * @ParamConverter("user", class="FitFixCoreBundle:User")
	 * @ParamConverter("stat", class="FitFixCoreBundle:Stat")
	 */
	public function getUserStatAction(User $user, Stat $stat){
		// Check to see whether the current user is authorised to post a new stat
		if(!$this->isAuthorised($user, self::OPERATION_READ)){
			return $this->view(null, 403);
		}
		
		$view = $this->view($stat);
				
		$context = SerializationContext::create()->setGroups(array('stat-details'));
		
		$view->setSerializationContext($context);
		
		return $view;
		
	}
	
	/**
	 * Deletes the users stat
	 * 
	 * @ApiDoc(
	 * 	section="Stats",
	 * 	resource=true
	 * )
	 * 
	 * @param User $user
	 * @param Stat $stat
	 * 
	 * @ParamConverter("user", class="FitFixCoreBundle:User")
	 * @ParamConverter("stat", class="FitFixCoreBundle:Stat")
	 */
	public function deleteUserStatAction(User $user, Stat $stat){
		// Check to see whether the current user is authorised to post a new stat
		if(!$this->isAuthorised($user, self::OPERATION_DELETE)){
			return $this->view(null, 403);
		}
		
		$em = $this->getDoctrine()->getManager();
		
		$em->remove($stat);
		$em->flush();
		
		return $this->view(null, 200);
		
	}
	
	/**
	 * Posts a stat for a specific user
	 * 
	 * @ApiDoc(
	 * 	section="Stats",
	 * 	resource=true,
	 * 	input="FitFix\CoreBundle\Form\StatType"
	 * )
	 * 
	 * @param User $user
	 * 
	 * @ParamConverter("user", class="FitFixCoreBundle:User")
	 */
	public function postUserStatsAction(User $user){
		
		// Check to see whether the current user is authorised to post a new stat
		if(!$this->isAuthorised($user, self::OPERATION_CREATE)){
			return $this->view(null, 403);
		}
		
		$stat = new Stat();
		$stat->setUser($user);
		
		$view = $this->processForm($stat);
		
		if($view->getStatusCode() != 400){
			$context = SerializationContext::create()->setGroups(array('stat-details'));
			$view->setSerializationContext($context);
		}
		
		return $view;
	}
	
	/**
	 * Check to see whether the current logged in user is authorised to perform a specifc operation
	 * @param User $user
	 */
	private function isAuthorised(User $user, $operation){
		
		if($operation == self::OPERATION_READ){
			// If this is the users trainer
			if($user->getClient()->getTrainer()->getUser()->getId() == $this->getUser()->getId()){
				return true;
			}
			
			// If the current logged in user is the user
			if($user->getId() == $this->getUser()->getId()){
				return true;
			}
		}
		
		if($operation == self::OPERATION_DELETE){
			// If this is the users trainer
			if($user->getClient()->getTrainer()->getUser()->getId() == $this->getUser()->getId()){
				return true;
			}
		}
		
		if($operation == self::OPERATION_CREATE){
			// If this is the users trainer
			if($user->getClient()->getTrainer()->getUser()->getId() == $this->getUser()->getId()){
				return true;
			}
		}
		
		return false;
		
	}
	
	/**
	 * Processes a stat object using a form
	 * @param Stat $stat
	 * @return FOS\RestBundle\View
	 */
	private function processForm(Stat $stat){
	
		$statusCode = $stat->getId() ? 204 : 201;
		$view = $this->view();
		$data;
		$requestBodyType = $this->getRequest()->getContentType();
	
		$currentuser = $this->getUser();
	
		$form = $this->createForm(new StatType(), $stat);
	
		if($requestBodyType !== null){
			if($requestBodyType == 'json'){
				$data = json_decode($this->getRequest()->getContent(), true);
				if($data === null){
	
					switch (json_last_error()) {
						default:
							return;
						case JSON_ERROR_DEPTH:
							$error = 'Maximum stack depth exceeded';
							break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Underflow or the modes mismatch';
							break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_UTF8:
							$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							break;
					}
	
					$view->setData(array('error' => $error));
					$view->setStatusCode(400);
					return $view;
	
				}
			}
		} else {
			$data = $this->getRequest();
		}
	
		// Remove the ID, it'll break EVERYTHING!
		
		$form->submit($data);
	
		if($form->isValid()){
	
			$em = $this->getDoctrine()->getManager();
	
			$storage = $this->get('fitfix_core.storage.s3');
			
			($storage instanceof \FitFix\CoreBundle\Service\Storage);
			
			if($stat->getFrontPhoto()){
				$frontPhoto = $storage->uploadUniqueFileWithData('fitfixuserdata', 'bodystats', $stat->getFrontPhoto(), 'jpg', 'image/jpeg');
				$stat->setFrontPhoto($frontPhoto);
			}
			
			if($stat->getBackPhoto()){
				$backPhoto = $storage->uploadUniqueFileWithData('fitfixuserdata', 'bodystats', $stat->getBackPhoto(), 'jpg', 'image/jpeg');
				$stat->setBackPhoto($backPhoto);
			}
			
			if($stat->getSidePhoto()){
				$sidePhoto = $storage->uploadUniqueFileWithData('fitfixuserdata', 'bodystats', $stat->getSidePhoto(), 'jpg', 'image/jpeg');
				$stat->setSidePhoto($sidePhoto);
			}
			
			
			$em->persist($stat);
			$em->flush();
			$view->setStatusCode($statusCode);
			if (201 == $statusCode) {
	
				$view->setHeader('Location', $this->generateUrl('fitfix_api_statrest_get_user_stat', array(
						'user' => $stat->getUser()->getId(),
						'stat' => $stat->getId(),
						'_format' => $this->getRequest()->get('_format', 'json')
				), true));
			}
	
			$view->setData($stat);
	
			return $view;
	
		}
		
		print_r($form->getErrorsAsString());
			
		return $view->setData($form, 400);
	
	}
	
}