<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Workout;
use FitFix\CoreBundle\Entity\Client;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use Doctrine\Common\Collections\ArrayCollection;

use \DateTime;
use FitFix\CoreBundle\Entity\Step;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller that provides Restful services over the resource Workout.
 *
 * @NamePrefix("fitfix_api_workoutrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class WorkoutRestController extends FOSRestController
{

    /**
     * Returns all workouts by client.
     *
     * @param string $slug slug
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getWorkoutsAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        
        $view = $this->view();

        //TODO this looks like a bug, commenting out
        //$client = $authenticatedUser->getClient();

        $roles = $user->getRoles();
        if (in_array('ROLE_CLIENT', $roles)) {
            $client = $user->getClient();
            $entities = $em->getRepository('FitFixCoreBundle:Workout')->findByClient($client);
        } elseif (in_array('ROLE_TRAINER', $roles)) {
            $trainer = $user->getTrainer();
            $entities = $em->getRepository('FitFixCoreBundle:Workout')->findTemplatesByTrainer($trainer);
        }

        $view->setSerializationContext(SerializationContext::create()->setGroups(array("workout-list")));
        $view->setStatusCode(200)->setData($entities);


        return $view;
    }

    /**
     * Returns an workout by id.
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getWorkoutAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $entity = $em->getRepository('FitFixCoreBundle:Workout')->find($id);

        $view = $this->view($entity);
        
        $view->setSerializationContext(SerializationContext::create()->setGroups(array("workout-details")));
        $view->setStatusCode(200)->setData($entity);


        return $view;
    }

    /**
     * Creates a new Workout entity.
     * Using param_fetcher_listener: force
     *
     * @param string $slug slug
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="name", requirements="\d+", default="", description="Name")
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function postWorkoutsAction(ParamFetcher $paramFetcher)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $userManager = $this->container->get('fos_user.user_manager');

        $params = array();
        
        $content = $this->get('request')->getContent();
        
        if(!empty($content)){
        	$params = json_decode($content);
        }
        
        $clients = (isset($params->clients)) ? $params->clients : array();
        
        $workout = new Workout();
        $workout->setCreated(new DateTime());
        $workout->setName($params->name);
        $workout->setTrainer($authenticatedUser->getTrainer());
        
        if(isset($params->warmup)){
        
	        foreach($params->warmup as $wstep){
	        	$step = new Step();
	        	$step->setName($wstep->name);
	        	$step->setRest($wstep->rest);
	        	$step->setSets($wstep->sets);
	        	$step->setTempo($wstep->tempo);
	        	$step->setWeight((int) $wstep->weight);
	        	$step->setReps($wstep->reps);
	
	        	$exercise = $em->getRepository('FitFixCoreBundle:Exercise')->find($wstep->exercise->id);
	        	$technique = $em->getRepository('FitFixCoreBundle:Technique')->find($wstep->technique->id);
	        	
	        	$step->setExercise($exercise);
	        	$step->setTechnique($technique);
	        	
	        	$workout->addWarmup($step);
	        }
        
        }
        
        if(isset($params->warmdown)){
        
	        foreach($params->warmdown as $wstep){
	        	$step = new Step();
	        	$step->setName($wstep->name);
	        	$step->setRest($wstep->rest);
	        	$step->setSets($wstep->sets);
	        	$step->setTempo($wstep->tempo);
	        	$step->setWeight((int) $wstep->weight);
	        	$step->setReps($wstep->reps);
	        
	        	$exercise = $em->getRepository('FitFixCoreBundle:Exercise')->find($wstep->exercise->id);
	        	$technique = $em->getRepository('FitFixCoreBundle:Technique')->find($wstep->technique->id);
	        	
	        	$step->setExercise($exercise);
	        	$step->setTechnique($technique);
	        	 
	        	$workout->addWarmdown($step);
	        }
        
        }
        
        if(isset($params->workout)){
        
	        foreach($params->workout as $wstep){
	        	$step = new Step();
	        	$step->setName($wstep->name);
	        	$step->setRest($wstep->rest);
	        	$step->setSets($wstep->sets);
	        	$step->setTempo($wstep->tempo);
	        	$step->setWeight((int) $wstep->weight);
	        	$step->setReps($wstep->reps);
	        
	        	$exercise = $em->getRepository('FitFixCoreBundle:Exercise')->find($wstep->exercise->id);
	        	$technique = $em->getRepository('FitFixCoreBundle:Technique')->find($wstep->technique->id);
	        	
	        	$step->setExercise($exercise);
	        	$step->setTechnique($technique);
	        
	        	$workout->addWorkout($step);
	        }
        
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($workout);

        $view = $this->view();
        
        if (count($errors) == 0) {
        	if(count($clients) > 0){
        		$this->_assignToClients($clients, $workout, $em);
        	} else {
        		$em->persist($workout);
        	}
            
            $em->flush();
            $params = array(
                "id" => $workout->getId()
            );
            //$view = RouteRedirectView::create("fitfix_api_workoutrest_get_trainer_workout", $params);
            $view->setData($workout);
            $view->setStatusCode(201);
        } else {
            $view = $this->get_errors_view($errors);
        }
        
        return $view;
    }
    
    private function _assignToClients(array $clients, Workout $workout, $em){
    	
    	$myClients = $this->getUser()->getTrainer()->getClients();
    	/* @var $myClients ArrayCollection */
    	
    	foreach ($clients as $client){
    		
    		// First check to see if the client belongs to the PT
    		$theClient = $myClients->filter(function(Client $clientObj) use ($client){
    			return ($clientObj->getId() == $client);
    		});
    		
    		
    		if($theClient->count() == 1){
    			$clonedWorkout = clone $workout;
    			$workout->setClient($theClient->first());
    			$em->persist($clonedWorkout);
    		}
    		
    	}
    	
    }

    /**
     * Update an workout by id.
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @RequestParam(name="name", requirements="\d+", default="", description="Name")
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function putWorkoutAction($id)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();



        $em = $this->getDoctrine()->getManager();
        $workout = $em->getRepository('FitFixCoreBundle:Workout')->find($id);
        
        if ($workout->getTrainer()->getId() !== $authenticatedUser->getTrainer()->getId()) {
        	$view->setStatusCode(401);
        	return $view;
        }
        
        if (!$workout) {
            throw $this->createNotFoundException('Unable to find Workout entity.');
        }
        
        ($workout instanceof Workout);
        
        $params = array();
        
        $content = $this->get('request')->getContent();
        
        if(!empty($content)){
        	$params = json_decode($content);
        }
        
        $clients = (isset($params->clients)) ? $params->clients : array();

        $workout->setCreated(new DateTime());
        $workout->setName($params->name);
        $workout->setTrainer($authenticatedUser->getTrainer());
        
        foreach($workout->getWarmdown() as $warmdown){
        	$em->remove($warmdown);
        	$em->flush();
        }
        $workout->getWarmdown()->clear();
        
        foreach($workout->getWarmup() as $warmup){
        	$em->remove($warmup);
        	$em->flush();
        }
        $workout->getWarmup()->clear();
        
        foreach($workout->getWorkout() as $theworkout){
        	$em->remove($theworkout);
        	$em->flush();
        }
        
        $workout->getWorkout()->clear();
        
        foreach($params->warmup as $wstep){
        	$step = new Step();
        	$step->setName($wstep->name);
        	$step->setRest($wstep->rest);
        	$step->setSets($wstep->sets);
        	$step->setTempo($wstep->tempo);
        	$step->setWeight((int) $wstep->weight);
        	$step->setReps($wstep->reps);

        	$exercise = $em->getRepository('FitFixCoreBundle:Exercise')->find($wstep->exercise->id);
        	$technique = $em->getRepository('FitFixCoreBundle:Technique')->find($wstep->technique->id);
        	
        	$step->setExercise($exercise);
        	$step->setTechnique($technique);
        	
        	$workout->addWarmup($step);
        }
        
        foreach($params->warmdown as $wstep){
        	$step = new Step();
        	$step->setName($wstep->name);
        	$step->setRest($wstep->rest);
        	$step->setSets($wstep->sets);
        	$step->setTempo($wstep->tempo);
        	$step->setWeight((int) $wstep->weight);
        	$step->setReps($wstep->reps);
        
        	$exercise = $em->getRepository('FitFixCoreBundle:Exercise')->find($wstep->exercise->id);
        	$technique = $em->getRepository('FitFixCoreBundle:Technique')->find($wstep->technique->id);
        	
        	$step->setExercise($exercise);
        	$step->setTechnique($technique);
        	 
        	$workout->addWarmdown($step);
        }
        
        foreach($params->workout as $wstep){
        	$step = new Step();
        	$step->setName($wstep->name);
        	$step->setRest($wstep->rest);
        	$step->setSets($wstep->sets);
        	$step->setTempo($wstep->tempo);
        	$step->setWeight((int) $wstep->weight);
        	$step->setReps($wstep->reps);
        
        	$exercise = $em->getRepository('FitFixCoreBundle:Exercise')->find($wstep->exercise->id);
        	$technique = $em->getRepository('FitFixCoreBundle:Technique')->find($wstep->technique->id);
        	
        	$step->setExercise($exercise);
        	$step->setTechnique($technique);
        
        	$workout->addWorkout($step);
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($workout);

        if (count($errors) == 0) {
        	
            if(count($clients) > 0){
        		$this->_assignToClients($clients, $workout, $em);
        	} else {
        		$em->persist($workout);
        	}
        	
            $em->flush();
            $view = FOSView::create();
            $view->setData($workout);
            $view->setStatusCode(200);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Delete an workout by ID
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function deleteWorkoutAction($id)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        $em = $this->getDoctrine()->getManager();
        $workout = $em->getRepository('FitFixCoreBundle:Workout')->find($id);
        
        if($workout->getTrainer()->getId() !== $authenticatedUser->getTrainer()->getId()){
        	throw new AccessDeniedHttpException();
        }
        
        if ($workout) {
            
        	$workout->setActive(false);
        	
        	$em->persist($workout);        	
            $em->flush();
            $view->setStatusCode(204)->setData("Workout removed.");
        } else {
            $view->setStatusCode(204)->setData("No data available.");
        }
        return $view;
    }

    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return FOSView
     */
    private function get_errors_view($errors)
    {
        $msgs = array();
        $it = $errors->getIterator();
        //$val = new \Symfony\Component\Validator\ConstraintViolation();
        foreach ($it as $val) {
            $msg = $val->getMessage();
            $params = $val->getMessageParameters();
            //using FOSUserBundle translator domain 'validators'
            $msgs[$val->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = FOSView::create($msgs);
        $view->setStatusCode(400);
        return $view;
    }

}
