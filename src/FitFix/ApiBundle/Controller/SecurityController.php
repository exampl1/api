<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

/**
 * Controller that provides Restfuls security functions.
 *
 * @Prefix("/security")
 * @NamePrefix("fitfix_api_securityrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class SecurityController extends Controller
{

    /**
     * WSSE Token generation
     *
     * @return FOSView
     * @throws AccessDeniedException
     * @Post("/token/create")
     * @ApiDoc()
     */
    public function postTokenCreateAction()
    {
	      $view = FOSView::create();
        $request = $this->getRequest();

        $username = $request->get('_username');
        $password = $request->get('_password');

        //$csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');
        //$data = array('csrf_token' => $csrfToken,);

        $um = $this->get('fos_user.user_manager');
        $user = $um->findUserByUsernameOrEmail($username);

        if (!$user instanceof User) {
            throw new AccessDeniedException("Your username or password appears to be incorrect");
        }

        $group = ($user->getTrainer()) ? "PT" : "client";

	      if($group === 'PT'){
          if($user->getTrainer()->isExpired()){
            throw new AccessDeniedException("Your subscription has expired");
          }
        }
        if($group === 'client'){
          if($user->getClient()->getArchived()){
            throw new AccessDeniedException("Your personal trainer has deactivated your account");
          }
        }

        $created = date('c');
        $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
        $nonceHigh = base64_encode($nonce);
        $passwordDigest = base64_encode(sha1($nonce . $created . $password . "{".$user->getSalt()."}", true));
        $header = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceHigh}\", Created=\"{$created}\"";
        $view->setHeader("Authorization", 'WSSE profile="UsernameToken"');
        $view->setHeader("X-WSSE", "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceHigh}\", Created=\"{$created}\"");
        $data = array('WSSE' => $header);
        $view->setStatusCode(200)->setData($data);
        return $view;
    }


    /**
     * WSSE Token Remove
     *
     * @return FOSView
     * @ApiDoc()
     */
    public function getTokenDestroyAction()
    {
        $view = FOSView::create();
        $security = $this->get('security.context');
        $token = new AnonymousToken(null, new User());
        $security->setToken($token);
        $this->get('session')->invalidate();
        $view->setStatusCode(200)->setData('Logout successful');
        return $view;
    }

    /**
     * WSSE Token forget
     *
     * @return FOSView
     * @throws AccessDeniedException
     * @Post("/token/forget")
     * @ApiDoc()
     */
    public function postTokenForgetAction()
    {
       $request = $this->getRequest();
       $email = $request->get('_email');

       //Password::entropicPassword();

       $em = $this->getDoctrine()->getManager();
       $letter = $em->getRepository('FitFixCoreBundle:Letter')->findOneById(5);
       $um = $this->get('fos_user.user_manager');
       $user = $um->findUserByUsernameOrEmail($email);

       if (!$user instanceof User) {
            throw new AccessDeniedException("Your email appears to be incorrect");
        }


       $messagetext = str_replace('{password}','qw34!57f' , $letter->getMessageBody());
       $messagetext = str_replace('{link}', "http://site-s.fitfixpro.com/app_dev.php/".$user->getId().'/'.$user->getPassword().'/'.'qw34!57f',$messagetext);
      //     $message = \Swift_Message::newInstance()
      //     ->setSubject($letter->getTitle())
      //     ->setFrom('alex.pay1990@gmail.com')
      //     ->setTo($email)
      //     ->setBody(
            
      //        $messagetext
      //         )
      //     )
      // ;
      // $this->get('mailer')->send($message);

       $message = \Swift_Message::newInstance()
                ->setSubject($letter->getTitle())
                ->setFrom("alex.pay1990@gmail.com")
                ->setTo($email)
                ->setBody($messagetext, 'text/html');

       //mail($email, $letter->getTitle(), $messagetext);
       //$this->get('mailer')->send($message);
       return "Emeil sent";
    }

    public function resetAction($uid, $old, $new)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('FitFixCoreBundle:User')->findById($uid);
        if($user[0]->getPassword() == $old) {
            $um = $this->get('fos_user.user_manager');
            $fos_user = $um->findUserByUsernameOrEmail($user[0]->getEmail());
            $fos_user->setPlainPassword($new);
            $um->updateUser($fos_user);
            die("Password changed!");
        }
        return "Failed!";
    }
}