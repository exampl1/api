<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Mealplan;
use FitFix\CoreBundle\Entity\Meal;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\HttpFoundation\Response;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SerializerBundle\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use \DateTime;
use Symfony\Bridge\Monolog\Logger;
use FOS\RestBundle\Controller\FOSRestController;
use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller that provides Restful services over the resource Mealplan.
 *
 * @NamePrefix("fitfix_api_planest_")
 * @author Daniele Longo <daniele.longo.development@gmail.com>
 */
class MealplanRestController extends FOSRestController
{
	
	/**
	 * Deletes a mealplan
	 * @param Mealplan $mealplan
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * @ApiDoc()
	 * 
	 * @ParamConverter("mealplan", class="FitFixCoreBundle:Mealplan")
	 */
	public function deleteMealplanAction(Mealplan $mealplan){
		
		$trainer = $this->getUser()->getTrainer();
		
		if($trainer->getId() !== $mealplan->getTrainer()->getId()){
			throw new AccessDeniedHttpException();
		}
		
		$em = $this->getDoctrine()->getManager();
		
		$em->remove($mealplan);
		
		$em->flush();
		
		return $this->view(null, 204);
		
	}

    /**
     * Returns an mealplan by id.
     *
     * @param string $userID user id (client or trainer)
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     * 
     * @ParamConverter("mealplan", class="FitFixCoreBundle:Mealplan")
     */
    public function getMealplanAction(Mealplan $mealplan)
    {

    	
    	if($this->getUser()->getTrainer()){
    		if($mealplan->getTrainer()->getId() !== $this->getUser()->getTrainer()->getId()){
    			throw new AccessDeniedHttpException();
    		}
    	} else if($this->getUser()->getClient()){
    		if($mealplan->getClient()->getId() !== $this->getUser()->getClient()->getId()){
    			throw new AccessDeniedHttpException();
    		}
    	}
    	
    	$view = $this->view($mealplan);

        return $view;
    }

    /**
     * Creates a new Mealplan entity.
     * Using param_fetcher_listener: force
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function postMealplanAction(ParamFetcher $paramFetcher)
    {
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $view = $this->view();

        if (!$authenticatedUser->getUsername()) {
            $view->setStatusCode(401);
            return $view;
        }

        $trainer = $authenticatedUser->getTrainer();

        $request = $this->getRequest();
        $userManager = $this->container->get('fos_user.user_manager');

        $clients = $request->get('clients');
        
        foreach($clients as $client){
	        	
	        $mealplan = new Mealplan();
	        
	        $mealplan->setCreated(new DateTime());
	        $mealplan->setName($request->get('name'));
	        
	        $client = $em->getRepository("FitFixCoreBundle:Client")->find($client);
	        
	        if($client->getTrainer()->getId() !== $trainer->getId()){
	        	break;
	        }
	        
	        $mealplan->setClient($client);
	
	        $mealplan->setTrainer($trainer);
	
	        $meals = $request->get('meals');
	        
	        $totalCalories = 0;
	
	        foreach($meals as $mealdata) {
	
	            $mealplanItem = new Meal();
	            $mealplanItem->setType($mealdata["type"]);   
	            $mealplanItem->setQuantity($mealdata["quantity"]);
	            $mealplanItem->setCalories($mealdata["calories"]); 
	            $mealplanItem->setMealplan($mealplan);
	            $mealplanItem->setPosition($mealdata["position"]);
	
	            $food = $em->getRepository('FitFixCoreBundle:Food')->find($mealdata["food_id"]);
	            
	            $mealplanItem->setFood($food);
	
	            $mealplanItem->setMealplan($mealplan);
	            $mealplan->addMeal($mealplanItem);
	
	            $totalCalories += $mealdata["calories"];
	        }
	
	        $mealplan->setCalories($totalCalories);
        
        	
        	$validator = $this->get('validator');
        	$errors = $validator->validate($mealplan);
        	
        	if (count($errors) == 0) {
        		$em = $this->getDoctrine()->getManager();
        		$em->persist($mealplan);
        		$em->flush();
        		$view->setData($mealplan);
        	} else {
        		$view = $this->get_errors_view($errors);
        	}
        	
        }
        
        return $view;
        
    }

}