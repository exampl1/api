<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Package;
use FitFix\CoreBundle\Entity\User;
use FitFix\CoreBundle\Entity\Trainer;
use FitFix\CoreBundle\Entity\Client;

use FitFix\CoreBundle\Repository\PackageRepository;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use FOS\RestBundle\Controller\FOSRestController;
use FitFix\CoreBundle\Form\PackageType;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Controller that provides Restful services over the resource Package.
 *
 * @NamePrefix("fitfix_api_packagerest_")
 * @author Gavin Williams <gavin.williams@fishrod.co.uk>
 * 
 * - A PT can create packages - DONE
 * - A PT can list all of their own packages - DONE
 * - A PT can edit their packages - DONE
 * - A PT can delete a package - DONE
 * - A Client can list all of the PT's packages - DONE
 */
class PackageRestController extends FOSRestController
{
	
	/**
	 * Gets packages for the currently logged in trainer
	 * 
	 * @ApiDoc(
	 * 	resource="Packages",
	 * 	section="Packages",
	 * 	output="FitFix\CoreBundle\Entity\Package"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER, ROLE_CLIENT")
	 * 
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function getPackagesAction(){
		
		// Get the repo
		$repo = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:Package');
		/* @var $repo PackageRepository */
		
		$user = $this->getUser();
		/* @var $user User */
		if($user->getTrainer()){
			$trainer = $user->getTrainer();
		} else {
			$trainer = $user->getClient()->getTrainer();
		}
		/* @var $trainer Trainer */
		
		$packages = $repo->findByTrainer($trainer);
		
		$view = $this->view($packages);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array("package-list")));
		
		return $this->handleView($view);
		
	}
	
	/**
	 * Gets the packages for a specific trainer for the logged in client
	 * 
	 * @ApiDoc(
	 * 	resource="Packages",
	 * 	section="Packages",
	 * 	output="FitFix\CoreBundle\Entity\Package"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @ParamConverter("trainer", class="FitFixCoreBundle:Trainer")
	 * 
	 * @param Trainer $trainer
	 */
	public function getTrainersPackagesAction(Trainer $trainer){
				
		$user = $this->getUser();
		/* @var $user User */
		
		$client = $user->getClient();
		/* @var $client Client */
		
		// Check if the logged in client has access to this trainers details
		if($client->getTrainer()->getId() !== $trainer->getId()){
			throw new HttpException(403);
		}
		
		$repo = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:Package');
		/* @var $repo PackageReository */
		
		$data = $repo->findByTrainer($trainer);
		
		$view = $this->view($data);
		$view->setSerializationContext(SerializationContext::create()->setGroups(array("package-list")));
		
		return $this->handleView($view);
		
	}
	
	/**
	 * Posts a package to the currently logged in trainers package list
	 * 
	 * @ApiDoc(
	 * 	resource="Packages",
	 * 	section="Packages",
	 * 	output="FitFix\CoreBundle\Entity\Package",
	 * 	input="FitFix\CoreBundle\Form\PackageType"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function postPackagesAction(){
		
		$package = new Package();
		$package->setTrainer($this->getUser()->getTrainer());
		
		$view = $this->processForm($package);
		$view->setSerializationContext(SerializationContext::create()->setGroups(array("package-details")));
		
		return $this->handleView($view);
		
	}
	
	/**
	 * Updates a package for the currently logged in trainer
	 * 
	 * @ApiDoc(
	 * 	resource="Packages",
	 * 	section="Packages",
	 * 	output="FitFix\CoreBundle\Entity\Package",
	 * 	input="FitFix\Curebundle\Entity\PackageType"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("package", class="FitFixCoreBundle:Package")
	 * 
	 * @param Package $package
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function putPackageAction(Package $package){
		
		if($package->getTrainer()->getId() !== $this->getUser()->getTrainer()->getId()){
			throw new HttpException(403);
		}
		
		$view = $this->processForm($package);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array("package-details")));
		
		return $this->handleView($view);
		
	}
	
	/**
	 * Deletes a package for the currently logged in trainer
	 * 
	 * @ApiDoc(
	 * 	resource="Packages",
	 * 	section="Packages"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("package", class="FitFixCoreBundle:Package")
	 * 
	 * @param Package $package
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function deletePackageAction(Package $package){
		
		if($package->getTrainer()->getId() !== $this->getUser()->getTrainer()->getId()){
			throw new HttpException(403);
		}
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($package);
		$em->flush();
		
		$view = $this->view(null, 204);

		return $this->handleView($view);
		
	}
	
	/**
	 * Processes a package object using a form
	 * @param Package $package
	 * @return FOS\RestBundle\View
	 */
	private function processForm(Package $package){
	
		$statusCode = $package->getId() ? 204 : 201;
		$view = $this->view();
		$data;
		$requestBodyType = $this->getRequest()->getContentType();
	
		$form = $this->createForm(new PackageType(), $package);
	
		if($requestBodyType !== null){
			if($requestBodyType == 'json'){
				$data = json_decode($this->getRequest()->getContent(), true);
				if($data === null){
	
					switch (json_last_error()) {
						default:
							return;
						case JSON_ERROR_DEPTH:
							$error = 'Maximum stack depth exceeded';
							break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Underflow or the modes mismatch';
							break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_UTF8:
							$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							break;
					}
	
					$view->setData(array('error' => $error));
					$view->setStatusCode(400);
					return $view;
	
				}
			}
		} else {
			$data = $this->getRequest();
		}
	
		// Remove the ID, it'll break EVERYTHING!
		unset($data['id']);
	
		$form->submit($data, false);
	
		if($form->isValid()){
	
			$em = $this->getDoctrine()->getManager();
	
			$em->persist($package);
			$em->flush();
			$view->setStatusCode($statusCode);
			if (201 == $statusCode) {
				/*
					$view->setHeader('Location', $this->generateUrl('fitfix_api_invoicerest_get_invoice', array(
							'invoice' => $invoice->getId(),
							'_format' => $this->getRequest()->get('_format', 'json')
					), true));
				*/
			}
	
			$view->setData($package);
	
			return $view;
	
		}
	
		return $view->setData($form, 400);
	
	}
	
	
	
}