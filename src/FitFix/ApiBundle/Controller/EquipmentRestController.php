<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Goal;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Equipment.
 *
 * @NamePrefix("fitfix_api_equipmentrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class EquipmentRestController extends Controller
{
    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function optionsEquipmentsAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
        return $view;
    }

    /**
     * Returns all equipment.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getEquipmentsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $view = FOSView::create();

        $request = $this->getRequest();
        if (!$orderBy = $request->get('orderBy')) {
            $orderBy = 'name';
        }
        if (!$limit = $request->get('limit')) {
            $limit = null;
        }
        if (!$offset = $request->get('limit')) {
            $offset = null;
        }

        $entities = $em->getRepository('FitFixCoreBundle:Equipment')->findBy(
            array(),
            array(
                $orderBy => 'ASC'
            ),
            $limit,
            $offset
        );

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Equipment entities.');
        }

        if ($entities) {
        	$view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }
}