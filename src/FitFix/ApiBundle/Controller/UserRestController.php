<?php

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\Serializer\SerializationContext;
use FitFix\CoreBundle\Entity\User;

class UserRestController extends FOSRestController {
	
	/**
	 * Updates a user with a username and password
	 *
	 * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
	 * @ApiDoc()
	 */
	public function getUserAction(){
		$user = $this->getUser();
		
		$serializationContext = SerializationContext::create()->setGroups(array("user-details"));
		
		$view = $this->view($user);
		
		$view->setSerializationContext($serializationContext);
		
		return $view;
		
	}
	
	/**
	 * Updates a user with a username and password
	 * 
	 * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
	 */
	public function putUserAction(){
		
		$userManager = $this->get('fos_user.user_manager');
		
		$user = $this->getUser();
		/* @var User $user */
		$email = $this->getRequest()->get('email');
		$password = $this->getRequest()->get('password');
		
		$user->setUsername($email);
		$user->setEmail($email);
		
		if($password){
			$user->setPlainPassword($password);
		}
		
		$userManager->updateUser($user);
		
	}
	
}