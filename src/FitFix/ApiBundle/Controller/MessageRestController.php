<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Message;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Address.
 *
 * @NamePrefix("fitfix_api_messagerest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class MessageRestController extends Controller
{
    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function optionsMessagesAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
        return $view;
    }
	
    /**
     * Returns all messages.
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER, ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getMessagesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        $roles = $authenticatedUser->getRoles();

        if (in_array('ROLE_CLIENT', $roles)) {
            $client = $authenticatedUser->getClient();
            $entities = $em->getRepository('FitFixCoreBundle:Thread')->findByClient($client);
        }
        elseif (in_array('ROLE_TRAINER', $roles)) {
            $trainer = $authenticatedUser->getTrainer();
            $entities = $em->getRepository('FitFixCoreBundle:Thread')->findByTrainer($trainer);
        }
        	
        if ($entities) {
            $serializer = $this->container->get('serializer');
            
            $serializationContext = SerializationContext::create()->setGroups(array('details'));
            $view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }
    /**
     * Returns an address by id.
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getMessageAction($id)
    {
        //
    }

    /**
     * Creates a new Message entity.
     * Using param_fetcher_listener: force
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="messageBody", requirements="\d+", default="", description="Message Body")
     * @RequestParam(name="messageRole", requirements="\d+", default="", description="Message Role")
     * @RequestParam(name="client_id", requirements="\d+", default="", description="Client Id")
     * @RequestParam(name="trainer_id", requirements="\d+", default="", description="Trainer Id")
     *
     * @return FOSView
     * @ApiDoc()
     */
    public function postMessagesAction(ParamFetcher $paramFetcher)
    {
        $view = FOSView::create();

        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();

        $message = new Message();

        $message->setMessageBody($request->get('messageBody'));
        $message->setMessageRole($request->get('messageRole'));
        //$message->setThreadId($request->get('threadId'));

        if ($request->get('thread_id')) {
            $thread = $em->getRepository('FitFixCoreBundle:Thread')->find($request->get('thread_id'));
            if ($thread) {
                $message->setThread($thread);
            }
        }
        //die($message->getThread());
        $message->setCreatedAt(new DateTime());

        $validator = $this->get('validator');
        $errors = $validator->validate($message);

        if (count($errors) == 0) {
           
            $em->persist($message);
            $em->flush();
            $params = array(
                "id" => $message->getId()
            );
            //$view = RouteRedirectView::create("fitfix_api_messagerest_get_message", $params);
            $view->setStatusCode(200)->setData($params);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }
}