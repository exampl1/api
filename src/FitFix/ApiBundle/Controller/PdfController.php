<?php
namespace FitFix\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FitFix\CoreBundle\Entity\Trainer;
use FitFix\AdminBundle\Form\TrainerType;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Trainer controller.
 *
 * @Route("/pdf")
 */
class PdfController extends Controller{
    /**
     * Print workout pdf
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @Route("/workouts/{id}", name="workout_pdf")
     *
     */
    public function getWorkoutAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        //$authenticatedUser = $this->get('security.context')->getToken()->getUser();


        //if ($authenticatedUser->getUsername() != $slug) {
            //$view->setStatusCode(401);
            //return $view;
        //}

        $user = $em->getRepository('FitFixCoreBundle:User')->find($id);
        $client = $user->getClient();

        $entity = $em->getRepository('FitFixCoreBundle:Workout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Workout entity.');
        }
        $html = $this->renderView('FitFixApiBundle:Pdf:workout.html.twig', array(
          'entity'  => $entity
        ));

        return new Response(
          $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
          200,
          array(
            'Content-Type'          => 'application/pdf',
            'Content-Disposition'   => 'attachment; filename="workout.pdf"'
          )
        );
    }

}
