<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Food;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Food.
 *
 * @NamePrefix("fitfix_api_foodrest_")
 * @author Daniele Longo <daniele.longo.development@gmail.com>
 */
class FoodRestController extends Controller
{

    /**
     * Returns all food available.
     *
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getFoodsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if (!$authenticatedUser->getUsername()) {
            $view->setStatusCode(401);
            return $view;
        }

        $entities = $em->getRepository('FitFixCoreBundle:Food')->findAll();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Food entities.');
        }

        if ($entities) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }


}