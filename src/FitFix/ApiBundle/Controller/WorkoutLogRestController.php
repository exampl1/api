<?php 

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FitFix\CoreBundle\Entity\WorkoutLog;
use FitFix\CoreBundle\Entity\User;
use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FitFix\CoreBundle\Repository\WorkoutLogRepository;
use JMS\Serializer\SerializationContext;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FitFix\CoreBundle\Form\WorkoutLogType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @author gavinwilliams
 * 
 * @NamePrefix("fitfix_api_workoutlogrest_")
 * 
 * Creates a workout log for a client from a PT
 * 
 * /workoutlogs.json - GET - Gets a workout log list for the currently logged in user - DONE
 * /client/{client}/workoutlogs.json - GET/POST - Gets a list of workouts for the specific client, for their PT or posts new data to that client
 */

class WorkoutLogRestController extends FOSRestController {
	
	
	/**
	 * Returns the workout log for the currently logged in user
	 * 
	 * @ApiDoc(
	 * 	section="Workout Log",
	 * 	resource="Workout Log",
	 * 	output="FitFix\CoreBundle\Entity\WorkoutLog"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function getWorkoutlogsAction(){

		$view = $this->view();
		
		$repo = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:WorkoutLog');
		/* @var $repo WorkoutLogRepository */
		
		$user = $this->getUser();
		/* @var $user User */
		
		$client = $user->getClient();
		/* @var $client Client */
		
		if(!$client){
			throw new HttpException(403);
		}
		
		$data = $repo->findByClient($client);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('workoutLog-list')));
		$view->setData($data);
		
		return $this->handleView($view);

	}
	
	/**
	 * Gets a workout log for the currently logged in user
	 * 
	 * @ApiDoc(
	 * 	resource="Workout Log",
	 * 	section="Workout Log",
	 * 	output="FitFix\CoreBundle\Entity\WorkoutLog"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @ParamConverter("workoutlog", class="FitFixCoreBundle:WorkoutLog")
	 * 
	 * @param WorkoutLog $workoutLog
	 * @throws HttpException
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 */
	public function getWorkoutlogAction(WorkoutLog $workoutlog){
		if($workoutlog->getClient()->getId() !== $this->getUser()->getClient()->getId()){
			throw new HttpException(403);
		}
		
		$view = $this->view($workoutlog);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array("workoutLog-details")));
		
		return $this->handleView($view);
	}
	
	/**
	 * Gets a workout log for a PT's client
	 * 
	 * @ApiDoc(
	 * 	resource="Workout Log",
	 * 	section="Workout Log",
	 * 	output="FitFix\CoreBundle\Entity\WorkoutLog"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 * @ParamConverter("workoutlog", class="FitFixCoreBundle:WorkoutLog")
	 * 
	 * @param Client $client
	 * @param WorkoutLog $workoutlog
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function getClientsWorkoutlogAction(Client $client, WorkoutLog $workoutlog){
		
		if($workoutlog->getTrainer()->getId() !== $this->getUser()->getTrainer()->getId()){
			throw new HttpException(403);
		}
		
		$view = $this->view($workoutlog);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array("workoutLog-details")));
		
		return $this->handleView($view);		
		
	}
	
	/**
	 * Posts a workout for a client
	 * 
	 * @ApiDoc(
	 * 	section="Workout Log",
	 * 	resource="Workout Log",
	 * 	output="FitFix\CoreBundle\Entity\WorkoutLog",
	 * 	input="FitFix\CoreBundle\Form\WorkoutLogType"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 * 
	 * @param Client $client
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function postClientWorkoutlogsAction(Client $client){
				
		$workoutLog = new WorkoutLog();
		$workoutLog->setClient($client);
		$workoutLog->setTrainer($this->getUser()->getTrainer());
		
		$view = $this->processForm($workoutLog);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('workoutLog-details')));
		
		return $this->handleView($view);
	}
	
	/**
	 * Gets a workout log for a specific client for the currently logged in PT
	 * 
	 * @ApiDoc(
	 * 	section="Workout Log",
	 * 	resource="Workout Log",
	 * 	output="FitFix\CoreBundle\Entity\WorkoutLog"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 * 
	 * @param Client $client
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function getClientWorkoutlogsAction(Client $client){
		
		$repo = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:WorkoutLog');
		/* @var $repo WorkoutLogRepository */
		
		$user = $this->getUser();
		/* @var $user User */
		
		$trainer = $user->getTrainer();
		/* @var $trainer Trainer */
		
		$data = $repo->findWorkoutLogsForClientFromTrainer($client, $trainer);
		
		$view = $this->view($data);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('workoutLog-list')));
		
		return $this->handleView($view);
				
	}
	
	/**
	 * Processes a workout log object using a form
	 * @param WorkoutLog $workoutLog
	 * @return FOS\RestBundle\View
	 */
	private function processForm(WorkoutLog $workoutLog){
	
		$statusCode = $workoutLog->getId() ? 204 : 201;
		$view = $this->view();
		$data;
		$requestBodyType = $this->getRequest()->getContentType();
		
		$form = $this->createForm(new WorkoutLogType(), $workoutLog);
		
		if($requestBodyType !== null){
			if($requestBodyType == 'json'){
				$data = json_decode($this->getRequest()->getContent(), true);
				if($data === null){
	
					switch (json_last_error()) {
						default:
							return;
						case JSON_ERROR_DEPTH:
							$error = 'Maximum stack depth exceeded';
							break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Underflow or the modes mismatch';
							break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_UTF8:
							$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							break;
					}
	
					$view->setData(array('error' => $error));
					$view->setStatusCode(400);
					return $view;
	
				}
			}
		} else {
			$data = $this->getRequest();
		}
	
		// Remove the ID, it'll break EVERYTHING!
		unset($data['id']);
		
		$form->submit($data, false);
		
		if($form->isValid()){
	
			$em = $this->getDoctrine()->getManager();
	
			$em->persist($workoutLog);
			$em->flush();
			$view->setStatusCode($statusCode);
			if (201 == $statusCode) {
				/*
				$view->setHeader('Location', $this->generateUrl('fitfix_api_invoicerest_get_invoice', array(
						'invoice' => $invoice->getId(),
						'_format' => $this->getRequest()->get('_format', 'json')
				), true));
				*/
			}
	
			$view->setData($workoutLog);
	
			return $view;
	
		}
	
		return $view->setData($form, 400);
	
	}
	
}