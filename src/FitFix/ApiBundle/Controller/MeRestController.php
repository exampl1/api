<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\User;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;                  // @ApiDoc(resource=true, description="Filter",filters={{"name"="a-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}})
use FOS\RestBundle\Controller\Annotations\NamePrefix;       // NamePrefix Route annotation class @NamePrefix("bdk_core_user_userrest_")
use FOS\RestBundle\View\RouteRedirectView;                  // Route based redirect implementation
use FOS\RestBundle\View\View AS FOSView;                    // Default View implementation.
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use JMS\SecurityExtraBundle\Annotation\Secure;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use JMS\Serializer\SerializationContext;

/**
 * Controller that provides Restful services over the resource Users.
 *
 * @NamePrefix("fitfix_api_merest_")
 * @author Gavin Williams <gavin.williams@fishrod.co.uk>
 */
class MeRestController extends Controller {

    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_USER")
     * @ApiDoc()
     */
    public function optionsMeAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
        return $view;
    }

    /**
     * Return high level information about the currently authenticated user
     *
     * @return FOSView
     * @Secure(roles="ROLE_USER")
     * @ApiDoc()
     */
    public function getMeAction(){
        $authenticatedUser = $this->container->get('security.context')->getToken()->getUser();

		$view = FOSView::create();
        $groups = array("details", "event-details", "invoice-list");
		
        if(!$authenticatedUser instanceof User){
            throw new AccessDeniedException('User must be authenticated before making a request to me');
        }

        $roles = $authenticatedUser->getRoles();
        if (in_array('ROLE_CLIENT', $roles)) {
            $data = $authenticatedUser->getClient();
        }
        else if (in_array('ROLE_TRAINER', $roles)) {
            $data = $authenticatedUser->getTrainer();
        }
        
		$view->setSerializationContext(SerializationContext::create()->setGroups($groups));
        $view->setStatusCode(200)->setData($data);

        return $view;
    }

}
