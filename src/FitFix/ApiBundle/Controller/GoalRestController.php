<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Goal;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Goal.
 *
 * @NamePrefix("fitfix_api_goalrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class GoalRestController extends Controller
{
    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function optionsGoalsAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
        return $view;
    }

    /**
     * Returns all goals by client.
     *
     * @param string $slug slug
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getGoalsAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }
        $client = $authenticatedUser->getClient();

        $entities = $em->getRepository('FitFixCoreBundle:Goal')->findByClient($client);

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Goal entities.');
        }

        if ($entities) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Returns an goal by id.
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getGoalAction($slug, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $client = $authenticatedUser->getClient();

        $entity = $em->getRepository('FitFixCoreBundle:Goal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Goal entity.');
        }

        if ($entity) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("details")));
            $view->setStatusCode(200)->setData($entity);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Creates a new Goal entity.
     * Using param_fetcher_listener: force
     *
     * @param string $slug slug
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="description", requirements="\d+", default="", description="Description")
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function postGoalsAction($slug, ParamFetcher $paramFetcher)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $client = $authenticatedUser->getClient();

        $request = $this->getRequest();
        $userManager = $this->container->get('fos_user.user_manager');

        $goal = new Goal();
        $goal->setDescription($request->get('description'));
        $goal->setClient($client);

        $validator = $this->get('validator');
        $errors = $validator->validate($goal);

        if (count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($goal);
            $em->flush();
            $params = array(
                "slug" => $slug,
                "id" => $goal->getId()
            );
            $view = RouteRedirectView::create("fitfix_api_goalrest_get_client_goal", $params);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Update an goal by id.
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @RequestParam(name="description", requirements="\d+", default="", description="Description")
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function putGoalAction($slug, $id)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FitFixCoreBundle:Goal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Goal entity.');
        }

        $request = $this->getRequest();

        if ($request->get('description')) {
            $entity->setDescription($request->get('description'));
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if (count($errors) == 0) {
            $em->persist($entity);
            $em->flush();
            $view = FOSView::create();
            $view->setStatusCode(204);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Delete an goal by ID
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function deleteGoalAction($slug, $id)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();
        
        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }
        
        $em = $this->getDoctrine()->getManager();
        $goal = $em->getRepository('FitFixCoreBundle:Goal')->find($id);
        if ($goal) {
            $em->remove($goal);
            $em->flush();
            $view->setStatusCode(204)->setData("Goal removed.");
        } else {
            $view->setStatusCode(204)->setData("No data available.");
        }
        return $view;
    }

    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return FOSView
     */
    private function get_errors_view($errors)
    {
        $msgs = array();
        $it = $errors->getIterator();
        //$val = new \Symfony\Component\Validator\ConstraintViolation();
        foreach ($it as $val) {
            $msg = $val->getMessage();
            $params = $val->getMessageParameters();
            //using FOSUserBundle translator domain 'validators'
            $msgs[$val->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = FOSView::create($msgs);
        $view->setStatusCode(400);
        return $view;
    }

}