<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Invoice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use FitFix\CoreBundle\Form\InvoiceType;


/**
 * Controller that provides Restful services over the resource Invoice.
 *
 * @NamePrefix("fitfix_api_invoicerest_")
 * @author gavinwilliams
 */
class InvoiceRestController extends FOSRestController
{

	/**
	 * Gets the invoices for a specific user
	 * 
	 * @ApiDoc(
	 * 	section="Accounts",
	 * 	output="FitFix\CoreBundle\Entity\Invoice"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER, ROLE_CIENT")
	 */
	public function getInvoicesAction(){
		
		$view = $this->view();
		
		$om = $this->getDoctrine()->getManager();
		
		$data = $om->getRepository('FitFixCoreBundle:Invoice')->findAllByUser($this->getUser());
		
		$context = SerializationContext::create()->setGroups(array('invoice-list'));
		
		$view->setSerializationContext($context);
		
		$view->setData($data);
		
		return $view;
		
	}
	
	/**
	 * Gets a specific invoice
	 * @param Invoice $invoice
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 *
	 * @ApiDoc(
	 * 	section="Accounts",
	 * 	output="FitFix\CoreBundle\Entity\Invoice"
	 * )
	 *
	 * @ParamConverter("invoice", class="FitFixCoreBundle:Invoice")
	 * @Secure(roles="ROLE_TRAINER, ROLE_CIENT")
	 */
	public function getInvoiceAction(Invoice $invoice){
	
		// Check to see whether the trainer is who they say they are
		
		if($this->getUser()->getTrainer()){
			if($invoice->getFrom()->getId() !== $this->getUser()->getTrainer()->getId()){
				throw new UnauthorizedHttpException('WSSE');
			}
		} else {
			if($invoice->getTo()->getId() !== $this->getUser()->getClient()->getId()){
				throw new UnauthorizedHttpException('WSSE');
			}
		}
	
		$view = $this->view();
	
		$context = SerializationContext::create()->setGroups(array('invoice-details'));
		$view->setSerializationContext($context);
	
		$view->setData($invoice);
	
		return $view;
	}

	/**
	 * Updates an invoice
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 *
	 * @ApiDoc(
	 * 	section="Accounts",
	 * 	input="FitFix\CoreBundle\Form\InvoiceType"
	 * )
	 *
	 * @ParamConverter("invoice", class="FitFixCoreBundle:Invoice")
	 * @Secure(roles="ROLE_TRAINER, ROLE_CIENT")
	 */
	public function putInvoiceAction(Invoice $invoice){
	
		$view = $this->processForm($invoice);
	
		return $view;
	}
	
	/**
	 * Posts a new invoice
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 *
	 * @ApiDoc(
	 * 	section="Accounts",
	 * 	input="FitFix\CoreBundle\Form\InvoiceType"
	 * )
	 *
	 * @Secure(roles="ROLE_TRAINER")
	 */
	public function postInvoicesAction(){
	
		$view = $this->processForm(new Invoice());
	
		return $view;
	}
	
	/**
	 * Processes an invoice object using a form
	 * @param Invoice $invoice
	 * @return FOS\RestBundle\View
	 */
	private function processForm(Invoice $invoice){
	
		$statusCode = $invoice->getId() ? 204 : 201;
		$view = $this->view();
		$data;
		$requestBodyType = $this->getRequest()->getContentType();
	
		$currentuser = $this->getUser();
	
		$form = $this->createForm(new InvoiceType(), $invoice);
	
		if($requestBodyType !== null){
			if($requestBodyType == 'json'){
				$data = json_decode($this->getRequest()->getContent(), true);
				if($data === null){
	
					switch (json_last_error()) {
						default:
							return;
						case JSON_ERROR_DEPTH:
							$error = 'Maximum stack depth exceeded';
							break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Underflow or the modes mismatch';
							break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_UTF8:
							$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							break;
					}
	
					$view->setData(array('error' => $error));
					$view->setStatusCode(400);
					return $view;
	
				}
			}
		} else {
			$data = $this->getRequest();
		}
	
		// If this is a client, it should always go to their PT
		if($currentuser->hasRole('ROLE_CLIENT')){
			$data['from'] = $currentuser->getClient()->getTrainer()->getUser()->getId();
		}
	
		$form->submit($data);
	
		if($form->isValid()){
	
			$em = $this->getDoctrine()->getManager();
	
			$em->persist($invoice);
			$em->flush();
			$view->setStatusCode($statusCode);
			if (201 == $statusCode) {
	
				$view->setHeader('Location', $this->generateUrl('fitfix_api_invoicerest_get_invoice', array(
						'invoice' => $invoice->getId(),
						'_format' => $this->getRequest()->get('_format', 'json')
				), true));
			}
	
			$view->setData($invoice);
	
			return $view;
	
		}
	
		return $view->setData($form, 400);
	
	}
	
}
