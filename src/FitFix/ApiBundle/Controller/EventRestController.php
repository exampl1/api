<?php

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FitFix\CoreBundle\Entity\Client;
use FitFix\CoreBundle\Entity\Trainer;
use FitFix\CoreBundle\Entity\Event;
use FitFix\CoreBundle\Entity\Session;
use FitFix\CoreBundle\Form\EventType;
use FitFix\CoreBundle\Form\SessionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\SecurityContext;
use FitFix\CoreBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use FitFix\CoreBundle\Entity\MappedSuperclass\CalendarItemMappedSuperclass;

/**
 * 
 * @author gavinwilliams
 * @NamePrefix("fitfix_api_calendarrest_")
 */
class EventRestController extends FOSRestController {
	
	/**
	 * Gets events for a specific trainer
	 * @param Trainer $trainer
	 * 
	 * @ApiDoc(
	 * 	section="Calendar",
	 *	output="FitFix\CoreBundle\Entity\Event",
	 *	filters={
	 *		{"name"="from", "dataType" = "datetime"},
	 *		{"name"="to", "dataType" = "datetime"}
	 *	}
	 * )
	 * 
	 */
	public function getEventsAction(){
		
		$view = $this->view();
		$data = array();
		
		$om = $this->getDoctrine()->getManager();
		
		$data = $om->getRepository('FitFixCoreBundle:Event')->findAllByUser($this->getUser());
		
		// There must be a cleaner way of doing this...
		foreach($data as $key => $value){
			if($value instanceof Session){
				
			}
		}
		
		$context = SerializationContext::create()->setGroups(array('event-list'));
		
		$view->setSerializationContext($context);
		
		$view->setData($data);
		
		return $view;
	}
	
	/**
	 * Gets events for specific user related to the
	 * currently logged in trainer
	 * 
	 * @param User $user
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 * 
	 * @ParamConverter("user", class="FitFixCoreBundle:User") 
	 */
	public function getUserSessionsAction(User $user){
		
    if(!($this->getUser()->getTrainer() && $user->getClient()) && 
      !($this->getUser()->getTrainer()->getId() == $user->getClient()->getTrainer()->getId())){
			throw new UnauthorizedHttpException('WSSE');
		}
		
		$em = $this->getDoctrine()->getManager();
		
		$repo = $em->getRepository('FitFixCoreBundle:Session');
		
		$events = $repo->findAllForClientWithTrainer($user, $this->getUser());
		
		$view = $this->view($events);
		
		$context = SerializationContext::create()->setGroups(array('event-list'));
		
		$view->setSerializationContext($context);
		
		return $view;
	}
	
	/**
	 * Gets the trainers specific event
	 * @param Event $event
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 * 
	 * @ApiDoc(
	 * 	section="Calendar",
	 * 	output="FitFix\CoreBundle\Entity\Event"
	 * )
	 * 
	 * @ParamConverter("event", class="FitFixCoreBundle:Event")
	 */
	public function getEventAction(Event $event){
		
		// Check to see whether the trainer is who they say they are
		if($event->getFrom()->getId() != $this->getUser()->getId() && $event->getTo()->getId() != $this->getUser()->getId()){
			throw new UnauthorizedHttpException('WSSE');
		}
		
		$view = $this->view();
		
		$context = SerializationContext::create()->setGroups(array('event-details', "address-list"));
		$view->setSerializationContext($context);
		
		$view->setData($event);
		
		return $view;
	}
	
	/**
	 * Posts a new event
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 * 
	 * @ApiDoc(
	 * 	section="Calendar",
	 * 	input="FitFix\CoreBundle\Form\EventType"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 */
	public function postEventsAction(){

		$view = $this->processForm(new Event());
		
		if($view->getStatusCode() != 400){
			$context = SerializationContext::create()->setGroups(array('event-details', "address-list"));
			$view->setSerializationContext($context);
		}
		
		return $view;
	}
	
	/**
	 * Updates an event
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 *
	 * @ApiDoc(
	 * 	section="Calendar",
	 * 	input="FitFix\CoreBundle\Form\EventType"
	 * )
	 *  
	 * @ParamConverter("event", class="FitFixCoreBundle:Event")
	 */
	public function putEventAction(Event $event){
	
		$view = $this->processForm($event);
	
		return $view;
	}
	
	/**
	 * Gets the trainers specific session
	 * @param Trainer $trainer
	 * @param Session $session
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 *
	 * @ApiDoc(
	 * 	section="Calendar"
	 * )
	 * 
	 * @ParamConverter("session", class="FitFixCoreBundle:Session")
	 */
	public function getSessionAction(Session $session){
		
		$view = $this->view();
		
		$context = SerializationContext::create()->setGroups(array('event-details', "address-list"));
		$view->setSerializationContext($context);
		
		$view->setData($session);
		
		return $view;
	}
	
	/**
	 * Gets sessions for a specific trainer
	 * @param Trainer $trainer
	 * 
	 * @ApiDoc(
	 * 	section="Calendar"
	 * )
	 * 
	 */
	public function getSessionsAction(){
		
		$view = $this->view();
		$data = array();
		
		$om = $this->getDoctrine()->getManager();
		
		$data = $om->getRepository('FitFixCoreBundle:Session')->findAllByUser($this->getUser());
		
		$context = SerializationContext::create()->setGroups(array('event-list'));
		
		$view->setSerializationContext($context);
		
		$view->setData($data);
		
		return $view;
	}
	
	/**
	 * Posts a new session for a trainer
	 * @param Trainer $trainer
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 * 
	 * @ApiDoc(
	 * 	section="Calendar",
	 * 	input="FitFix\CoreBundle\Form\SessionType"
	 * )
	 * 
	 */
	public function postSessionsAction(){
				
		$view = $this->processForm(new Session());
		
		if($view->getStatusCode() != 400){
			$context = SerializationContext::create()->setGroups(array('event-details', "address-list"));
			$view->setSerializationContext($context);
		}
		
		return $view;
	}
	
	/**
	 * Updates a session
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 *
	 * @ApiDoc(
	 * 	section="Calendar",
	 * 	input="FitFix\CoreBundle\Form\SessionType"
	 * )
	 *
	 * @ParamConverter("session", class="FitFixCoreBundle:Session")
	 */
	public function putSessionAction(Session $session){
	
		$view = $this->processForm($session);
		if($view->getStatusCode() !== 400){
			$context = SerializationContext::create()->setGroups(array('event-details', "address-list"));
			$view->setSerializationContext($context);
		}
		return $view;
	}
	
	/**
	 * Processes an event object using a form
	 * @param Event $event
	 * @return FOS\RestBundle\View
	 */
	private function processForm(CalendarItemMappedSuperclass $event){
		
		$statusCode = $event->getId() ? 204 : 201;
		$view = $this->view();
		$type;
		$routearg;
		$data;
		$requestBodyType = $this->getRequest()->getContentType();
		
		$currentuser = $this->getUser();
		
		if($event instanceof Session){
			$type = new SessionType();
			$routearg = 'session';
		} else if($event instanceof Event) {
			$type = new EventType();
			$routearg = 'event';
		}
		
		$form = $this->createForm($type, $event);
		
		if($requestBodyType !== null){
			if($requestBodyType == 'json'){
				$data = json_decode($this->getRequest()->getContent(), true);
				if($data === null){
					
					switch (json_last_error()) {
						default:
							return;
						case JSON_ERROR_DEPTH:
							$error = 'Maximum stack depth exceeded';
							break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Underflow or the modes mismatch';
							break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_UTF8:
							$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							break;
					}
					
					$view->setData(array('error' => $error));
					$view->setStatusCode(400);
					return $view;
					
				}
			}
		} else {
			$data = $this->getRequest();
		}
		
		if(!isset($data['id'])){
			// If this is a client, it should always go to their PT
			if($currentuser->hasRole('ROLE_CLIENT')){
				$data['to'] = $currentuser->getClient()->getTrainer()->getUser()->getId();
			}
			
			// Always set from to the current user if creating new event/session
			if(!$event->getId()){
				$data['from'] = $currentuser->getId();
			}
		} else {
			unset($data['id']);
		}
		
		$form->submit($data);
		
		if($form->isValid()){
			
			$em = $this->getDoctrine()->getManager();
			
			$em->persist($event);
			$em->flush();
            $view->setStatusCode($statusCode);
            if (201 == $statusCode) {
                
                $view->setHeader('Location', $this->generateUrl('fitfix_api_calendarrest_get_' . $routearg, array(
                    $routearg => $event->getId(),
                    '_format' => $this->getRequest()->get('_format', 'json')
                ), true));
            }
            
            $view->setData($event);
			
			return $view;
			
		}
		
		return $view->setData($form)->setStatusCode(400);
		
	}
	
}
