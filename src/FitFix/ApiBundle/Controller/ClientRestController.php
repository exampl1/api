<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Client;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\HttpFoundation\Response;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use Symfony\Bridge\Monolog\Logger;
use FOS\UserBundle\Model\UserManager;
use FitFix\CoreBundle\Generator\Password;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FitFix\CoreBundle\Entity\Thread;
use FitFix\CoreBundle\Entity\Trainer;
use FitFix\CoreBundle\Entity\Lifestyle;
use FitFix\CoreBundle\Entity\Nutrition;
use FitFix\ApiBundle\PayloadHelper\ClientPayloadHelper;
use FitFix\ApiBundle\PayloadHelper\LifestylePayloadHelper;
use FitFix\ApiBundle\PayloadHelper\NutritionPayloadHelper;
use FitFix\CoreBundle\Entity\Objectives;
use FitFix\ApiBundle\PayloadHelper\ObjectivesPayloadHelper;
use FitFix\CoreBundle\Entity\PARQ;
use FitFix\ApiBundle\PayloadHelper\PARQPayloadHelper;
use Gedmo\ReferenceIntegrity\Mapping\Validator;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Controller that provides Restful services over the resource Client.
 *
 * @NamePrefix("fitfix_api_clientrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class ClientRestController extends Controller
{

    /**
     * Returns all clients.
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getClientsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $entities = $em->getRepository('FitFixCoreBundle:Client')->findByUserSortByName($authenticatedUser);

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Client entities.');
        }

        $view = FOSView::create();

        if ($entities) {
    	    $serializationContext = SerializationContext::create()->setGroups(array('list', 'details'));
            $view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Returns a client by id.
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getClientAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $nextSession = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:Session')->findNextSessionForUser($entity->getUser());

        if($nextSession){
        	$entity->setNextSession($nextSession);
        }

        $view = FOSView::create();

        if ($entity) {
        	$serializationContext = SerializationContext::create()->setGroups(array("details", "event-details", "workout-details"));
			$view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entity);
            $view->setHeader('Access-Control-Allow-Origin', '*');
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }


    /**
     * Handles posting photos to the API
     *
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function optionsClientsPhotoAction(){
    	$view = FOSView::create();
    	$view->setStatusCode(200);
    	$view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST');
    	return $view;
    }

    /**
     * Handles posting photos to the API
     *
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function postClientsPhotoAction(){

    	$files = $this->getRequest()->files;

    	$logger = $this->get('logger');

    	$logger->info('Got Files');

    }

    private function __rollback(Client $client, \FOS\UserBundle\Doctrine\UserManager $usermanager){
    	/**
    	 * TODO: This needs to be fixed, throws constraint error
    	 */
    	// $usermanager->deleteUser($client->getUser());
    }

    /**
     * Creates a new Client entity.
     * Using param_fetcher_listener: force
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function postClientsAction()
    {
    	// Declare everything
    	$client = new Client();
    	$user;
    	$view = FOSView::create();
    	$logger = $this->get('logger');
    	$em = $this->getDoctrine()->getManager();
    	$validator = $this->get('validator');

    	$data = json_decode($this->getRequest()->getContent());

    	// Can't do anything without an email!
    	if(!isset($data->details->details_email)){
    		$view->setStatusCode(409);
    		$view->setData(array('errors' => array('An email address is required to register a client')));
    		return $view;
    	}

    	$usermanager = $this->get('fos_user.user_manager');

    	$user = $usermanager->createUser();
    	$user->setEmail($data->details->details_email);
    	$user->setUsername($user->getEmail());
    	$user->setRoles(array('ROLE_CLIENT'));
    	$user->setEnabled(true);

    	$user->setPlainPassword(Password::entropicPassword());

    	$logger->debug('Creating user');


    	// First persist the user
    	try {
    		$usermanager->updateUser($user);
    	} catch(\Exception $e){

    		if($e->getPrevious()->getCode() == 23000){
    			// A unique constraint probably failed
    			$view->setStatusCode(400);
    			$view->setData(array('errors' => array('A user already exists with the email address, please use another one')));
    			return $view;
    		} else {
    			$view->setStatusCode(500);
    			$logger->crit($e);
    			return $view;
    		}
    	}

    	// User created so let's populate the client

    	$client->setUser($user);
    	ClientPayloadHelper::populateEntityFromPayload($client, $data->details, 'details_');

    	$logger->debug('Creating client');

    	// Let's create the lifestyle object, populate and validate it
    	$lifestyle = new Lifestyle();
    	LifestylePayloadHelper::populateEntityFromPayload($lifestyle, $data->lifestyle, 'lifestyle_');

    	$logger->debug('Creating lifestyle');

    	$client->setLifestyle($lifestyle);

    	// Let's create nutrition object, populate and validate it
    	$nutrition = new Nutrition();
    	NutritionPayloadHelper::populateEntityFromPayload($nutrition, $data->nutrition, 'nutrition_');

    	$logger->debug('Creating nutrition');

    	$client->setNutrition($nutrition);

    	// Set objectives object
    	$objectives = new Objectives();
    	ObjectivesPayloadHelper::populateEntityFromPayload($objectives, $data->objectives, 'objective_');

    	$logger->debug('Creating objectives');

    	$client->setObjectives($objectives);

    	$parq = new PARQ();
    	PARQPayloadHelper::populateEntityFromPayload($parq, $data->parq, 'parq_');

    	$logger->debug('Creating PARQ');

    	$client->setPARQ($parq);

    	$trainerUser = $this->get('security.context')->getToken()->getUser();

    	$trainer = $em->getRepository('FitFixCoreBundle:Trainer')->findOneBy(array('user' => $trainerUser));

    	if(!$trainer instanceof Trainer){
    		$this->__rollback($client, $usermanager);
    		$view->setStatusCode(500);
    		$logger->crit('Unable to fetch trainer for client registration, does not appear to be logged in', $this);
    		return $view;
    	}
    	
    	if(isset($data->photo) && strstr($data->photo, 'data:image')){
    		$photo = $storage->uploadUniqueFileWithData('fitfixuserdata', 'profilepictures', $data->photo, 'jpg', 'image/jpg');
    		$client->setPhoto($photo);
    	}

    	$client->setTrainer($trainer);

    	if(count($errors = $validator->validate($client))){
    		$this->__rollback($client, $usermanager);
    		$view->setStatusCode(400);
    		$view->setData($errors);
    		return $view;

    	}

    	$em->persist($client);

    	$em->flush();

    	$user->setClient($client);

    	$usermanager->updateUser($user);

    	$view->setData($client);
    	
    	$view->setStatusCode(201);
    	$view->setHeader('Location', $this->generateUrl('fitfix_api_clientrest_get_client', array(
    				'_format' => $this->getRequest()->get('_format'),
    				'id' => $client->getId()
    	), true));

        return $view;
    }

    /**
     * Update an client id.
     *
     * @param string $id ID
     *
     * @RequestParam(name="username", requirements="\d+", default="", description="Username.")
     * @RequestParam(name="email", requirements="\d+", default="", description="Email.")
     * @RequestParam(name="plainPassword", requirements="\d+", default="", description="Plain Password.")
     * @RequestParam(name="gender", requirements="\d+", default="", description="Gender")
     * @RequestParam(name="dob", requirements="\d+", default="", description="Date of birth")
     * @RequestParam(name="mobile", requirements="\d+", default="", description="Mobile")
     * @RequestParam(name="firstName", requirements="\d+", default="", description="First Name")
     * @RequestParam(name="lastName", requirements="\d+", default="", description="Last Name")
     * @RequestParam(name="trainerId", requirements="\d+", default="", description="Trainer ID")
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function putClientAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('FitFixCoreBundle:Client')->find($id);

        $storage = $this->get('fitfix_core.storage.s3');
         
        ($storage instanceof \FitFix\CoreBundle\Service\Storage);
        
        if (!$client) {
            $view = FOSView::create();
            $view->setStatusCode(404);
            return $view;
        }

        if ($request->get('username')) {
            $client->getUser()->setUsername($request->get('username'));
        }
        if ($request->get('email')) {
            $client->getUser()->setEmail($request->get('email'));
        }
        if ($request->get('plainPassword')) {
            $client->getUser()->setPlainPassword($request->get('plainPassword'));
        }

        if ($request->get('trainerId')) {
            $em = $this->getDoctrine()->getManager();
            $trainer = $em->getRepository('FitFixCoreBundle:Trainer')->find($request->get('trainerId'));

            if ($trainer) {
                $client->setTrainer($trainer);
            }
        }

        if ($request->get('gender')) {
            $client->setGender($request->get('gender'));
        }
        // php bug with exception thrown in DateTime constructor not caught means strtotime needed to check string is valid
        if (strtotime($request->get('dob'))) {
            try {
                $client->setDob(new \DateTime($request->get('dob')));
            } catch (Exception $e) {

            }
        }
        if ($request->get('mobile')) {
            $client->setMobile($request->get('mobile'));
        }
        if ($request->get('firstName')) {
            $client->setFirstName($request->get('firstName'));
        }
        if ($request->get('lastName')) {
            $client->setLastName($request->get('lastName'));
        }
        
        $client->setArchived($request->get('archived'));
        
        $client->setGoals($request->get('goals'));
        
        $client->setNotes($request->get('notes'));
        
        $client->setSpecificAim($request->get('specificAim'));
        
        $client->setLandline($request->get('landline'));
        
        $client->setAddress($request->get('address'));
        
        if($request->get('photo') && strstr($request->get('photo'), 'data:image')){
        	$photo = $storage->uploadUniqueFileWithData('fitfixuserdata', 'profilepictures', $request->get('photo'), 'jpg', 'image/jpg');
        	$client->setPhoto($photo);
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($client, array('Profile'));
        if (count($errors) == 0) {
            $em->persist($client);
            $em->flush();
            $view = FOSView::create();
            $view->setStatusCode(204);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Delete an client by ID
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function deleteClientAction($id)
    {
        $view = FOSView::create();
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('FitFixCoreBundle:Client')->find($id);
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        if ($client) {

            if ($client->getTrainer() === $authenticatedUser->getTrainer()) {
                $client->setTrainer(null);
                $em->persist($client);
                $em->flush();
                $view->setStatusCode(204)->setData("Client removed.");
            }
            else {
                $view->setStatusCode(401);
            }

        } else {
            $view->setStatusCode(204)->setData("No data available.");
        }
        return $view;
    }

    /**
     * Deactive an client by ID
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function putClientDeactiveAction($id)
    {
        $view = FOSView::create();
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('FitFixCoreBundle:Client')->find($id);
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        if ($client) {
          if ($client->getTrainer() === $authenticatedUser->getTrainer()) {
                $client->setInactive(true);
                $em->persist($client);
                $em->flush();
                $view->setStatusCode(204)->setData("Client archived.");
            }
            else {
                $view->setStatusCode(401);
            }

        } else {
            $view->setStatusCode(204)->setData("No data available.");
        }
    }

    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return FOSView
     */
    private function get_errors_view($errors)
    {
        $msgs = array();
        $it = $errors->getIterator();
        //$val = new \Symfony\Component\Validator\ConstraintViolation();
        foreach ($it as $val) {
            $msg = $val->getMessage();
            $params = $val->getMessageParameters();
            //using FOSUserBundle translator domain 'validators'
            $msgs[$val->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = FOSView::create($msgs);
        $view->setStatusCode(400);
        return $view;
    }

}
