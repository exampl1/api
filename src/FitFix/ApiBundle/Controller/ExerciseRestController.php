<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Goal;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

/**
 * Controller that provides Restful services over the resource Exercise.
 *
 * @NamePrefix("fitfix_api_exerciserest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class ExerciseRestController extends Controller
{
    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function optionsExercisesAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
        return $view;
    }

    /**
     * Returns all exercises.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getExercisesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $view = FOSView::create();

        $request = $this->getRequest();

        $offset = null;
        
        if (!$limit = $request->get('limit')) {
            $limit = null;
        }
        if (!$page = $request->get('page')) {
            $page = 0;
        }

        if($limit > 0 && $page > 0){
        	$offset = $limit * $page;
        }
        //
        if ($keywords = $request->get('keywords')) {
            $entities = $em->getRepository('FitFixCoreBundle:Exercise')->findByKeywords($keywords, $limit, $offset);
        
        } else if($equipment = $request->get('equipment')) {
            $entities = $em->getRepository('FitFixCoreBundle:Exercise')->findByEquipment($equipment, $limit, $offset);

        } else if($muscle = $request->get('muscle')) {

            $entities = $em->getRepository('FitFixCoreBundle:Exercise')->findByMuscle($muscle, $limit, $offset);

        } else {
            $entities = $em->getRepository('FitFixCoreBundle:Exercise')->findBy(
                array(),
                array(
                    'name' => 'ASC'
                ),
                $limit,
            	$offset
            );
        }

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Exercise entities.');
        }

        if ($entities) {
            $serializationContext = SerializationContext::create()->setGroups(array("list"));
            $view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Returns an exercise by id.
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getExerciseAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $view = FOSView::create();

        $entity = $em->getRepository('FitFixCoreBundle:Exercise')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Exercise entity.');
        }

        if ($entity) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("details")));
            $view->setStatusCode(200)->setData($entity);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

}