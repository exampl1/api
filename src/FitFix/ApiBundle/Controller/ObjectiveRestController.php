<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Objective;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Objective.
 *
 * @NamePrefix("fitfix_api_objectiverest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class ObjectiveRestController extends Controller
{

    /**
     * Returns all objectives by client.
     *
     * @param string $slug slug
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getObjectivesAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }
        $client = $authenticatedUser->getClient();

        $entities = $em->getRepository('FitFixCoreBundle:Objective')->findByClient($client);

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Objective entities.');
        }

        if ($entities) {
        	$view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Returns an objective by id.
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getObjectiveAction($slug, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $client = $authenticatedUser->getClient();

        $entity = $em->getRepository('FitFixCoreBundle:Objective')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Objective entity.');
        }

        if ($entity) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("details")));
            $view->setStatusCode(200)->setData($entity);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Creates a new Objective entity.
     * Using param_fetcher_listener: force
     *
     * @param string $slug slug
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="description", requirements="\d+", default="", description="Description")
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function postObjectivesAction($slug, ParamFetcher $paramFetcher)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $client = $authenticatedUser->getClient();

        $request = $this->getRequest();
        $userManager = $this->container->get('fos_user.user_manager');

        $objective = new Objective();
        $objective->setDescription($request->get('description'));
        $objective->setClient($client);

        $validator = $this->get('validator');
        $errors = $validator->validate($objective);

        if (count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($objective);
            $em->flush();
            $params = array(
                "slug" => $slug,
                "id" => $objective->getId()
            );
            $view = RouteRedirectView::create("fitfix_api_objectiverest_get_client_objective", $params);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Update an objective by id.
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @RequestParam(name="description", requirements="\d+", default="", description="Description")
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function putObjectiveAction($slug, $id)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FitFixCoreBundle:Objective')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Objective entity.');
        }

        $request = $this->getRequest();

        if ($request->get('description')) {
            $entity->setDescription($request->get('description'));
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if (count($errors) == 0) {
            $em->persist($entity);
            $em->flush();
            $view = FOSView::create();
            $view->setStatusCode(204);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Delete an objective by ID
     *
     * @param string $slug slug
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT")
     * @ApiDoc()
     */
    public function deleteObjectiveAction($slug, $id)
    {
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        if ($authenticatedUser->getUsername() != $slug) {
            $view->setStatusCode(401);
            return $view;
        }

        $em = $this->getDoctrine()->getManager();
        $objective = $em->getRepository('FitFixCoreBundle:Objective')->find($id);
        if ($objective) {
            $em->remove($objective);
            $em->flush();
            $view->setStatusCode(204)->setData("Objective removed.");
        } else {
            $view->setStatusCode(204)->setData("No data available.");
        }
        return $view;
    }

    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return FOSView
     */
    private function get_errors_view($errors)
    {
        $msgs = array();
        $it = $errors->getIterator();
        //$val = new \Symfony\Component\Validator\ConstraintViolation();
        foreach ($it as $val) {
            $msg = $val->getMessage();
            $params = $val->getMessageParameters();
            //using FOSUserBundle translator domain 'validators'
            $msgs[$val->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = FOSView::create($msgs);
        $view->setStatusCode(400);
        return $view;
    }

}