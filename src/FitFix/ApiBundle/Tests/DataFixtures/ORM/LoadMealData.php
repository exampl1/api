<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Meal;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadMealData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*
        // meal-1
        $meal = new Meal();
        $meal->setClient($this->getReference('client-1'));
        $meal->setDescription('Energy Bar');

        $manager->persist($meal);

        // meal-2
        $meal = new Meal();
        $meal->setClient($this->getReference('client-2'));
        $meal->setDescription('Pasta');

        $manager->persist($meal);

        $manager->flush();
        */
    }

    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }

}
