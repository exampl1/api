<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Address;
use FitFix\CoreBundle\Entity\SessionType;
use FitFix\CoreBundle\Entity\SessionTypeLocation;
use FitFix\CoreBundle\Entity\SessionTypeLocationDate;
use FitFix\CoreBundle\Entity\SessionTypeLocationDateTime;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadSessionTypeData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*
        // sessionType-1
        $sessionType = new SessionType();
        $sessionType->setTrainer($this->getReference('trainer-1'));
        $sessionType->setDescription('Running');
        $sessionType->setPrice(5);

        $manager->persist($sessionType);

        $this->addReference('sessiontype-1', $sessionType);

        // sessionType-2
        $sessionType = new SessionType();
        $sessionType->setTrainer($this->getReference('trainer-1'));
        $sessionType->setDescription('Tennis');
        $sessionType->setPrice(15);

        $manager->persist($sessionType);

        $this->addReference('sessiontype-2', $sessionType);

        $sessionType = new SessionType();
        $sessionType->setTrainer($this->getReference('trainer-1'));
        $sessionType->setDescription('Football');
        $sessionType->setPrice(15);

        $manager->persist($sessionType);

        $this->addReference('sessiontype-3', $sessionType);

        $manager->flush();
        */
    }

    public function getOrder()
    {
        return 10; // the order in which fixtures will be loaded
    }

}
