<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Session;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadSessionData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*
        // session-1
        $session = new Session();
        $session->setClient($this->getReference('client-1'));
        $session->setTrainer($this->getReference('trainer-1'));
        $session->setAddress($this->getReference('address-1'));
        $session->setStartTime(new DateTime('2013-08-10 10:00:00'));
        $session->setEndTime(new DateTime('2013-08-10 11:00:00'));
        $session->setRequestStatus('pending');
        //$session->setSessionType($this->getReference('sessiontype-1'));

        $manager->persist($session);
        $this->addReference('session-1', $session);

        // session-2
        $session = new Session();
        $session->setClient($this->getReference('client-2'));
        $session->setTrainer($this->getReference('trainer-1'));
        $session->setAddress($this->getReference('address-2'));
        $session->setStartTime(new DateTime('2013-07-10 13:30:00'));
        $session->setEndTime(new DateTime('2013-07-10 14:30:00'));
        $session->setRequestStatus('active');
        //$session->setSessionType($this->getReference('sessiontype-2'));

        $manager->persist($session);
        $this->addReference('session-2', $session);

        // session-3
        $session = new Session();
        $session->setClient($this->getReference('client-2'));
        $session->setTrainer($this->getReference('trainer-1'));
        $session->setAddress($this->getReference('address-3'));
        $session->setStartTime(new DateTime('2013-07-11 13:30:00'));
        $session->setEndTime(new DateTime('2013-07-11 14:30:00'));
        $session->setRequestStatus('active');
        //$session->setSessionType($this->getReference('sessiontype-3'));

        $manager->persist($session);
        $this->addReference('session-3', $session);

        $manager->flush();
        */
    }

    public function getOrder()
    {
        return 13; // the order in which fixtures will be loaded
    }

}
