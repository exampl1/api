<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Review;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadReviewData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // review-1
        $review = new Review();
        $review->setComment('you are fab');
        $review->setRating(5);
        $review->setTrainer($this->getReference('trainer-1'));
        $review->setClient($this->getReference('client-1'));

        $manager->persist($review);

        // review-2
        $review = new Review();
        $review->setComment('you are great');
        $review->setRating(4);
        $review->setTrainer($this->getReference('trainer-1'));
        $review->setClient($this->getReference('client-1'));

        $manager->persist($review);

        $manager->flush();

    }

    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }

}
