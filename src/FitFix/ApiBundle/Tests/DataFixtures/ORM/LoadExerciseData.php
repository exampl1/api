<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Exercise;
use FitFix\CoreBundle\Entity\Equipment;
use FitFix\CoreBundle\Entity\Muscle;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadExerciseData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $row = 0;
        $equipmentEntities = array();
        $muscleEntities = array();
        if (($handle = fopen(__DIR__."/Data/exercises.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row > 0 && $data[0] != '') {
                    $exercise = new Exercise();
                    $exercise->setName(ucwords(strtolower(trim($data[0]))));
                    $exercise->setPreparation(trim($data[4]));
                    $exercise->setExecution(trim($data[5]));
                    $exercise->setRecovery(trim($data[6]));

                    if (array_key_exists(ucwords(strtolower(trim($data[1]))), $muscleEntities)) {
                        $exercise->addMuscle($muscleEntities[ucwords(strtolower(trim($data[1])))]);
                    }
                    else {
                        $muscle = new Muscle();
                        $muscle->setName(ucwords(strtolower(trim($data[1]))));
                        $muscleEntities[$muscle->getName()] = $muscle;
                        $exercise->addMuscle($muscle);
                    }

                    if ($data[2] != '') {

                        if (array_key_exists(ucwords(strtolower(trim($data[2]))), $equipmentEntities)) {
                            $exercise->addEquipment($equipmentEntities[ucwords(strtolower(trim($data[2])))]);
                        }
                        else {
                            $equipment = new Equipment();
                            $equipment->setName(ucwords(strtolower(trim($data[2]))));
                            $equipmentEntities[$equipment->getName()] = $equipment;
                            $exercise->addEquipment($equipment);
                        }

                    }

                    if ($data[3] != '') {

                        if (array_key_exists(ucwords(strtolower(trim($data[3]))), $equipmentEntities)) {
                            $exercise->addEquipment($equipmentEntities[ucwords(strtolower(trim($data[3])))]);
                        }
                        else {
                            $equipment = new Equipment();
                            $equipment->setName(ucwords(strtolower(trim($data[3]))));
                            $equipmentEntities[$equipment->getName()] = $equipment;
                            $exercise->addEquipment($equipment);
                        }

                    }

                    $manager->persist($exercise);
                    $this->addReference('exercise-'.$row, $exercise);
                }

                $row++;
            }
            fclose($handle);
        }
        $manager->flush();

        $exercises = $manager->getRepository('FitFixCoreBundle:Exercise')->findAll();
        foreach ($exercises as $exercise) {

            $name = urlencode(strtoupper($exercise->getName()));

            $exercise->setWebmVideoPath("videos/workouts/$name.android.webm");
            $exercise->setMp4VideoPath("videos/workouts/$name.ipad3.mp4");
            $exercise->setPosterframePath("videos/workouts/$name.ipad3.png");
            $exercise->setPicturePath("photos/workouts/$name.jpg");
            $exercise->setCdnHost('https://s3-eu-west-1.amazonaws.com/fitfix/');
            $manager->persist($exercise);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 12; // the order in which fixtures will be loaded
    }

}
