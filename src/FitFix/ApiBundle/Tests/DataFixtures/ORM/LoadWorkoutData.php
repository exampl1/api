<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Workout;
use FitFix\CoreBundle\Entity\Intensity;
use FitFix\CoreBundle\Entity\Technique;
use FitFix\CoreBundle\Entity\Step;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadWorkoutData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // workout-1
        $workout = new Workout();
        $workout->setClient($this->getReference('client-1'));
        $workout->setName('Morning');
        $workout->setCreated(new \DateTime());
        $workout->setTrainer($this->getReference('trainer-1'));

        $intensity = new Intensity();
        $intensity->setName('Hardcore');
        $intensity->setDescription('Really intense');
        $workout->setIntensity($intensity);

        $technique = new Technique();
        $technique->setDescription('Slow');
        $technique2 = new Technique();
        $technique2->setDescription('Fast');
        $technique3 = new Technique();
        $technique3->setDescription('Medium');

        $warmup = new Step();
        $warmup->setName('Stretch');
        $warmup->setReps(1);
        $warmup->setWeight(1);
        $warmup->setSets(1);
        $warmup->setTempo(1);
        $warmup->setRest(1);
        $warmup->setExercise($this->getReference('exercise-1'));
        $warmup->setTechnique($technique);
        $workout->addWarmup($warmup);

        $workoutStep = new Step();
        $workoutStep->setName('Run');
        $workoutStep->setReps(1);
        $workoutStep->setWeight(1);
        $workoutStep->setSets(1);
        $workoutStep->setTempo(1);
        $workoutStep->setRest(1);
        $workoutStep->setExercise($this->getReference('exercise-1'));
        $workoutStep->setTechnique($technique2);
        $workout->addWarmup($workoutStep);

        $warmdown = new Step();
        $warmdown->setName('Walk');
        $warmdown->setReps(1);
        $warmdown->setWeight(1);
        $warmdown->setSets(1);
        $warmdown->setTempo(1);
        $warmdown->setRest(1);
        $warmdown->setExercise($this->getReference('exercise-1'));
        $warmdown->setTechnique($technique3);
        $workout->addWarmup($warmdown);

        $manager->persist($workout);

        // workout-2
        $workout = new Workout();
        $workout->setClient($this->getReference('client-2'));
        $workout->setName('Weekend');
        $workout->setCreated(new \DateTime());
        $workout->setTrainer($this->getReference('trainer-1'));
        
        $intensity = new Intensity();
        $intensity->setName('Super Hardcore');
        $intensity->setDescription('Really Really intense');
        $workout->setIntensity($intensity);
        

        $warmup = new Step();
        $warmup->setName('Stretch');
        $warmup->setReps(1);
        $warmup->setWeight(1);
        $warmup->setSets(1);
        $warmup->setTempo(1);
        $warmup->setRest(1);
        $warmup->setExercise($this->getReference('exercise-4'));
        $warmup->setTechnique($technique);
        $workout->addWarmup($warmup);
        
        $workoutStep = new Step();
        $workoutStep->setName('Run');
        $workoutStep->setReps(1);
        $workoutStep->setWeight(1);
        $workoutStep->setSets(1);
        $workoutStep->setTempo(1);
        $workoutStep->setRest(1);
        $workoutStep->setExercise($this->getReference('exercise-5'));
        $workoutStep->setTechnique($technique2);
        $workout->addWarmup($workoutStep);
        
        $warmdown = new Step();
        $warmdown->setName('Walk');
        $warmdown->setReps(1);
        $warmdown->setWeight(1);
        $warmdown->setSets(1);
        $warmdown->setTempo(1);
        $warmdown->setRest(1);
        $warmdown->setExercise($this->getReference('exercise-6'));
        $warmdown->setTechnique($technique3);
        $workout->addWarmup($warmdown);

        $manager->persist($workout);

        $manager->flush();

    }

    public function getOrder()
    {
        return 13; // the order in which fixtures will be loaded
    }

}
