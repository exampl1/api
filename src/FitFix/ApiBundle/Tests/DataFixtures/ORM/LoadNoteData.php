<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Note;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadNoteData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // note-1
        $note = new Note();
        $note->setClient($this->getReference('client-1'));
        $note->setDescription('Go to Gym');

        $manager->persist($note);

        // note-2
        $note = new Note();
        $note->setClient($this->getReference('client-2'));
        $note->setDescription('Go on a run');

        $manager->persist($note);

        $manager->flush();

    }

    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }

}
