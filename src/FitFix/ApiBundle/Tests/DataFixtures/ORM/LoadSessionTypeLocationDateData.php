<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Address;
use FitFix\CoreBundle\Entity\SessionType;
use FitFix\CoreBundle\Entity\SessionTypeLocation;
use FitFix\CoreBundle\Entity\SessionTypeLocationDate;
use FitFix\CoreBundle\Entity\SessionTypeLocationDateTime;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadSessionTypeLocationDateData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*
        // sessionTypeLocationdate-1
        $sessionTypeLocationDate = new SessionTypeLocationDate();
        $sessionTypeLocationDate->setStartTime(new DateTime('2013-08-10 10:00:00'));
        $sessionTypeLocationDate->setEndTime(new DateTime('2013-08-10 11:00:00'));
        $sessionTypeLocationDate->setLocation($this->getReference('sessiontypelocation-1'));

        $manager->persist($sessionTypeLocationDate);

        // sessionTypeLocationdate-1
        $sessionTypeLocationDate = new SessionTypeLocationDate();
        $sessionTypeLocationDate->setStartTime(new DateTime('2013-08-10 10:00:00'));
        $sessionTypeLocationDate->setEndTime(new DateTime('2013-08-10 11:00:00'));
        $sessionTypeLocationDate->setLocation($this->getReference('sessiontypelocation-1'));

        $manager->persist($sessionTypeLocationDate);

        $manager->flush();
        */
    }

    public function getOrder()
    {
        return 12; // the order in which fixtures will be loaded
    }

}
