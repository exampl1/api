<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Objective;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadObjectiveData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // objective-1
        $objective = new Objective();
        $objective->setClient($this->getReference('client-1'));
        $objective->setDescription('Get Fit');

        $manager->persist($objective);

        // objective-2
        $objective = new Objective();
        $objective->setClient($this->getReference('client-2'));
        $objective->setDescription('Keep Fit');

        $manager->persist($objective);

        $manager->flush();

    }

    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }

}
