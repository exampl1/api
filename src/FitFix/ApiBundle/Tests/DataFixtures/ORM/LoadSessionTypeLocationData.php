<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Address;
use FitFix\CoreBundle\Entity\SessionType;
use FitFix\CoreBundle\Entity\SessionTypeLocation;
use FitFix\CoreBundle\Entity\SessionTypeLocationDate;
use FitFix\CoreBundle\Entity\SessionTypeLocationDateTime;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadSessionTypeLocationData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*
        // sessionTypeLocation-1
        $sessionTypeLocation = new SessionTypeLocation();
        $sessionTypeLocation->setAddress($this->getReference('address-1'));
        $sessionTypeLocation->setSessionType($this->getReference('sessiontype-1'));

        $manager->persist($sessionTypeLocation);

        $this->addReference('sessiontypelocation-1', $sessionTypeLocation);

        // sessionTypeLocation-2
        $sessionTypeLocation = new SessionTypeLocation();
        $sessionTypeLocation->setAddress($this->getReference('address-3'));
        $sessionTypeLocation->setSessionType($this->getReference('sessiontype-2'));

        $manager->persist($sessionTypeLocation);

        $this->addReference('sessiontypelocation-2', $sessionTypeLocation);

        $manager->flush();
        */
    }

    public function getOrder()
    {
        return 11; // the order in which fixtures will be loaded
    }

}
