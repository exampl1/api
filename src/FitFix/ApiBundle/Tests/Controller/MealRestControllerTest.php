<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;

use Symfony\Component\BrowserKit\Client as BrowserClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for meal rest controller
 *
 */
class MealRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("client1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get meals
     *
     * @return none
     */
    public function testGetMealsAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/meals');
        $content = $this->client->getResponse()->getContent();
        $meals = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $meals);
    }

    /**
     * Test get meal
     *
     * @return none
     */
    public function testGetMealAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/meals/1');
        $content = $this->client->getResponse()->getContent();
        $meal = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Energy Bar', $meal->description);
    }

    /**
     * Test get client nonexistent slug
     *
     * @return none
     */
    public function testGetMealAction_invalid_client()
    {
        $this->client->request('GET', '/api/clients/client0/meals');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get client unauthorised slug
     *
     * @return none
     */
    public function testGetMealAction_unauthorised_client()
    {
        $this->client->request('GET', '/api/clients/client2/meals');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create meal
     *
     * @return none
     */
    public function testPostMealsAction()
    {

        $params = array(
            'description'      => 'Eat Well'
        );

        $this->client->request('POST', '/api/clients/client1/meals', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/clients\/client1\/meals\/3/', $this->client->getResponse()->headers->get('location'));

        $meal = $this->em->getRepository('FitFixCoreBundle:Meal')->find(3);
        $this->assertNotNull($meal, "Meal created");
        if ($meal) {
            $this->em->remove($meal);
            $this->em->flush();
        }
    }

    /**
     * Test create meal invalid data
     *
     * @return none
     */
    public function testPostMealsAction_invalid_data()
    {
        $params = array(
            'description'      => ''
        );

        $this->client->request('POST', '/api/clients/client1/meals', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"description":["Please enter a description"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing meal
     *
     * @return none
     */
    public function testDeleteMealAction()
    {
        $this->client->request('DELETE', '/api/clients/client1/meals/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete meal nonexistent
     *
     * @return none
     */
    public function testDeleteMealAction_invalid_meal()
    {
        $this->client->request('DELETE', '/api/clients/client1/meals/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete meal without id
     *
     * @return none
     */
    public function testDeleteMealAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients/client1/meals');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update meal
     *
     * @return none
     */
    public function testPutMealAction()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/meals/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update meal invalid id
     *
     * @return none
     */
    public function testPutMealAction_invalid_id()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/meals/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update meal unauthorised slug
     *
     * @return none
     */
    public function testPutMealAction_unauthorised_client()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('GET', '/api/clients/client2/meals/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsMealsAction()
    {
        $this->client->request('OPTIONS', '/api/client/meals', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
