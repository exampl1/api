<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for address rest controller
 *
 */
class ReviewRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var type
     */
    private $client1;

    /**
     * Browser Client
     * @var type
     */
    private $client2;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var type
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->userManager = $kernel->getContainer()->get('fos_user.user_manager');

        $this->em = $kernel->getContainer()->get('doctrine')->getManager();

        /*
         * Creation of the browser client with the trainer1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );

            $this->client1 = static::createClient(array(), $this->header);
        }

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("client1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );

            $this->client2 = static::createClient(array(), $this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get reviews
     *
     * @return none
     */
    public function testGetReviewsAction()
    {
        $this->client1->request('GET', '/api/reviews');
        $content = $this->client1->getResponse()->getContent();

        $reviews = json_decode($content, false);

        $this->assertEquals(200, $this->client1->getResponse()->getStatusCode());

        $this->assertTrue($this->client1->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(2, $reviews);
    }

    /**
     * Test get review
     *
     * @return none
     */
    public function testGetReviewAction()
    {
        $this->client1->request('GET', '/api/reviews/1');
        $content = $this->client1->getResponse()->getContent();

        $reviews = json_decode($content, false);

        $this->assertEquals(200, $this->client1->getResponse()->getStatusCode());

        $this->assertTrue($this->client1->getResponse()->headers->contains('Content-Type', 'application/json'));

    }

    /**
     * Test get review as trainer
     *
     * @return none
     */
    public function testGetTrainerReviewActionAsTrainer()
    {
        $this->client1->request('GET', '/api/trainers/1/reviews/1');
        $content = $this->client1->getResponse()->getContent();

        $review = json_decode($content, false);

        $this->assertEquals(200, $this->client1->getResponse()->getStatusCode());

        $this->assertTrue($this->client1->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"comment":"you are fab","rating":5,"client":{"id":1},"trainer":{"id":1}}';
        $this->assertEquals($expected, $this->client1->getResponse()->getContent());

    }

    /**
     * Test get review as client
     *
     * @return none
     */
    public function testGetTrainerReviewActionAsClient()
    {
        $this->client2->request('GET', '/api/trainers/1/reviews/1');
        $content = $this->client2->getResponse()->getContent();

        $review = json_decode($content, false);

        $this->assertEquals(200, $this->client2->getResponse()->getStatusCode());

        $this->assertTrue($this->client2->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"comment":"you are fab","rating":5,"client":{"id":1},"trainer":{"id":1}}';
        $this->assertEquals($expected, $this->client2->getResponse()->getContent());

    }

    /**
     * Test create review
     *
     * @return none
     */
    public function testPostTrainerReviewAction()
    {

        $params = array(
            'comment'     => 'Excellent',
            'rating'     => '3'
        );

        $this->client2->request(
            'POST',
            '/api/trainers/1/reviews',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($params)
        );

        $this->assertEquals(201, $this->client2->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/trainers\/1\/reviews\/3/', $this->client2->getResponse()->headers->get('location'));

        $review = $this->em->getRepository('FitFixCoreBundle:Review')->find(3);
        $this->assertNotNull($review, "Review created");
        if ($review) {
            $this->em->remove($review);
            $this->em->flush();
        }
    }
}
