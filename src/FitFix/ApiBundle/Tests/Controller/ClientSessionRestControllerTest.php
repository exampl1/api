<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Kernel;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for session rest controller as role client
 *
 */
class ClientSessionRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("client1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get sessions
     *
     * @return none
     */
    public function testGetSessionsAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/sessions');
        $content = $this->client->getResponse()->getContent();
        $sessions = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $sessions);
    }

    /**
     * Test get session
     *
     * @return none
     */
    public function testGetSessionAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/sessions/1');
        $content = $this->client->getResponse()->getContent();
        $session = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals(1, $session->trainer->id);
    }

    /**
     * Test get client nonexistent slug
     *
     * @return none
     */
    public function testGetSessionAction_invalid_client()
    {
        $this->client->request('GET', '/api/clients/client0/sessions');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get client unauthorised slug
     *
     * @return none
     */
    public function testGetSessionAction_unauthorised_client()
    {
        $this->client->request('GET', '/api/clients/client2/sessions');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create session
     *
     * @return none
     */
    public function testPostSessionsAction()
    {

        $params = array(
            'trainerId'     => '1',
            'startTime'     => '2013-05-01 10:00:00',
            'endTime'       => '2013-05-01 11:00:00',
            'requestStatus' => 'pending',
        );

        $this->client->request('POST', '/api/clients/client1/sessions', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/clients\/client1\/sessions\/4/', $this->client->getResponse()->headers->get('location'));

        $session = $this->em->getRepository('FitFixCoreBundle:Session')->find(4);
        $this->assertNotNull($session, "Session created");
        if ($session) {
            $this->em->remove($session);
            $this->em->flush();
        }
    }

    /**
     * Test create session invalid data
     *
     * @return none
     */
    public function testPostSessionsAction_invalid_data()
    {
        $params = array(
            'trainerId'     => '1'
        );

        $this->client->request('POST', '/api/clients/client1/sessions', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"startTime":["Please enter a start date\/time (YYYY-MM-DD HH:MM:SS)"],"endTime":["Please enter an end date\/time (YYYY-MM-DD HH:MM:SS)"],"requestStatus":["Please select a request status"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing session
     *
     * @return none
     */
    public function testDeleteSessionAction()
    {
        $this->client->request('DELETE', '/api/clients/client1/sessions/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete session nonexistent
     *
     * @return none
     */
    public function testDeleteSessionAction_invalid_session()
    {
        $this->client->request('DELETE', '/api/clients/client1/sessions/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete session without id
     *
     * @return none
     */
    public function testDeleteSessionAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients/client1/sessions');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update session
     *
     * @return none
     */
    public function testPutSessionAction()
    {
        $params = array('requestStatus' => 'complete');

        $this->client->request('PUT', '/api/clients/client1/sessions/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update session invalid id
     *
     * @return none
     */
    public function testPutSessionAction_invalid_id()
    {
        $params = array('requestStatus' => 'active');

        $this->client->request('PUT', '/api/clients/client1/sessions/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update session unauthorised slug
     *
     * @return none
     */
    public function testPutSessionAction_unauthorised_client()
    {
        $params = array('requestStatus' => 'cancelled');

        $this->client->request('GET', '/api/clients/client2/sessions/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsSessionsAction()
    {
        $this->client->request('OPTIONS', '/api/client/sessions', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
