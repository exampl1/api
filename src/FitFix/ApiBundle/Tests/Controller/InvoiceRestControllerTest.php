<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for invoice rest controller
 *
 */
class InvoiceRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var type
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var type
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->userManager = $kernel->getContainer()->get('fos_user.user_manager');

        $this->em = $kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the trainer1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client = static::createClient(array(), $this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get invoices
     *
     * @return none
     */
    public function testGetInvoicesAction()
    {
        $this->client->request('GET', '/api/invoices');
        $content = $this->client->getResponse()->getContent();
        $invoices = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $invoices);
    }

    /**
     * Test get invoice
     *
     * @return none
     */
    public function testGetInvoiceAction()
    {
        $this->client->request('GET', '/api/invoices/1');
        $content = $this->client->getResponse()->getContent();
        $invoice = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals(20, $invoice->price);
    }

    /**
     * Test create invoice
     *
     * @return none
     */
    public function testPostInvoicesAction()
    {
        $params = array(
            'paid'      => '2013-08-10 10:00:00',
            'price'     => 50,
            'status'    => 'paid',
            'trainerId' => 1,
            'clientId'  => 1,
            'sessionId' => 3
        );

        $this->client->request('POST', '/api/invoices', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/invoices\/3/', $this->client->getResponse()->headers->get('location'));

        $invoice = $this->em->getRepository('FitFixCoreBundle:Invoice')->find(3);
        $this->assertNotNull($invoice, "Invoice created");
        if ($invoice) {
            $this->em->remove($invoice);
            $this->em->flush();
        }
    }

    /**
     * Test create invoice invalid data
     *
     * @return none
     */
    public function testPostInvoicesAction_invalid_data()
    {
        $params = array(
            'paid'      => '2013-08-10 10:00:00',
            'price'     => 'something',
            'status'    => 'blah'
        );

        $this->client->request('POST', '/api/invoices', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"price":["You must enter a number"],"status":["Please choose a valid status"],"trainer":["Please select a trainer"],"client":["Please select a client"],"session":["Please select a session"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing invoice
     *
     * @return none
     */
    public function testDeleteInvoiceAction()
    {
        $this->client->request('DELETE', '/api/invoices/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete invoice nonexistent
     *
     * @return none
     */
    public function testDeleteInvoiceAction_invalid_invoice()
    {
        $this->client->request('DELETE', '/api/invoices/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete invoice without id
     *
     * @return none
     */
    public function testDeleteInvoiceAction_without_id()
    {
        $this->client->request('DELETE', '/api/invoices');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update invoice
     *
     * @return none
     */
    public function testPutInvoiceAction()
    {
        $params = array(
            'paid'      => '2013-08-11 10:00:00'
        );

        $this->client->request('PUT', '/api/invoices/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update invoice invalid id
     *
     * @return none
     */
    public function testPutInvoiceAction_invalid_id()
    {
        $params = array(
            'paid'      => '2013-08-11 10:00:00'
        );

        $this->client->request('PUT', '/api/invoices/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsInvoicesAction()
    {
        $this->client->request('OPTIONS', '/api/invoices', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
