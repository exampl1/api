<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for sessiontypelocation rest controller as role trainer
 *
 */
class TrainerSessionTypeLocationRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {

        $this->client = static::createClient();

        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get sessionTypeLocations
     *
     * @return none
     */
    public function testGetSessionTypeLocationsAction_valid_user()
    {
        $this->client->request('GET', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations');
        $content = $this->client->getResponse()->getContent();
        $sessionTypes = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $sessionTypes);
    }

    /**
     * Test get sessionTypeLocation
     *
     * @return none
     */
    public function testGetSessionTypeLocationAction_valid_user()
    {
        $this->client->request('GET', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations/1');
        $content = $this->client->getResponse()->getContent();
        $sessionTypeLocation = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals(1, $sessionTypeLocation->id);
    }

    /**
     * Test get sessionTypeLocation nonexistent slug
     *
     * @return none
     */
    public function testGetSessionTypeLocationAction_invalid_user()
    {
        $this->client->request('GET', '/api/trainers/trainer0/sessiontypes/1/sessiontypelocations/1');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get sessionTypeLocation unauthorised slug
     *
     * @return none
     */
    public function testGetSessionTypeLocationAction_unauthorised_user()
    {
        $this->client->request('GET', '/api/trainers/trainer2/sessiontypes/1/sessiontypelocations/1');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create sessionTypeLocation and set for sessionType id 1
     *
     * @return none
     */
    public function testPostSessionTypeLocationsAction()
    {
        $params = array(
            'addressId' => 3
        );

        $this->client->request('POST', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/trainers\/trainer1\/sessiontypes\/1\/sessiontypelocations\/3/', $this->client->getResponse()->headers->get('location'));

        $sessionTypeLocation = $this->em->getRepository('FitFixCoreBundle:SessionTypeLocation')->find(3);
        $this->assertNotNull($sessionTypeLocation, "SessionTypeLocation created");
        // we don't remove the entity as it has replaced the existing location for sessiontype id 1
    }

    /**
     * Test create sessionTypeLocation invalid data
     *
     * @return none
     *
     */
    public function testPostSessionTypeLocationsAction_invalid_data()
    {
        $params = array(
            'addressId'     => ''
        );

        $this->client->request('POST', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"address":["Please select an address"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing sessionTypeLocation
     *
     * @return none
     */
    public function testDeleteSessionTypeLocationAction()
    {
        $this->client->request('DELETE', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete sessionTypeLocation nonexistent
     *
     * @return none
     */
    public function testDeleteSessionTypeLocationAction_invalid_id()
    {
        $this->client->request('DELETE', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete sessionType without id
     *
     * @return none
     */
    public function testDeleteSessionAction_without_id()
    {
        $this->client->request('DELETE', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update sessionType
     *
     * @return none
     */
    public function testPutSessionTypeAction()
    {
        $params = array('addressId' => 3);

        $this->client->request('PUT', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update sessionType invalid id
     *
     * @return none
     */
    public function testPutSessionAction_invalid_id()
    {
        $params = array('addressId' => 2);

        $this->client->request('PUT', '/api/trainers/trainer1/sessiontypes/1/sessiontypelocations/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update sessionType unauthorised slug
     *
     * @return none
     */
    public function testPutSessionAction_unauthorised_client()
    {
        $params = array('addressId' => 2);

        $this->client->request('GET', '/api/trainers/trainer2/sessiontypes/1/sessiontypelocations/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsSessionTypeLocationsAction()
    {
        $this->client->request('OPTIONS', '/api/trainer/sessiontype/sessiontypelocations', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
