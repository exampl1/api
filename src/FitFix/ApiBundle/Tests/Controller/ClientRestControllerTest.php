<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for client rest controller
 *
 */
class ClientRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();

        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();

        /*
         * Creation of the browser client with the trainer1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get clients
     *
     * @return none
     */
    public function testGetClientsAction()
    {
        $this->client->request('GET', '/api/clients');
        $content = $this->client->getResponse()->getContent();

        $clients = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(3, $clients);
    }

    /**
     * Test get client
     *
     * @return none
     */
    public function testGetClientAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/1', array(), array(), $this->header);
        $content = $this->client->getResponse()->getContent();

        $client = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('client1', $client->user->username);
        $this->assertEquals('client1@test.com', $client->user->email);
        $this->assertEquals('male', $client->gender);
    }

    /**
     * Test get client nonexistent id
     *
     * @return none
     */
    public function testGetClientAction_invalid_client()
    {
        $this->client->request('GET', '/api/clients/0');

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create client
     *
     * @return none
     */
    public function testPostClientsAction()
    {

        $params = array(
            'username'      => 'client4',
            'email'         => 'client4@test.com',
            'plainPassword' => '12345',
            'gender'        => 'male',
            'dob'           => '1980-12-25',
            'mobile'        => '1234567',
            'firstName'     => 'Joe',
            'lastName'      => 'Bloggs',
            'trainerId'     => 1
        );

        $this->client->request('POST', '/api/clients', $params, array());

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/clients\/4/', $this->client->getResponse()->headers->get('location'));

        $user = $this->userManager->findUserByUsername("client4");
        $this->assertNotNull($user);
        if ($user) {
            $this->userManager->deleteUser($user);
        }
    }

    /**
     * Test create client invalid data
     *
     * @return none
     */
    public function testPostClientsAction_invalid_data()
    {
        $params = array(
            'username'      => 'client4',
            'plainPassword' => '12345',
            'email'         => 'client4_test.com',
            'trainerId'     => 1
        );

        $this->client->request('POST', '/api/clients', $params, array());

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"gender":["Please select your gender"],"dob":["Please enter your date or birth"],"firstName":["Please enter your first name"],"lastName":["Please enter your last name"],"user.email":["The email is not valid"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing client
     *
     * @return none
     */
    public function testDeleteClientAction()
    {
        $this->client->request('DELETE', '/api/clients/2');

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        // Assert that the "Content-Type" header is "application/json"
        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete client nonexistent
     *
     * @return none
     */
    public function testDeleteClientAction_invalid_user()
    {
        $this->client->request('DELETE', '/api/clients/0');

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        // Assert that the "Content-Type" header is "application/json"
        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete client without id
     *
     * @return none
     */
    public function testDeleteClientAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients');

        // Assert a specific 400 status code
        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test delete client unauthorised trainer
     *
     * @return none
     */
    public function testDeleteClientAction_unauthorised_trainer()
    {
        $this->client->request('DELETE', '/api/clients/2');

        // Assert a specific 401 status code
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update client
     *
     * @return none
     */
    public function testPutClientAction()
    {
        $params = array('mobile' => '987654321');

        $this->client->request('PUT', '/api/clients/1', $params);

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update client invalid id
     *
     * @return none
     */
    public function testPutClientAction_invalid_slug()
    {
        $params = array('mobile' => '987654321');

        $this->client->request('PUT', '/api/clients/one', $params);

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update client invalid password
     *
     * @return none
     */
    public function testPutClientAction_invalid_password()
    {
        $params = array('plainPassword' => '5');
        //$headers = array('CONTENT_TYPE' => 'application/x-www-form-urlencoded');

        $this->client->request('PUT', '/api/clients/1', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update client invalid username
     *
     * @return none
     */
    public function testPutClientAction_invalid_username()
    {
        $params = array('username' => '5');
        //$headers = array('CONTENT_TYPE' => 'application/x-www-form-urlencoded');

        $this->client->request('PUT', '/api/clients/1', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update client invalid email
     *
     * @return none
     */
    public function testPutClientAction_invalid_email()
    {
        $params = array('email' => '5');
        //$headers = array('CONTENT_TYPE' => 'application/x-www-form-urlencoded');

        $this->client->request('PUT', '/api/clients/1', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsClientsAction()
    {
        $this->client->request('OPTIONS', '/api/clients', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
