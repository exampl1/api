<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for sessiontype rest controller as role trainer
 *
 */
class TrainerSessionTypeRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {

        $this->client = static::createClient();

        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get sessiontypes
     *
     * @return none
     */
    public function testGetSessionTypesAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/trainer1/sessiontypes');
        $content = $this->client->getResponse()->getContent();
        $sessionTypes = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(2, $sessionTypes);
    }

    /**
     * Test get sessionType
     *
     * @return none
     */
    public function testGetSessionTypeAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/trainer1/sessiontypes/1');
        $content = $this->client->getResponse()->getContent();
        $sessionType = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals(1, $sessionType->trainer->id);
    }

    /**
     * Test get sessionType nonexistent slug
     *
     * @return none
     */
    public function testGetSessionTypeAction_invalid_user()
    {
        $this->client->request('GET', '/api/clients/trainer0/sessiontypes');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get sessionType unauthorised slug
     *
     * @return none
     */
    public function testGetSessionAction_unauthorised_user()
    {
        $this->client->request('GET', '/api/clients/trainer2/sessiontypes');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create sessionType
     *
     * @return none
     */
    public function testPostSessionTypesAction()
    {
        $params = array(
            'description' => 'Cross Fit Training',
            'price'       => 10
        );

        $this->client->request('POST', '/api/clients/trainer1/sessiontypes', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/trainers\/trainer1\/sessiontypes\/3/', $this->client->getResponse()->headers->get('location'));

        $sessionType = $this->em->getRepository('FitFixCoreBundle:SessionType')->find(3);
        $this->assertNotNull($sessionType, "SessionType created");
        if ($sessionType) {
            $this->em->remove($sessionType);
            $this->em->flush();
        }
    }

    /**
     * Test create sessionType invalid data
     *
     * @return none
     */
    public function testPostSessionTypesAction_invalid_data()
    {
        $params = array(
            'description'     => '',
            'price'           => 'Ten pounds'
        );

        $this->client->request('POST', '/api/clients/trainer1/sessiontypes', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"description":["Please enter a description"],"price":["You must enter a number"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing sessionType
     *
     * @return none
     */
    public function testDeleteSessionTypeAction()
    {
        $this->client->request('DELETE', '/api/clients/trainer1/sessiontypes/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete sessionType nonexistent
     *
     * @return none
     */
    public function testDeleteSessionTypeAction_invalid_id()
    {
        $this->client->request('DELETE', '/api/clients/trainer1/sessiontypes/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete sessionType without id
     *
     * @return none
     */
    public function testDeleteSessionAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients/trainer1/sessiontypes');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update sessionType
     *
     * @return none
     */
    public function testPutSessionTypeAction()
    {
        $params = array('description' => 'Running');

        $this->client->request('PUT', '/api/clients/trainer1/sessiontypes/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update sessionType invalid id
     *
     * @return none
     */
    public function testPutSessionAction_invalid_id()
    {
        $params = array('description' => 'Running');

        $this->client->request('PUT', '/api/clients/trainer1/sessiontypes/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update sessionType unauthorised slug
     *
     * @return none
     */
    public function testPutSessionAction_unauthorised_client()
    {
        $params = array('description' => 'Running');

        $this->client->request('GET', '/api/trainers/trainer2/sessiontypes/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsSessionTypesAction()
    {
        $this->client->request('OPTIONS', '/api/trainer/sessiontypes', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
