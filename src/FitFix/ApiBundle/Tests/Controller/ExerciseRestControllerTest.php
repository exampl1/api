<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for exercise rest controller
 *
 */
class ExerciseRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var type
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var type
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->userManager = $kernel->getContainer()->get('fos_user.user_manager');

        $this->em = $kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client = static::createClient(array(), $this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get exercises
     *
     * @return none
     */
    public function testGetExercisesAction()
    {
        $this->client->request('GET', '/api/exercises');
        $content = $this->client->getResponse()->getContent();
        $exercises = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(619, $exercises);
    }

    /**
     * Test get exercises with keywords search
     *
     * @return none
     */
    public function testGetExercisesAction_keywords()
    {
        $this->client->request('GET', '/api/exercises?keywords[]=forearms');
        $content = $this->client->getResponse()->getContent();
        $exercises = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(25, $exercises);
    }

    /**
     * Test get exercise
     *
     * @return none
     */
    public function testGetExerciseAction()
    {
        $this->client->request('GET', '/api/exercises/1');
        $content = $this->client->getResponse()->getContent();
        $exercises = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Reverse Wrist Flexion', $exercises->name);
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsExercisesAction()
    {
        $this->client->request('OPTIONS', '/api/exercises', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET'));
    }
}
