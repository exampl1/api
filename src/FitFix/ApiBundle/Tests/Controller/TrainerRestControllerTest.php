<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Trainer;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for trainer rest controller
 *
 */
class TrainerRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();
        $user = $this->userManager->findUserByUsername("trainer1");

        /*
         * Creation of the browser client with the trainer1 authenticated header
         */
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get trainer
     *
     * @return none
     */
    public function testGetTrainerAction_valid_user()
    {
        $this->client->request('GET', '/api/trainers/1', array(), array(), $this->header);
        $content = $this->client->getResponse()->getContent();
        $trainer = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('trainer1', $trainer->user->username);
        $this->assertEquals('trainer1@test.com', $trainer->user->email);
        $this->assertEquals('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae risus vel elit pretium pulvinar in in sem. Curabitur adipiscing feugiat viverra. Vivamus et tincidunt purus.', $trainer->personal_details);
    }

    /**
     * Test get trainer nonexistent id
     *
     * @return none
     */
    public function testGetTrainerAction_invalid_trainer()
    {
        $this->client->request('GET', '/api/trainers/0');

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create trainer
     *
     * @return none
     */
    public function testPostTrainersAction()
    {

        $params = array(
            'username'        => 'trainer3',
            'plainPassword'   => '12345',
            'email'           => 'trainer3@test.com',
            'firstName'       => 'Phil',
            'lastName'        => 'Adams',
            'personalDetails' => 'test',
            'specialisms'     => 'test',
            'bio'             => 'test'
        );

        $this->client->request('POST', '/api/trainers', $params, array());

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/trainers\/3/', $this->client->getResponse()->headers->get('location'));

        $user = $this->userManager->findUserByUsername("trainer3");
        $this->assertNotNull($user);
        if ($user) {
            $this->userManager->deleteUser($user);
        }
    }

    /**
     * Test create trainer invalid data
     *
     * @return none
     */
    public function testPostUsersAction_invalid_data()
    {
        $params = array(
            'username'        => 'trainer4',
            'plainPassword'   => '12345',
            'email'           => 'trainer4_test.com'
        );

        $this->client->request('POST', '/api/trainers', $params, array());

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"firstName":["Please enter your first name"],"lastName":["Please enter your last name"],"personalDetails":["Please enter your personal details"],"specialisms":["Please enter your specialisms"],"bio":["Please enter your biography"],"user.email":["The email is not valid"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing trainer
     *
     * @return none
     */
    public function testDeleteTrainerAction()
    {
        $this->client->request('DELETE', '/api/trainers/2');

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        // Assert that the "Content-Type" header is "application/json"
        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete trainer nonexistent
     *
     * @return none
     */
    public function testDeleteTrainerAction_invalid_user()
    {
        $this->client->request('DELETE', '/api/trainers/0');

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        // Assert that the "Content-Type" header is "application/json"
        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete trainer without id
     *
     * @return none
     */
    public function testDeleteTrainerAction_without_id()
    {
        $this->client->request('DELETE', '/api/trainers');

        // Assert a specific 400 status code
        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update trainer
     *
     * @return none
     */
    public function testPutTrainerAction()
    {
        $params = array('plainPassword' => '222222');

        $this->client->request('PUT', '/api/trainers/1', $params);

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update trainer invalid id
     *
     * @return none
     */
    public function testPutTrainerAction_invalid_slug()
    {
        $params = array('plainPassword' => '54321');

        $this->client->request('PUT', '/api/trainers/one', $params);

        // Assert a specific 200 status code
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update trainer invalid password
     *
     * @return none
     */
    public function testPutTrainerAction_invalid_password()
    {
        $params = array('plainPassword' => '5');
        //$headers = array('CONTENT_TYPE' => 'application/x-www-form-urlencoded');

        $this->client->request('PUT', '/api/trainers/1', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update trainer invalid username
     *
     * @return none
     */
    public function testPutTrainerAction_invalid_username()
    {
        $params = array('username' => '5');
        //$headers = array('CONTENT_TYPE' => 'application/x-www-form-urlencoded');

        $this->client->request('PUT', '/api/trainers/1', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update trainer invalid email
     *
     * @return none
     */
    public function testPutTrainerAction_invalid_email()
    {
        $params = array('email' => '5');
        //$headers = array('CONTENT_TYPE' => 'application/x-www-form-urlencoded');

        $this->client->request('PUT', '/api/trainers/1', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsTrainersAction()
    {
        $this->client->request('OPTIONS', '/api/trainers', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
