<?php

namespace FitFix\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * FitFix Api Bundle Class.
 */
class FitFixApiBundle extends Bundle
{

}
