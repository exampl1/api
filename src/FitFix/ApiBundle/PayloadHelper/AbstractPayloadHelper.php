<?php

namespace FitFix\ApiBundle\PayloadHelper;

abstract class AbstractPayloadHelper {
	
	/**
	 * Populates an entity from a payload
	 * 
	 * @param stdObj $entity
	 * @param stdObj $payload
	 */
	public static function populateEntityFromPayload($entity, $payload, $prefix = ''){
		// Populate client data with common attributes
		foreach($payload as $key => $value){
			$field = 'set' . ucfirst(str_replace($prefix, '', $key));
			if(method_exists($entity, $field)){
				$entity->$field($value);
			}
		}
	}
	
}