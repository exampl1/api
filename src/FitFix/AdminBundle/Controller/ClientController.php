<?php

namespace FitFix\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FitFix\CoreBundle\Entity\Client;
use FitFix\AdminBundle\Form\ClientType;

/**
 * Client controller.
 *
 * @Route("/client")
 */
class ClientController extends Controller
{
    /**
     * Lists all Client entities.
     *
     * @Route("/", name="admin_client")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FitFixCoreBundle:Client')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Client entity.
     *
     * @Route("/{id}/show", name="admin_client_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Client entity.
     *
     * @Route("/new", name="admin_client_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Client();
        $form   = $this->createForm(new ClientType('add'), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Client entity.
     *
     * @Route("/create", name="admin_client_create")
     * @Method("POST")
     * @Template("FitFixAdminBundle:Client:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Client();


        $request = $this->getRequest();
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        /*
        $user->setUsername($request->get('fitfix_adminbundle_clienttype')['user']['username']);
        $user->setEmail($request->get('fitfix_adminbundle_clienttype')['user']['email']);
        $user->setPlainPassword($request->get('fitfix_adminbundle_clienttype')['user']['plainPassword']);
        */
        $user->addRole($request->get('role_client'));

        $user->setClient($entity);
        //$entity->setUser($user);

        $form = $this->createForm(new ClientType('add'), $entity);
        $form->bind($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity, array('Registration'));
        //var_dump($errors);die;

        if (count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_client_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Client entity.
     *
     * @Route("/{id}/edit", name="admin_client_edit")
     * @Template()
     */
    public function editAction($id)
    {


        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

       // print_r($entity);die;

        $editForm = $this->createForm(new ClientType(), $entity);

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Client entity.
     *
     * @Route("/{id}/update", name="admin_client_update")
     * @Method("POST")
     * @Template("FitFixAdminBundle:Client:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ClientType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_client_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Client entity.
     *
     * @Route("/{id}/delete", name="admin_client_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FitFixCoreBundle:Client')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Client entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_client'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
