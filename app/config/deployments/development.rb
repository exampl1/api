server 'fitfix.dev.fishrod.co.uk', :app, :web, :primary => true

set   :deploy_to,     "/var/www/graph.fitfix.dev.fishrod.co.uk/application"
set   :user,		  "ubuntu"
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "staging-environments.pem")]
ssh_options[:forward_agent] = true

set :webserver_user,      "www-data"
set :dump_assetic_assets, true

namespace :deploy do
	# Apache needs to be restarted to make sure that the APC cache is cleared.
	# This overwrites the :restart task in the parent config which is empty.
	desc "Restart Apache"
	task :restart, :except => { :no_release => true }, :roles => :app do
		run "sudo service apache2 restart"
		puts "--> Apache successfully restarted".green
	end
    desc "Load Fixtures"
	task :restart, :except => { :no_release => true }, :roles => :app do
		run "cd #{deploy_to}/current/ && app/Resources/bin/setup.sh"
		puts "--> Fixtures successfully loaded".green
	end
end