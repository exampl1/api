# Deployments
set :stages,        %w(production staging development)
set :default_stage, "development"
set :stage_dir,     "app/config/deployments"
require 'capistrano/ext/multistage'

set :application, "fitfix fitness graph"

set :writable_dirs,       ["app/cache", "app/logs"]
set :permission_method,   :acl
set :use_set_permissions, true
set :webserver_user,      "www-data"

set   :scm,           :git
set   :repository,    "ssh://git@stash.fishrod.co.uk:7999/fit/fitfix-fitness-graph.git"
set	  :branch, fetch(:branch, "master")

set :deploy_via, :remote_cache

set :shared_files,      ["app/config/parameters.yml"]
set :shared_children,     [app_path + "/logs", "vendor"]
set :use_composer, true
set :update_vendors, true

set   :use_sudo,      false
set   :keep_releases, 2

after "deploy", "deploy:cleanup"

logger.level = Logger::MAX_LEVEL