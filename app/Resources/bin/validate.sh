#!/bin/bash

app/console doctrine:database:drop --force --env=test
app/console doctrine:database:create --env=test
app/console doctrine:schema:create --env=test
app/console doctrine:schema:update --env=test

app/console doctrine:fixtures:load --fixtures=src/FitFix/ApiBundle/Tests/DataFixtures/ORM --env=test

app/console fos:user:create admin admin@qa.int admin --env=test
app/console fos:user:activate admin --env=test
app/console fos:user:promote admin ROLE_DEVELOPER --env=test

app/console cache:clear --env=test
