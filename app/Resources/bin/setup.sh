#!/bin/bash

app/console doctrine:database:drop -n --force
app/console doctrine:database:create -n
app/console doctrine:schema:create -n
app/console doctrine:schema:update -n

app/console doctrine:fixtures:load -n --fixtures=src/FitFix/ApiBundle/Tests/DataFixtures/ORM

app/console fos:user:create -n admin admin@qa.int admin
app/console fos:user:activate -n admin
app/console fos:user:promote -n admin ROLE_DEVELOPER

app/console cache:clear -n
