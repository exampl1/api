// Filename: router.js
define([
    'jquery',
    'underscore',
    'backbone',
    'vm',
    'models/session',
], function ($, _, Backbone, Vm, Session) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            // Pages
            'client': 'client',
            'clients': 'clients',

            // Default - catch all
            '*actions': 'defaultAction'
        }
    });

    var initialize = function(options){
        var appView = options.appView;
        var router = new AppRouter(options);
        router.on('route:defaultAction', function (actions) {
            require(['views/dashboard/page'], function (DashboardPage) {
                var dashboardPage = Vm.create(appView, 'DashboardPage', DashboardPage);
                dashboardPage.render();
            });
        });
        router.on('route:client', function () {
            require(['views/client/page'], function (ClientPage) {
                var clientPage = Vm.create(appView, 'ClientPage', ClientPage);
                clientPage.render();
            });
        });
        router.on('route:clients', function () {
            require(['views/client/list'], function (ClientList) {
                var clientList = Vm.create(appView, 'ClientList', ClientList);
                clientList.render();
            });
        });
        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});
