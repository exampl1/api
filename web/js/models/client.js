define([
    'lodash',
    'backbone'
], function(_, Backbone) {
    var ClientModel = Backbone.Model.extend({
        urlRoot: '/api/clients'
    });
    return ClientModel;

});
