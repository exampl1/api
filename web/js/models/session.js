define([
    'jquery',
    'underscore',
    'backbone',
    'jquerycookie'
], function($, _, Backbone) {
    var SessionModel = Backbone.Model.extend({

        initialize: function () {
            this.set({ 'WSSE': $.cookie('WSSE')} );
        },
        login: function(username, password) {
            var that = this;
            this.urlRoot = '/security/token/create.json?_username='+username+'&_password='+password;
            this.save({}, {
                 success: function (token) {
                      $.cookie('WSSE', that.get('WSSE'));
                 }
            });
        },
        logout: function() {
            var that = this;
            this.urlRoot = '/security/token/destroy.json';
            this.fetch({
                success: function (model, resp) {
                    model.clear();
                    $.removeCookie('WSSE');
                }
            });
        },
        authenticated: function(){
            return Boolean(this.get('WSSE'));
        }
    });
    return new SessionModel();

});