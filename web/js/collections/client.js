define([
    'jquery',
    'lodash',
    'backbone',
    'models/client'
], function($, _, Backbone, clientModel){
    var clientCollection = Backbone.Collection.extend({
        model: clientModel,
        url: '/api/clients',
        initialize: function(){

        }

    });

    return clientCollection;
});
