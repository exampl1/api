define([
    'jquery',
    'handlebars',
    'backbone',
    'models/session',
    'collections/client',
    'text!templates/client/list.html',
], function($, _, Backbone, Session, ClientCollection, clientPageTemplate){
    var ClientList = Backbone.View.extend({
        el: '.page',
        render: function () {
            that = this;

            that.collection = new ClientCollection();

            that.wsse = Session.get('WSSE');

            // The fetch below will perform GET /clients
            that.collection.fetch({
                success: function (client) {
                    var data = { clients: client.toJSON() };
                    var compiledTemplate = Handlebars.compile(clientPageTemplate);
                    var html = compiledTemplate(data);
                    $(that.el).html(html);
                },
                headers: {
                        'Authorization' :'WSSE profile="UsernameToken"',
                        'X-wsse' : that.wsse,
                        'Accept' : 'application/json'
                    }
            })


            //this.collection.add({ name: "Ginger Kid"});
            // Using Underscore we can compile our template with data

        }
    });
    return ClientList;
});