define([
    'jquery',
    'lodash',
    'backbone',
    'models/session',
    'text!templates/dashboard/login.html',
    'text!templates/dashboard/logout.html'
], function($, _, Backbone, Session, exampleLoginTemplate, exampleLogoutTemplate){
    var DashboardPage = Backbone.View.extend({
        el: '.page',
        initialize: function () {
            var that = this;
            // Bind to the Session auth attribute so we
            // make our view act recordingly when auth changes
            Session.on('change:auth', function (session) {
                that.render();
            });


        },
        render: function () {
              // Simply choose which template to choose depending on
              // our Session models auth attribute
            if(Session.authenticated()){
                this.$el.html(_.template(exampleLogoutTemplate, {username: Session.get('username')}));
            } else {
                this.$el.html(exampleLoginTemplate);
            }
        },
        events: {
            'submit form.login': 'login', // On form submission
            'click .logout': 'logout'
        },
        login: function (ev) {
            ev.preventDefault();
            // Disable the button
            $('[type=submit]', ev.currentTarget).val('Logging in').attr('disabled', 'disabled');
            // Serialize the form into an object using a jQuery plgin
            username = $('#username').val();
            password = $('#password').val();
            Session.login(username, password);
        },
        logout: function (ev) {
            // Disable the button
            $(ev.currentTarget).text('Logging out').attr('disabled', 'disabled');
            ev.preventDefault();
            Session.logout();
        }
    });
    return DashboardPage;
});
